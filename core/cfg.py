from matplotlib import pyplot as plt
from matplotlib import cm
import os
import numpy as np
from scipy.linalg import norm
from scipy.interpolate import RectBivariateSpline as RBS
from scipy import sparse
from scipy.sparse import linalg as splinalg
from scipy.interpolate import griddata
from scipy.integrate import quad
import itertools
from mpi4py import MPI
COMM = MPI.COMM_WORLD           # MPI Init
RANK = COMM.Get_rank()          # Each thread gets own rank
NUMTHREADS = COMM.Get_size()      # Each thread knows numprocs
print RANK
def wait(): COMM.recv(source=0,tag=1)
def release(): return [COMM.send('go',dest=i+1,tag=1) for i in xrange(NUMTHREADS - 1)]

#root = '/home/freechus9/Research/transportgf'
root = os.getcwd()
output_dir = root + '/output'
potroot = root + '/pots/'
#output_dir = '/home/freechus9/Research/transportgf/output'
#potroot = '/home/freechus9/Research/transportgf/pots/'
np.set_printoptions(precision = 3,linewidth=10000,threshold=np.nan)

class Const:
    e = 1.602e-19
    t = 2.7*e
    eV = e*1.e-6
    h = 6.626e-34
    hbar = 1.054e-34
    eps = 1.e-28
    kB = 1.38e-23
    transres = .01*e
    i = complex(0.,1.)
    a = 1.42e-10
    aH = 1.1e-10
    mu0 = 4.e-7*np.pi

class Bools:
    verbose = True
    gfoverwrite = False
    transoverwrite = False
    condoverwrite = True
    sixband = True
    thirdnearest = False
    Hpassivation = True
    periodic = False


def adjoint(matrix): return np.conjugate(np.transpose(matrix))

def load():
    global HParam,BW
    BW = (3 if Bools.sixband else 1)

    class HParam:
        if Bools.sixband:

            '''
            EpC = 1.2057*Const.e
            EdC = 24.1657*Const.e
            Cppp = -3.26*Const.e
            #Cppp = -2.7*Const.e
            Cpdp = 2.40*Const.e
            Cddp = 3.60*Const.e
            Cddd = -7.40*Const.e
            '''
            EpC = 1.2057*Const.e
            EdC = 24.1657*Const.e
            Cppp = -3.26*Const.e
            Cpdp = 2.40*Const.e
            Cddp = 3.60*Const.e
            Cddd = -7.40*Const.e
            
        else:
            EpC = 0.#.12742*Const.e
            EdC = 0.
            Cppp = -2.7*Const.e
            Cpdp = 0.
            Cddp = 0.
            Cddd = 0.

        if Bools.Hpassivation:
            #EpH = 1.2057*Const.e
            #EdH = 24.1657*Const.e
            #Hppp = -3.26*Const.e
            #Hpdp = 2.40*Const.e
            #Hddp = 3.60*Const.e
            #Hddd = -7.40*Const.e
            EpH = 13.0402*Const.e
            EdH = 20.9020*Const.e
            Hppp = -.61754*Const.e
            Hpdp = 3.41170*Const.e
            Hddp = 10.44660*Const.e
            Hddd = -13.96340*Const.e
        else:
            EpH = 0.
            EdH = 0.
            Hppp = 0.
            Hpdp = 0.
            Hddp = 0.
            Hddd = 0.

        bw = (3 if Bools.sixband else 1) #blockwidth

        selfH = np.zeros((bw,bw),dtype = complex)
        selfC = np.zeros((bw,bw),dtype = complex)

        selfH[0,0] = EpH
        selfC[0,0] = EpC

        if bw == 3:
            selfH[1,1] = EdH
            selfH[2,2] = EdH
            selfC[1,1] = EdC
            selfC[2,2] = EdC

        """
        Hopping directions:
        1 - \. upleft
        2 - /' downleft
        3 - ,_ right
        4 - '\ downright
        5 - ,/ upright
        6 - _, left
        """

        t1 = -2.7*Const.e
        t2 = -0.2*Const.e
        t3 = -0.18*Const.e

        r1 = [-Const.a/2.,Const.a*np.sqrt(3.)/2.]
        r2 = [-Const.a/2.,-Const.a*np.sqrt(3.)/2.]
        r3 = [Const.a,0.]
        r4 = [Const.a/2.,-Const.a*np.sqrt(3.)/2.]
        r5 = [Const.a/2.,Const.a*np.sqrt(3.)/2.]
        r6 = [-Const.a,0.]
        rH1 = [-Const.aH/2.,Const.aH*np.sqrt(3.)/2.]
        rH2 = [-Const.aH/2.,-Const.aH*np.sqrt(3.)/2.]
        rH3 = [Const.aH,0.]
        rH4 = [Const.aH/2.,-Const.aH*np.sqrt(3.)/2.]
        rH5 = [Const.aH/2.,Const.aH*np.sqrt(3.)/2.]
        rH6 = [-Const.aH,0.]

        CC1 = np.zeros((bw,bw),dtype = complex)
        CC2 = np.zeros((bw,bw),dtype = complex)
        CC3 = np.zeros((bw,bw),dtype = complex)

        CC1[0,0] = Cppp
        CC2[0,0] = Cppp
        CC3[0,0] = Cppp

        if bw == 3:
            CC1[0,1] = .5*Cpdp
            CC1[0,2] = -np.sqrt(3.)*.5*Cpdp
            CC1[1,0] = -.5*Cpdp
            CC1[1,1] = .25*Cddp + .75*Cddd
            CC1[1,2] = -np.sqrt(3.)*.25*(Cddp - Cddd)
            CC1[2,0] = np.sqrt(3.)*.5*Cpdp
            CC1[2,1] = -np.sqrt(3.)*.25*(Cddp - Cddd)
            CC1[2,2] = .75*Cddp + .25*Cddd

            CC2[0,1] = .5*Cpdp
            CC2[0,2] = np.sqrt(3.)*.5*Cpdp
            CC2[1,0] = -.5*Cpdp
            CC2[1,1] = .25*Cddp + .75*Cddd
            CC2[1,2] = np.sqrt(3.)*.25*(Cddp - Cddd)
            CC2[2,0] = -np.sqrt(3.)*.5*Cpdp
            CC2[2,1] = np.sqrt(3.)*.25*(Cddp - Cddd)
            CC2[2,2] = .75*Cddp + .25*Cddd

            CC3[0,1] = -Cpdp
            CC3[1,0] = Cpdp
            CC3[1,1] = Cddp
            CC3[2,2] = Cddd

        CC4 = adjoint(CC1)
        CC5 = adjoint(CC2)
        CC6 = adjoint(CC3)

        if Bools.Hpassivation:
            CH1 = np.zeros((bw,bw),dtype = complex)
            CH2 = np.zeros((bw,bw),dtype = complex)
            CH3 = np.zeros((bw,bw),dtype = complex)

            CH1[0,0] = Hppp
            CH2[0,0] = Hppp
            CH3[0,0] = Hppp

            if bw == 3:

                CH1[0,1] = .5*Hpdp
                CH1[0,2] = -np.sqrt(3.)*.5*Hpdp
                CH1[1,0] = -.5*Hpdp
                CH1[1,1] = .25*Hddp + .75*Hddd
                CH1[1,2] = -np.sqrt(3.)*.25*(Hddp - Hddd)
                CH1[2,0] = np.sqrt(3.)*.5*Hpdp
                CH1[2,1] = -np.sqrt(3.)*.25*(Hddp - Hddd)
                CH1[2,2] = .75*Hddp + .25*Hddd

                CH2[0,1] = .5*Hpdp
                CH2[0,2] = np.sqrt(3.)*.5*Hpdp
                CH2[1,0] = -.5*Hpdp
                CH2[1,1] = .25*Hddp + .75*Hddd
                CH2[1,2] = np.sqrt(3.)*.25*(Hddp - Hddd)
                CH2[2,0] = -np.sqrt(3.)*.5*Hpdp
                CH2[2,1] = np.sqrt(3.)*.25*(Hddp - Hddd)
                CH2[2,2] = .75*Hddp + .25*Hddd

                CH3[0,1] = -Hpdp
                CH3[1,0] = Hpdp
                CH3[1,1] = Hddp
                CH3[2,2] = Hddd

            CH4 = adjoint(CH1)
            CH5 = adjoint(CH2)
            CH6 = adjoint(CH3)
load()
def get_pot(pot,x,y):
    return pot[0](pot[2][0] - (pot[3][0] - x),pot[2][1] - (pot[3][1] - y))[0][0]

def print_2d_colormap(xdata,ydata,zdata,vlim = [],num_pts = (1000,1000),cmap = cm.Set1,subplot = None):
    if subplot != None:
        vx = np.linspace(xdata.min(),xdata.max(),num_pts[0])[:,None]
        vy = np.linspace(ydata.min(),ydata.max(),num_pts[1])[None,]
        VX,VY = np.broadcast_arrays(vx,vy)
        interpdata = griddata((xdata.ravel(),ydata.ravel()),zdata.ravel(),(VX,VY),method = 'cubic')
        mesh = subplot.pcolormesh(VX,VY,interpdata,cmap = cm.Set1,vmin = vlim[0],vmax = vlim[1])
        subplot.set_xlabel('x (nm)')
        subplot.set_ylabel('y (nm)')
        subplot.set_xlim(VX.min(),VX.max())
        subplot.set_ylim(VY.min(),VY.max())
        subplot.set_aspect('equal')
    return subplot,mesh

def line_int(field,r1,r2):
    """
    Evaluates the line integral through 3D vector potential field betweetn
    3d coords r1 and r2
    """

    r1 = np.array(r1)
    r2 = np.array(r2)

    def integrand(t):
        return np.dot(field(t*r2 + (1 - t)*r1),r2 - r1)

    return quad(integrand,0.,1.)[0]
        

def Hamiltonian(lattice, edge = 'ac',nrg = 0.0, pot = None, V = None, A = None, B = None, 
                Z = None, U = None, spin = 1):
    """
    Given a LxW ac lattice, fill in hamiltonian with 
    correct hydrogen passivation with d-orbital hopping
    """
    L = len(lattice)
    W = len(lattice[0])

    lattice = np.append(lattice,np.array([['O' for i in xrange(3)] for j in xrange(L)]),axis = 1)
    lattice = np.append(lattice,np.array([['O' for i in xrange(W + 3)] for j in xrange(3)]),axis=0)

    H = sparse.lil_matrix((BW*L*W*spin,BW*L*W*spin),dtype = complex)
    I = np.identity(BW,dtype = complex)

    N = BW*L*W*spin

    for site in xrange(L*W):
        col = site/W
        row = site % W

        x,y = get_pos(col,row,edge)

        if pot != None: 
            potval = -get_pot(pot,x,y)*Const.e
            
        elif V != None: potval = V
        else: potval = 0.

        center = lattice[col,row]

        if Bools.thirdnearest:
            if edge == 'zz':
                if center == 'A':
                    H[site*spin,site*spin] = ((U[0]*Const.e*U[1][site*spin+1]) 
                                              if (U != None and center != 'O' and spin == 2) 
                                              else 0.) + ((Z[site*spin]) 
                                              if (Z != None and center != 'O') 
                                              else 0.) + HParam.EpC
                    if spin == 2:
                        H[site*spin+1,site*spin+1] = ((U[0]*Const.e*U[1][site*spin]) 
                                                  if (U != None and center != 'O' and spin == 2) 
                                                  else 0.) + ((Z[site*spin+1]) 
                                                  if (Z != None and center != 'O') 
                                                  else 0.) + HParam.EpC

                    #1NN
                    if lattice[col-1,row] == 'B':
                        H[site*spin - spin*W,site*spin] = HParam.t1
                        if spin == 2: H[site*spin - spin*W + 1,site*spin+1] = HParam.t1
                    if lattice[col+1,row] == 'B':
                        H[site*spin + spin*W,site*spin] = HParam.t1
                        if spin == 2: H[site*spin + spin*W + 1,site*spin+1] = HParam.t1
                    if lattice[col,row+1] == 'B':
                        H[site*spin + spin,site*spin] = HParam.t1
                        if spin == 2: H[site*spin + spin + 1,site*spin+1] = HParam.t1

                    #2NN
                    if lattice[col-2,row] == 'A':
                        H[site*spin - spin*2*W,site*spin] = HParam.t2
                        if spin == 2: H[site*spin - spin*2*W + 1,site*spin+1] = HParam.t2
                    if lattice[col-1,row-1] == 'A':
                        H[site*spin - spin*W - spin,site*spin] = HParam.t2
                        if spin == 2: H[site*spin - spin*W - spin + 1,site*spin+1] = HParam.t2
                    if lattice[col+2,row] == 'A':
                        H[site*spin + spin*2*W,site*spin] = HParam.t2
                        if spin == 2: H[site*spin + spin*2*W + 1,site*spin + 1] = HParam.t2
                    if lattice[col+1,row-1] == 'A':
                        H[site*spin + spin*W - spin,site*spin] = HParam.t2
                        if spin == 2: H[site*spin + spin*W - spin + 1,site*spin+1] = HParam.t2
                    if lattice[col-1,row+1] == 'A':
                        H[site*spin - spin*W + spin,site*spin] = HParam.t2
                        if spin == 2: H[site*spin - spin*W + spin + 1,site*spin +1] = HParam.t2
                    if lattice[col+1,row+1] == 'A':
                        H[site*spin + spin*W + spin,site*spin] = HParam.t2
                        if spin == 2: H[site*spin + spin*W + spin + 1,site*spin+1] = HParam.t2

                    #3NN
                    if lattice[col-2,row+1] == 'B':
                        H[site*spin - spin*2*W + spin,site*spin] = HParam.t3
                        if spin == 2: H[site*spin - spin*2*W + spin + 1,site*spin + 1] = HParam.t3
                    if lattice[col+2,row+1] == 'B':
                        H[site*spin + spin*2*W + spin,site*spin] = HParam.t3
                        if spin == 2: H[site*spin + spin*2*W + spin + 1,site*spin + 1] = HParam.t3
                    if lattice[col,row-1] == 'B':
                        H[site*spin - spin,site*spin] = HParam.t3
                        if spin == 2: H[site*spin - spin + 1,site*spin + 1] = HParam.t3
                elif center == 'B':
                    H[site*spin,site*spin] = ((U[0]*Const.e*U[1][site*spin+1]) 
                                              if (U != None and center != 'O' and spin == 2) 
                                              else 0.) + ((Z[site*spin]) 
                                              if (Z != None and center != 'O') 
                                              else 0.) + HParam.EpC
                    if spin == 2:
                        H[site*spin+1,site*spin+1] = ((U[0]*Const.e*U[1][site*spin]) 
                                                  if (U != None and center != 'O' and spin == 2) 
                                                  else 0.) + ((Z[site*spin+1]) 
                                                  if (Z != None and center != 'O') 
                                                  else 0.) + HParam.EpC

                    #1NN
                    if lattice[col-1,row] == 'A':
                        H[site*spin - spin*W,site*spin] = HParam.t1
                        if spin == 2: H[site*spin - spin*W + 1,site*spin+1] = HParam.t1
                    if lattice[col+1,row] == 'A':
                        H[site*spin + spin*W,site*spin] = HParam.t1
                        if spin == 2: H[site*spin + spin*W + 1,site*spin+1] = HParam.t1
                    if lattice[col,row-1] == 'A':
                        H[site*spin - spin,site*spin] = HParam.t1
                        if spin == 2: H[site*spin - spin + 1,site*spin+1] = HParam.t1

                    #2NN
                    if lattice[col-2,row] == 'B':
                        H[site*spin - spin*2*W,site*spin] = HParam.t2
                        if spin == 2: H[site*spin - spin*2*W + 1,site*spin + 1] = HParam.t2
                    if lattice[col+2,row] == 'B':
                        H[site*spin + spin*2*W,site*spin] = HParam.t2
                        if spin == 2: H[site*spin + spin*2*W + 1,site*spin + 1] = HParam.t2
                    if lattice[col-1,row-1] == 'B':
                        H[site*spin - spin*W - spin,site*spin] = HParam.t2
                        if spin == 2: H[site*spin - spin*W - spin + 1,site*spin +1] = HParam.t2
                    if lattice[col+1,row-1] == 'B':
                        H[site*spin + spin*W - spin,site*spin] = HParam.t2
                        if spin == 2: H[site*spin + spin*W - spin + 1,site*spin+1] = HParam.t2
                    if lattice[col-1,row+1] == 'B':
                        H[site*spin - spin*W + spin,site*spin] = HParam.t2
                        if spin == 2: H[site*spin - spin*W + spin + 1,site*spin +1] = HParam.t2
                    if lattice[col+1,row+1] == 'B':
                        H[site*spin + spin*W + spin,site*spin] = HParam.t2
                        if spin == 2: H[site*spin + spin*W + spin + 1,site*spin+1] = HParam.t2

                    #3NN
                    if lattice[col-2,row-1] == 'A': 
                        H[site*spin - spin*2*W - spin,site*spin] = HParam.t3
                        if spin == 2: H[site*spin - spin*2*W - spin + 1,site*spin +1] = HParam.t3
                    if lattice[col+2,row-1] == 'A': 
                        H[site*spin + spin*2*W - spin,site*spin] = HParam.t3
                        if spin == 2: H[site*spin + spin*2*W - spin + 1,site*spin +1] = HParam.t3
                    if lattice[col,row+1] == 'A': 
                        H[site*spin + spin,site*spin] = HParam.t3
                        if spin == 2: H[site*spin + spin + 1,site*spin +1] = HParam.t3
                    

        else:
            up = 'O' if row == W - 1 else lattice[col,row + 1]
            down = 'O' if row == 0 else lattice[col,row - 1]
            right = ('P' if Bools.periodic else 'L') if col == L - 1 else lattice[col + 1,row]
            left = ('P' if Bools.periodic else 'L') if col == 0 else lattice[col - 1,row]

            """
            AC For magfield, phase = np.exp(ie/hbar * 1/2 (x1-x0)(y1+y0))
            ZZ For magfield, phase = np.exp(ie/hbar * 1/2 (y1-y0)(x1+x0))

            Hopping directions:
            1 - \. upleft
            2 - /' downleft
            3 - ,_ right
            4 - '\ downright
            5 - ,/ upright
            6 - _, left
            """

            setup = True
            setdown = True
            setright = True
            setleft = True
            setleftper = False
            setrightper = False

            if edge == 'zz':
                if center == 'A':
                    # Onsite
                    ctrblock = HParam.selfC.copy()
                    
                    # Then do hop up (site -> site + 1)
                    if up == 'B': 
                        upblock = HParam.CC3.copy()
                        if A != None: 
                            upblock *= np.exp(Const.i*Const.e/Const.hbar*
                                              line_int(A,[x,y,0.],[x + HParam.r3[0],y + HParam.r3[1],0.]))
                            
                    elif up == 'H': 
                        upblock = HParam.CH3.copy()
                        if A != None: 
                            upblock *= np.exp(Const.i*Const.e/Const.hbar*
                                              line_int(A,[x,y,0.],[x + HParam.rH3[0],y + HParam.rH3[1],0.]))
                    elif up == 'O':
                        if BW == 3 and Bools.Hpassivation:
                            if pot != None:
                                potvalH = -pot[0](pot[2][0] - (pot[3][0] - (x + Const.aH)),
                                                  pot[2][1] - (pot[3][1] - y))[0][0]*Const.e
                            else: potvalH = 0.
                            ctrblock += get_self_energy(nrg,HParam.CH6,HParam.CH3,HParam.selfH + I*potvalH)
                        setup = False
                    # Then do hop right (col -> col + 1)
                    if right == 'B' or right == 'P': 
                        rightblock = HParam.CC2.copy()
                        if A != None: 
                            rightblock *= np.exp(Const.i*Const.e/Const.hbar*
                                              line_int(A,[x,y,0.],[x + HParam.r2[0],y + HParam.r2[1],0.]))
                        if right == 'P':
                            setrightper = True
                            setright = False
                    elif right == 'H': 
                        rightblock = HParam.CH2.copy()
                        if A != None: 
                            rightblock *= np.exp(Const.i*Const.e/Const.hbar*
                                              line_int(A,[x,y,0.],[x + HParam.rH2[0],y + HParam.rH2[1],0.]))
                    elif right == 'O':
                        if BW == 3 and Bools.Hpassivation:
                            if pot != None:
                                potvalH = -pot[0](pot[2][0] - (pot[3][0] - (x - .5*Const.aH)),
                                                  pot[2][1] - (pot[3][1] - (y - np.sqrt(3)*.5*Const.aH)))[0][0]*Const.e
                            else: potvalH = 0.
                            ctrblock += get_self_energy(nrg, HParam.CH5,HParam.CH2,HParam.selfH + I*potvalH)
                        setright = False
                    elif right == 'L': setright = False
                    # Then do hop left (col -> col - 1)
                    if left == 'B' or left == 'P': 
                        leftblock = HParam.CC1.copy()
                        if A != None: 
                            leftblock *= np.exp(Const.i*Const.e/Const.hbar*
                                              line_int(A,[x,y,0.],[x + HParam.r1[0],y + HParam.r1[1],0.]))
                        if left == 'P':
                            setleftper = True
                            setleft = False
                    elif left == 'H': 
                        leftblock = HParam.CH1.copy()
                        if A != None: 
                            leftblock *= np.exp(Const.i*Const.e/Const.hbar*
                                              line_int(A,[x,y,0.],[x + HParam.rH1[0],y + HParam.rH1[1],0.]))
                    elif left == 'O':
                        if BW == 3 and Bools.Hpassivation:
                            if pot != None:
                                potvalH = -pot[0](pot[2][0] - (pot[3][0] - (x - .5*Const.aH)),
                                                  pot[2][1] - (pot[3][1] - (y + np.sqrt(3)*.5*Const.aH)))[0][0]*Const.e
                            else: potvalH = 0.
                            ctrblock += get_self_energy(nrg, HParam.CH4,HParam.CH1,HParam.selfH + I*potvalH)
                        setleft = False
                    elif left == 'L': setleft = False
                    setdown = False
                elif center == 'B':
                    ctrblock = HParam.selfC.copy()
                    if down == 'A': 
                        downblock = HParam.CC6.copy()
                        if A != None: 
                            downblock *= np.exp(Const.i*Const.e/Const.hbar*
                                              line_int(A,[x,y,0.],[x + HParam.r6[0],y + HParam.r6[1],0.]))
                    elif down == 'H': 
                        downblock = HParam.CH6.copy()
                        if A != None: 
                            downblock *= np.exp(Const.i*Const.e/Const.hbar*
                                              line_int(A,[x,y,0.],[x + HParam.rH6[0],y + HParam.rH6[1],0.]))
                    elif down == 'O': 
                        if BW == 3 and Bools.Hpassivation:
                            if pot != None:
                                potvalH = -pot[0](pot[2][0] - (pot[3][0] - (x - Const.aH)),
                                                  pot[2][1] - (pot[3][1] - y))[0][0]*Const.e
                            else: potvalH = 0.
                            ctrblock += get_self_energy(nrg, HParam.CH3,HParam.CH6,HParam.selfH + I*potvalH)
                        setdown = False
                    # Then do hop left (col -> col - 1)
                    if left == 'A' or left == 'P': 
                        leftblock = HParam.CC5.copy()
                        if A != None: 
                            leftblock *= np.exp(Const.i*Const.e/Const.hbar*
                                              line_int(A,[x,y,0.],[x + HParam.r5[0],y + HParam.r5[1],0.]))
                        if left == 'P':
                            setleftper = True
                            setleft = False
                    elif left == 'H': 
                        leftblock = HParam.CH5.copy()
                        if A != None: 
                            leftblock *= np.exp(Const.i*Const.e/Const.hbar*
                                              line_int(A,[x,y,0.],[x + HParam.rH5[0],y + HParam.rH5[1],0.]))
                    elif left == 'O': 
                        if BW == 3 and Bools.Hpassivation:
                            if pot != None:
                                potvalH = -pot[0](pot[2][0] - (pot[3][0] - (x + .5*Const.aH)),
                                                  pot[2][1] - (pot[3][1] - (y + np.sqrt(3.)*.5*Const.aH)))[0][0]*Const.e
                            else: potvalH = 0.
                            ctrblock += get_self_energy(nrg, HParam.CH2,HParam.CH5,HParam.selfH + I*potvalH)
                        setleft = False
                    elif left == 'L': setleft = False
                    # Then do hop right (col -> col r 1)
                    if right == 'A' or right == 'P': 
                        rightblock = HParam.CC4.copy()
                        if A != None: 
                            rightblock *= np.exp(Const.i*Const.e/Const.hbar*
                                              line_int(A,[x,y,0.],[x + HParam.r4[0],y + HParam.r4[1],0.]))
                        if right == 'P':
                            setrightper = True
                            setright = False
                    elif right == 'H': 
                        rightblock = HParam.CH4.copy()
                        if A != None: 
                            rightblock *= np.exp(Const.i*Const.e/Const.hbar*
                                              line_int(A,[x,y,0.],[x + HParam.rH4[0],y + HParam.rH4[1],0.]))
                    elif right == 'O': 
                        if BW == 3 and Bools.Hpassivation:
                            if pot != None:
                                potvalH = -pot[0](pot[2][0] - (pot[3][0] - (x + .5*Const.aH)),
                                                  pot[2][1] - (pot[3][1] - (y - np.sqrt(3.)*.5*Const.aH)))[0][0]*Const.e
                            else: potvalH = 0.
                            ctrblock += get_self_energy(nrg, HParam.CH1,HParam.CH4,HParam.selfH + I*potvalH)
                        setright = False
                    elif right == 'L': setright = False
                    setup = False
                elif center == 'H':
                    # Onsite
                    ctrblock = HParam.selfH.copy()
                    
                    # Then do hop up (site -> site + 1)
                    if up == 'B': 
                        upblock = HParam.CH3.copy()
                        if A != None: 
                            upblock *= np.exp(Const.i*Const.e/Const.hbar*
                                              line_int(A,[x,y,0.],[x + HParam.rH3[0],y + HParam.rH3[1],0.]))
                        setright = setleft = setdown = False
                    elif right == 'B': 
                        rightblock = HParam.CH2.copy()
                        if A != None: 
                            rightblock *= np.exp(Const.i*Const.e/Const.hbar*
                                              line_int(A,[x,y,0.],[x + HParam.rH2[0],y + HParam.rH2[1],0.]))
                        setup = setleft = setdown = False
                    elif left == 'B': 
                        leftblock = HParam.CH1.copy()
                        if A != None: 
                            leftblock *= np.exp(Const.i*Const.e/Const.hbar*
                                              line_int(A,[x,y,0.],[x + HParam.rH1[0],y + HParam.rH1[1],0.]))
                        setright = setdown = setup = False
                    elif down == 'A': 
                        downblock = HParam.CH6.copy()
                        if A != None: 
                            downblock *= np.exp(Const.i*Const.e/Const.hbar*
                                              line_int(A,[x,y,0.],[x + HParam.rH6[0],y + HParam.rH6[1],0.]))
                        setleft = setright = setup = False
                    elif left == 'A': 
                        leftblock = HParam.CH5.copy()
                        if A != None: 
                            leftblock *= np.exp(Const.i*Const.e/Const.hbar*
                                              line_int(A,[x,y,0.],[x + HParam.rH5[0],y + HParam.rH5[1],0.]))
                        setup = setdown = setright = False
                    elif right == 'A': 
                        rightblock = HParam.CH4.copy()
                        if A != None: 
                            rightblock *= np.exp(Const.i*Const.e/Const.hbar*
                                              line_int(A,[x,y,0.],[x + HParam.rH4[0],y + HParam.rH4[1],0.]))
                        setleft = setup = setdown = False
                    else: setup = setdown = setright = setleft = False
                else:
                    ctrblock = np.zeros((BW,BW),dtype = complex)
                    setup = False
                    setdown = False
                    setright = False
                    setleft = False

                """
                AC For magfield, phase = np.exp(ie/hbar * 1/2 (x1-x0)(y1+y0))
                ZZ For magfield, phase = np.exp(ie/hbar * 1/2 (y1-y0)(x1+x0))

                Hopping directions:
                1 - \. upleft
                2 - /' downleft
                3 - ,_ right
                4 - '\ downright
                5 - ,/ upright
                6 - _, left
                """

            elif edge == 'ac':
                if center == 'A':
                    # Onsite
                    ctrblock = HParam.selfC.copy()
                    
                    # Then do hop up (site -> site + 1)
                    if up == 'B': 
                        upblock = HParam.CC1.copy()
                        if A != None: 
                            upblock *= np.exp(Const.i*Const.e/Const.hbar*
                                              line_int(A,[x,y,0.],[x + HParam.r1[0],y + HParam.r1[1],0.]))
                    elif up == 'H': 
                        upblock = HParam.CH1.copy()
                        if A != None: 
                            upblock *= np.exp(Const.i*Const.e/Const.hbar*
                                              line_int(A,[x,y,0.],[x + HParam.rH1[0],y + HParam.rH1[1],0.]))
                    elif up == 'O':
                        if BW == 3 and Bools.Hpassivation:
                            if pot != None:
                                potvalH = -pot[0](pot[2][0] - (pot[3][0] - (x - .5*Const.aH)),
                                                  pot[2][1] - (pot[3][1] - (y + np.sqrt(3.)*.5*Const.aH)))[0][0]*Const.e
                            else: potvalH = 0.
                            ctrblock += get_self_energy(nrg, HParam.CH4,HParam.CH1,HParam.selfH + I*potvalH)
                        setup = False
                    # Then do hop down (site -> site - 1)
                    if down == 'B': 
                        downblock = HParam.CC2.copy()
                        if A != None: 
                            downblock *= np.exp(Const.i*Const.e/Const.hbar*
                                              line_int(A,[x,y,0.],[x + HParam.r2[0],y + HParam.r2[1],0.]))
                    elif down == 'H': 
                        downblock = HParam.CH2.copy()
                        if A != None: 
                            downblock *= np.exp(Const.i*Const.e/Const.hbar*
                                              line_int(A,[x,y,0.],[x + HParam.rH2[0],y + HParam.rH2[1],0.]))
                    elif down == 'O':
                        if BW == 3 and Bools.Hpassivation:
                            if pot != None:
                                potvalH = -pot[0](pot[2][0] - (pot[3][0] - (x - .5*Const.aH)),
                                                  pot[2][1] - (pot[3][1] - (y - np.sqrt(3.)*.5*Const.aH)))[0][0]*Const.e
                            else: potvalH = 0.
                            ctrblock += get_self_energy(nrg, HParam.CH5,HParam.CH2,HParam.selfH + I*potvalH)
                        setdown = False
                    # Then do hop right (col -> col + 1)
                    if right == 'B': 
                        rightblock = HParam.CC3.copy()
                        if A != None: 
                            rightblock *= np.exp(Const.i*Const.e/Const.hbar*
                                              line_int(A,[x,y,0.],[x + HParam.r3[0],y + HParam.r3[1],0.]))
                    elif right == 'H': 
                        rightblock = HParam.CH3.copy()
                        if A != None: 
                            rightblock *= np.exp(Const.i*Const.e/Const.hbar*
                                              line_int(A,[x,y,0.],[x + HParam.rH3[0],y + HParam.rH3[1],0.]))
                    elif right == 'O':
                        if BW == 3 and Bools.Hpassivation:
                            if pot != None:
                                potvalH = -pot[0](pot[2][0] - (pot[3][0] - (x + Const.aH)),
                                                  pot[2][1] - (pot[3][1] - y))[0][0]*Const.e
                            else: potvalH = 0.
                            ctrblock += get_self_energy(nrg, HParam.CH6,HParam.CH3,HParam.selfH + I*potvalH)
                        setright = False
                    elif right == 'L': setright = False
                    setleft = False
                elif center == 'B':
                    ctrblock = HParam.selfC.copy()
                    if up == 'A': 
                        upblock = HParam.CC5.copy()
                        if A != None: 
                            upblock *= np.exp(Const.i*Const.e/Const.hbar*
                                              line_int(A,[x,y,0.],[x + HParam.r5[0],y + HParam.r5[1],0.]))
                    elif up == 'H': 
                        upblock = HParam.CH5.copy()
                        if A != None: 
                            upblock *= np.exp(Const.i*Const.e/Const.hbar*
                                              line_int(A,[x,y,0.],[x + HParam.rH5[0],y + HParam.rH5[1],0.]))
                    elif up == 'O': 
                        if BW == 3 and Bools.Hpassivation:
                            if pot != None:
                                potvalH = -pot[0](pot[2][0] - (pot[3][0] - (x + .5*Const.aH)),
                                                  pot[2][1] - (pot[3][1] - (y + np.sqrt(3.)*.5*Const.aH)))[0][0]*Const.e
                            else: potvalH = 0.
                            ctrblock += get_self_energy(nrg, HParam.CH2,HParam.CH5,HParam.selfH + I*potvalH)
                        setup = False
                    if down == 'A': 
                        downblock = HParam.CC4.copy()
                        if A != None: 
                            downblock *= np.exp(Const.i*Const.e/Const.hbar*
                                              line_int(A,[x,y,0.],[x + HParam.r4[0],y + HParam.r4[1],0.]))
                    elif down == 'H': 
                        downblock = HParam.CH4.copy()
                        if A != None: 
                            downblock *= np.exp(Const.i*Const.e/Const.hbar*
                                              line_int(A,[x,y,0.],[x + HParam.rH4[0],y + HParam.rH4[1],0.]))
                    elif down == 'O':
                        if BW == 3 and Bools.Hpassivation:
                            if pot != None:
                                potvalH = -pot[0](pot[2][0] - (pot[3][0] - (x + .5*Const.aH)),
                                                  pot[2][1] - (pot[3][1] - (y - np.sqrt(3.)*.5*Const.aH)))[0][0]*Const.e
                            else: potvalH = 0.
                            ctrblock += get_self_energy(nrg, HParam.CH1,HParam.CH4,HParam.selfH + I*potvalH)
                        setdown = False
                    # Then do hop left (col -> col - 1)
                    if left == 'A': 
                        leftblock = HParam.CC6.copy()
                        if A != None: 
                            leftblock *= np.exp(Const.i*Const.e/Const.hbar*
                                              line_int(A,[x,y,0.],[x + HParam.r6[0],y + HParam.r6[1],0.]))
                    elif left == 'H': 
                        leftblock = HParam.CH6.copy()
                        if A != None: 
                            leftblock *= np.exp(Const.i*Const.e/Const.hbar*
                                              line_int(A,[x,y,0.],[x + HParam.rH6[0],y + HParam.rH6[1],0.]))
                    elif left == 'O': 
                        if BW == 3 and Bools.Hpassivation:
                            if pot != None:
                                potvalH = -pot[0](pot[2][0] - (pot[3][0] - (x - Const.aH)),
                                                  pot[2][1] - (pot[3][1] - y))[0][0]*Const.e
                            else: potvalH = 0.
                            ctrblock += get_self_energy(nrg, HParam.CH3,HParam.CH6,HParam.selfH + I*potvalH)
                        setleft = False
                    elif left == 'L': setleft = False
                    setright = False
                elif center == 'H':
                    # Onsite
                    ctrblock = HParam.selfH.copy()
                    
                    # Then do hop up (site -> site + 1)
                    if up == 'B': 
                        upblock = HParam.CH1.copy()
                        if A != None: 
                            upblock *= np.exp(Const.i*Const.e/Const.hbar*
                                              line_int(A,[x,y,0.],[x + HParam.rH1[0],y + HParam.rH1[1],0.]))
                        setright = setleft = setdown = False
                    elif right == 'B': 
                        rightblock = HParam.CH3.copy()
                        if A != None: 
                            rightblock *= np.exp(Const.i*Const.e/Const.hbar*
                                              line_int(A,[x,y,0.],[x + HParam.rH3[0],y + HParam.rH3[1],0.]))
                        setup = setleft = setdown = False
                    elif down == 'B': 
                        downblock = HParam.CH2.copy()
                        if A != None: 
                            downblock *= np.exp(Const.i*Const.e/Const.hbar*
                                              line_int(A,[x,y,0.],[x + HParam.rH2[0],y + HParam.rH2[1],0.]))
                        setright = setleft = setup = False
                    elif down == 'A': 
                        downblock = HParam.CH4.copy()
                        if A != None: 
                            downblock *= np.exp(Const.i*Const.e/Const.hbar*
                                              line_int(A,[x,y,0.],[x + HParam.rH4[0],y + HParam.rH4[1],0.]))
                        setleft = setright = setup = False
                    elif left == 'A': 
                        leftblock = HParam.CH6.copy()
                        if A != None: 
                            leftblock *= np.exp(Const.i*Const.e/Const.hbar*
                                              line_int(A,[x,y,0.],[x + HParam.rH6[0],y + HParam.rH6[1],0.]))
                        setup = setdown = setright = False
                    elif up == 'A': 
                        upblock = HParam.CH5.copy()
                        if A != None: 
                            upblock *= np.exp(Const.i*Const.e/Const.hbar*
                                              line_int(A,[x,y,0.],[x + HParam.rH5[0],y + HParam.rH5[1],0.]))
                        setleft = setright = setdown = False
                    else: setup = setdown = setright = setleft = False
                else:
                    ctrblock = np.zeros((BW,BW),dtype = complex)
                    setup = False
                    setdown = False
                    setright = False
                    setleft = False

            #TODO(anuj) fix for multiple basis H
            H[BW*site*spin:BW*site*spin+BW,BW*site*spin:BW*site*spin + BW] = ((((U[0]*Const.e*U[1][site*spin+1]) 
                                              if (U != None and center != 'O' and spin == 2) else 0.) + potval)*np.identity(BW,dtype=complex) + ctrblock.copy())
            H[BW*site*spin:BW*site*spin + BW,BW*site*spin:BW*site*spin + BW] =H[BW*site*spin:BW*site*spin+BW,BW*site*spin:BW*site*spin + BW] + (((Z[site*spin]) 
                                              if (Z != None and center != 'O' and spin == 2) else 0.)*np.identity(BW,dtype=complex))

            if setup: H[BW*site*spin + BW*spin:BW*site*spin + BW*spin + BW,BW*site*spin:BW*site*spin + BW] = upblock.copy() # row -> row + 1
            if setdown: H[BW*site*spin - BW*spin:BW*site*spin - BW*spin + BW,BW*site*spin:BW*site*spin + BW] = downblock.copy() # row -> row - 1
            if setright: H[BW*site*spin + BW*W*spin:BW*site*spin + BW*W*spin + BW,BW*site*spin:BW*site*spin + BW] = rightblock.copy()
            if setleft: H[BW*site*spin - BW*W*spin:BW*site*spin - BW*W*spin + BW,BW*site*spin:BW*site*spin + BW] = leftblock.copy()
            if setrightper: H[BW*site*spin + BW*W*spin - N:BW*site*spin + BW*W*spin + BW - N,BW*site*spin:BW*site*spin + BW] = rightblock.copy()
            if setleftper: H[BW*site*spin - BW*W*spin + N:BW*site*spin - BW*W*spin + BW + N,BW*site*spin:BW*site*spin + BW] = leftblock.copy()

            if spin == 2:
                H[BW*site*spin+BW:BW*site*spin+2*BW,BW*site*spin+BW:BW*site*spin + 2*BW] = ((((U[0]*Const.e*U[1][site*spin]) 
                                                  if (U != None and center != 'O' and spin == 2) else 0.) + potval)*np.identity(BW,dtype=complex) + ctrblock.copy())
                H[BW*site*spin+BW:BW*site*spin + 2*BW,BW*site*spin+BW:BW*site*spin + 2*BW] = H[BW*site*spin+BW:BW*site*spin + 2*BW,BW*site*spin+BW:BW*site*spin + 2*BW] + (((Z[site*spin + 1]) 
                                                  if (Z != None and center != 'O' and spin == 2) else 0.)*np.identity(BW,dtype=complex))
                if setup: H[BW*site*spin + BW*spin + BW:BW*site*spin + BW*spin + BW + BW,BW*site*spin + BW:BW*site*spin + BW + BW] = upblock.copy() # row -> row + 1
                if setdown: H[BW*site*spin - BW*spin + BW:BW*site*spin - BW*spin + BW + BW,BW*site*spin + BW:BW*site*spin + BW + BW] = downblock.copy() # row -> row - 1
                if setright: H[BW*site*spin + BW*W*spin + BW:BW*site*spin + BW*W*spin + BW + BW,
                               BW*site*spin + BW:BW*site*spin + BW + BW] = rightblock.copy()
                if setleft: H[BW*site*spin - BW*W*spin + BW:BW*site*spin - BW*W*spin + BW + BW,BW*site*spin + BW:BW*site*spin + BW + BW] = leftblock.copy()
                if setrightper: H[BW*site*spin + BW*W*spin + BW - N:BW*site*spin + BW*W*spin + BW + BW - N,
                                  BW*site*spin + BW:BW*site*spin + 2*BW] = rightblock.copy()
                if setleftper: H[BW*site*spin - BW*W*spin + BW + N:BW*site*spin - BW*W*spin + BW + BW + N,
                                 BW*site*spin + BW:BW*site*spin + 2*BW] = leftblock.copy()



    return H

def Hamiltonian_ind(H):
    """
    takes lil matrix H and returns list of all elements in tuple form
    with the first element the number of elems
    """ 

    x_ind,y_ind = H.nonzero()

    data = [len(x_ind)]

    for x,y in zip(x_ind,y_ind):
        data.append((x,y,H[x,y].real,H[x,y].imag))

    return data

def grouper(n,iterable,fillvalue=None):
    args = [iter(iterable)]*n
    return itertools.izip_longest(*args,fillvalue=fillvalue)

def get_self_energy(nrg,V12,V21,H2):
    """
    Returns self energy of lattice 2 given hopping matrices 
    V12 (|1><2|) and V21 (|2><1|) (direction of hop)
    """
    g2 = np.zeros((BW,BW),dtype = complex)
    I = np.identity(BW, dtype = complex)
    EHS = sparse.csr_matrix((nrg + Const.i*Const.eps)*I - H2)
    I = np.zeros(BW,dtype = complex)
    for i in xrange(len(V12)):
        I[i-1],I[i] = 0,1
        g2[:,i] = splinalg.spsolve(EHS,I,use_umfpack = True)

    return np.dot(np.dot(V12,g2),V21)

def alone(dev_config,i,j,edge):
    neighbors = 0

    L = len(dev_config)
    W = len(dev_config[0])

    species = dev_config[i][j]

    if edge == 'ac':

        if species == 'B':

            if i > 0 and dev_config[i-1][j] != 'O': neighbors += 1
            if j > 0 and dev_config[i][j - 1] != 'O': neighbors += 1
            if j < W - 1 and dev_config[i][j + 1] != 'O': neighbors +=1
            if i == 0: neighbors += 1
        

        elif species == 'A':

            if i < L - 1 and dev_config[i+1][j] != 'O': neighbors += 1
            if j > 0 and dev_config[i][j - 1] != 'O': neighbors += 1
            if j < W - 1 and dev_config[i][j + 1] != 'O': neighbors +=1
            if i == L - 1: neighbors += 1

        if neighbors == 1: return True
        else: return False
    

def get_lattice(width,length,edge,shape='GNR',pores = None,pinch = .15, extent = .25):
    """
    Returns colmajor lattice with H on edge dangling bonds with edge shape

    To ensure a pore center is the center of a hexagon, 

    W odd, L even
    
    if (W + 1) % 4 == 0 then (L % 4) != 0
    if (W - 1) % 4 == 0 then (L % 4) == 0

    ie. if W is 3(5), then L = 2,6,10(4,8,12) would be fine

    To ensure a pore center is at a bond,

    W odd, L even
    
    if (W - 1) % 4 == 0 then (L % 4) != 0
    if (W + 1) % 4 == 0 then (L % 4) == 0

    ie. if W is 3(5), then L = 4,8,12(2,6,10) would be fine

    FYI, if (W - 1) % 4 == 0 (5,9,13...) then n*W/4 pore locations are
    symmetric
    """
    #TODO(anuj): Implement pores with correct H passivation

    lattice = []
    x0,x1 = get_pos(0,1,'ac')[0],get_pos(length - 1, 1,'ac')[0]
    y0,y1 = get_pos(0,0,'ac')[1],get_pos(0,width-1,'ac')[1]
    for i in xrange(length):
        col = []
        for j in xrange(width):
            x,y = get_pos(i,j,'ac')
            if pores != None:
                for pore in pores:
                    if pore[0] == 'ellipse':
                        r1 = pore[1]
                        r2 = pore[2]
                        px,py = get_pos(length - 1, 1,'ac')[0]/2,pore[3]*get_pos(0,width - 1,'ac')[1]
                        delta = np.array(get_pos(i,j,'ac')) - np.array([px,py])

                        if delta[0]**2/r1**2 + delta[1]**2/r2**2 <= 1.: col.append('O')
                        elif not i % 2: col.append('A' if not j % 2 else 'B')
                        else: col.append('A' if j % 2 else 'B')
                    else:
                        r = pore[0]
                        px,py = get_pos(length - 1, 1,'ac')[0]/2,pore[1]*get_pos(0,width - 1,'ac')[1]

                        if norm(np.array(get_pos(i,j,'ac')) - np.array([px,py])) <= r: col.append('O')
                        elif not i % 2: col.append('A' if not j % 2 else 'B')
                        else: col.append('A' if j % 2 else 'B')
            else:
                if not i % 2: col.append('A' if not j % 2 else 'B')
                else: col.append('A' if j % 2 else 'B')
            if shape == 'QPC':
                if y + 1.e-10 <= (y1-y0)*pinch*np.exp(-(x - (x0 + x1)/2.)**2/((x1 - x0)*extent)**2):
                    col[-1] = 'O'
                elif y  - 1.e-10 > y1 - (y1-y0)*pinch*np.exp(-(x - (x0 + x1)/2.)**2/((x0 + x1)*extent)**2):
                    col[-1] = 'O'
        lattice.append(col)
    was_alone = True
    while was_alone:
        was_alone = False
        for i in xrange(length):
            for j in xrange(width):
                if alone(lattice,i,j,edge): 
                    lattice[i][j] = 'O'
                    if not was_alone: was_alone = True
    return np.array(lattice)


def plot(curves,labels=None,leglabels = None,titles = None,subplots = 1,filename = 'test',loadfile = None,xlim = None,ylim = None,vlines = None):
    #TODO(anuj): add checker to see if numcurves/subplots = int
    #            as well as subplots = len(labels)

    plots = ()
    if loadfile != None: 
        curves = []
        for loadthis in loadfile: curves.append(np.genfromtxt(loadthis,delimiter=', '))
    persubplot = len(curves)/subplots
    rows = int(np.ceil(subplots**.5))
    cols = rows #Assuming square grid

    fig = plt.figure(2,figsize = (16,12))
    colors = ['k','r','b','g','k','r','b','g']
    styles = ['-','-','-','-','--','--','--','--']

    for i in xrange(subplots):
        sub = fig.add_subplot(rows*100+cols*10+i+1)
        for j in xrange(persubplot):
            sub.plot(curves[i*persubplot + j][0],curves[i*persubplot + j][1],color=colors[j],ls=styles[j],lw=4,label='poo')
            #print curves[i*persubplot + j][0],curves[i*persubplot + j][1]
        if labels != None:
            sub.set_xlabel(labels[i][0],fontweight='bold')
            sub.set_ylabel(labels[i][1],fontweight='bold')
        if titles != None: sub.set_title(titles[i],fontweight='bold')
        if not xlim == None: 
            if not xlim[i] == None:
                sub.set_xlim([xlim[i][0],xlim[i][1]])
        if not ylim == None: 
            if not ylim[i] == None: 
                sub.set_ylim([ylim[i][0],ylim[i][1]])
        if not vlines == None:
            sub.vlines(vlines[i],curves[i*persubplot + j][1].min(),curves[i*persubplot + j][1].max())

        if leglabels != None:
            handles, labels = sub.get_legend_handles_labels()
            # reverse the order
            leg = sub.legend(handles, leglabels,'upper left',prop={'size':28},ncol=1)
            leg.draw_frame(False)

    if filename.rfind("/") != -1:
        mkdir(filename[:filename.rfind("/")])

    fig.savefig(filename + '.png',format='png',bbox_inches='tight')
    fig.clear()

def insort(data1,data2,kind='mergesort'):
    """
    Adapted from user seberg on stack overflow
    takes two 2-d ndarrays, and sorts them based on axis 1.
    """
    data3 = np.column_stack((data1,data2)).T
    data3 = data3[data3[:,0].argsort()]
    flag = np.ones(len(data3),dtype=bool)
    np.not_equal(data3[1:,0],data3[:-1,0],out=flag[1:])
    return data3[flag].T

def print_dev_conf(lattice,edge,filename='lattice.png',subplot = None,fig = None,style='k-'):
    #TODO(anuj): add H atoms


    if fig == None: fig = plt.figure(1,figsize=(12,9))
    if subplot == None: x = fig.add_subplot(111)
    else: x = subplot

    color = style
    L = len(lattice)
    W = len(lattice[0])
    for i in xrange(L):
        for j in xrange(W):
            if edge == 'ac':
                ax = float(i)*1.5 + .5
                ay = float(j)*np.sqrt(3.0)/2
                if lattice[i][j] == 'A':
                    if j < W - 1 and lattice[i][j+1] == 'B':
                        x.plot([ax,ax - .5],[ay,ay + np.sqrt(3.0)/2],'k',lw=2)
                    if i < L - 1 and lattice[i+1][j] == 'B':
                        x.plot([ax,ax + 1.],[ay,ay],'k',lw=2)
                    if j > 0 and lattice[i][j - 1] == 'B':
                        x.plot([ax,ax - .5],[ay,ay - np.sqrt(3.0)/2],'k',lw=2)
            elif edge=='zz':
                ax = float(j)*1.5 + .5
                ay = float(i)*np.sqrt(3.0)/2
                if lattice[i][j] == 'A':
                    if j < W - 1 and lattice[i][j+1] == 'B':
                        x.plot([ax,ax + 1.],[ay,ay],'k',lw=2)
                    if i < L - 1 and lattice[i+1][j] == 'B':
                        x.plot([ax,ax - .5],[ay,ay + np.sqrt(3.0)/2],'k',lw=2)
                    if i > 0 and lattice[i-1][j] == 'B':
                        x.plot([ax,ax - .5],[ay,ay - np.sqrt(3.0)/2],'k',lw=2)
    x.set_aspect('equal')
    x.axis('off')
    fig.savefig(filename,format='png')

    
    return (fig,x)

def lattice_hash(lattice): return hash(' '.join([''.join(elem) for elem in lattice]))
    
def mkdir(dirpath):
    """ 
    Create a directory and all parent folders.
    Features:
        - parent directoryies will be created
        - if directory already exists, then do nothing
        - if there is another filsystem object with the same name, raise an exception
    """
    if Bools.verbose: print "Making dir %s..." % dirpath
    try:
        if os.path.isdir(dirpath):
            if Bools.verbose: print "Directory already exists!"
        elif os.path.isfile(dirpath):
            if Bools.verbose: print "Is not directory, but file!"
            raise OSError("cannot create directory, file already exists: '%s'" % newdir)
        else:
            if Bools.verbose: print "Directory not made! Creating..."
            head, tail = os.path.split(dirpath)
            if head and not os.path.isdir(head):
                mkdir(head)
            if tail:
                os.mkdir(dirpath)
    except:
        if Bools.verbose: print "Skipping mkdir %s" % dirpath

def fermi(_energy,_fermienergy,_T,carrier=None,gapval=0.):
    arg = (_energy - _fermienergy)/(Const.kB*_T)
    
    if carrier=='e' and _energy < gapval: return 0.
    elif carrier == 'h' and _energy > gapval: return 1.
    elif arg > 500.0: return 0.0
    elif arg < -500.0: return 1.0
    else: return 1.0/(1.0 + np.exp(arg))

def gen_pot(filename,offset = 0.):

    if os.path.isfile(potroot + filename): 
        tmp_x,tmp_y,tmp_z,tmp_val = np.genfromtxt(potroot + filename,delimiter=',').T
    else: raise NameError("Pot data file %s does not exist!" % (potroot + filename))

    # Find length of y mesh by finding location of first different x value
    token = tmp_x[0]
    for val in tmp_x:
        if val != token: 
            meshlen_y = np.where(tmp_x==val)[0][0]
            break
    meshlen_x = len(tmp_x)/meshlen_y

    pot_x = np.zeros(meshlen_x,dtype = float)
    pot_y = np.zeros(meshlen_y,dtype = float)
    pot_val = np.zeros((meshlen_x,meshlen_y),dtype = float)

    for i in xrange(meshlen_x):
        pot_x[i] = tmp_x[meshlen_y*i]
        for j in xrange(meshlen_y):
            if i == 0: pot_y[j] = tmp_y[j]
            pot_val[i][j] = tmp_val[i*meshlen_y + j] + offset

    return RBS(pot_x,pot_y,pot_val,bbox=[pot_x[0],pot_x[-1],pot_y[0],pot_y[-1]])

def get_pos(col,row,edge, hand = 'R'):
    if edge == 'ac': 
        """
        Row 0 is at y = 0.0
        Col 0 B sublattice atoms are at x = 0.0
        """

        x = Const.a

        if row % 2 == 0:
            if col % 2 == 0: x *= (1.5*col + .5) 
            else: x *= (1.5*col)
        else:
            if col % 2 == 0: x *= (1.5*col)
            else: x *= (1.5*col + .5)

        if hand == 'L': x = -x - Const.a

        y = .5*np.sqrt(3)*Const.a*row
    elif edge == 'zz': 
        """
        Row 0 sublattice B atoms are at y = 0.0
        Col 0 is at y = 0.0
        """

        # Just having some fun..
        x = Const.a*(((1.5*row + .5) if not row % 2 else 1.5*row) if not col % 2 
                     else (1.5*row if not row % 2 else (1.5*row + .5)))
        x = Const.a

        if col % 2 == 0:
            if row % 2 == 0: x *= (1.5*row + .5)
            else: x *= (1.5*row)
        else:
            if row % 2 == 0: x *= (1.5*row)
            else: x *= (1.5*row + .5)

        y = .5*np.sqrt(3)*Const.a*col

        if hand == 'L': y = -y - .5*np.sqrt(3)*Const.a

    return np.array([x,y])

def closest_to_val(array,val):
    """
    returns index of position in array closest to val
    assumes array is sorted in increasing order
    """
    length = len(array)

    if length == 0: raise Exception
    elif length == 1: return 0
    else:
        diff = abs(val - array[0])
        index = 0

        for i in xrange(1,len(array)):
            if abs(val - array[i]) > diff:
                break
            else:
                diff = abs(val - array[i])
                index = i

        return index


def get_gap(energy,trans):
    """
    Given transmission vs energy data, returns size of gap
    """
    tol = 1.e-2
    index = closest_to_val(energy,0.0)
    for x,y in zip(energy[index:],trans[index:]): 
        if y >= tol: 
            end = x
            break
    for x,y in reversed(zip(energy[:index],trans[:index])):
        if y >= tol: 
            start = x
            break
    return end - start

def remove_outliers(data,thresholds):
    #N dimensional data with N-1 dimensional thresholds
    index = []
    for lens in xrange(len(thresholds)):
        for i,val in enumerate(data[lens+1]):
    	    if val > thresholds[lens] or val < 0.: index.append(i)

    return np.delete(data,list(set(index)),1)

def infinite_wire(wires):
    """
    """


    return A

def Bwire(wires):

    def B(r):
        _B = np.array([0.,0.,0.])
        for (current,direction,point) in wires:
            d = norm(np.cross(direction,point - r))
            _B += (Const.mu0*current/(2*np.pi*d))*np.cross(direction,r - point)/norm(point - r)/norm(direction)

        return _B

    return B

def Awire(wires):
    
    def A(r):
        _A = np.array([0.,0.,0.])
        for (current,direction,point) in wires:
            d = norm(np.cross(direction,point - r))
            _A += -Const.mu0*current/3./np.pi*np.log(d**2)*direction
        return _A

    return A
        


def curl(field,dx,dy,dz):
    xx,xy,xz = np.gradient(field[0],dx,dy,dz)
    yx,yy,yz = np.gradient(field[1],dx,dy,dz)
    zx,zy,zz = np.gradient(field[2],dx,dy,dz)

    return (zy - yz, xz - zx, yx - xy)


def get_dirs(width,length,U,phase,num_ks,current):
    return "/%i-%i-%.2f-%s-%i-%.2e%s" % (width,length,U,phase,num_ks,current,'-H' if Bools.Hpassivation else '')

