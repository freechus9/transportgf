import os
import sys
from numpy import *
import numpy as np
from scipy.integrate import simps
from scipy import linalg,sparse
from scipy.sparse import linalg as splinalg
import cfg
from cfg import Const as const
from cfg import Bools as bools
from mpi4py import MPI
from numpy.linalg import eig
from numpy.linalg import eigvalsh
from matplotlib import pyplot as plt


if cfg.Bools.verbose: print "VERBOSE MODE"

COMM = MPI.COMM_WORLD           # MPI Init
RANK = COMM.Get_rank()          # Each thread gets own rank
NUMTHREADS = COMM.Get_size()      # Each thread knows numprocs

font = {'family' : 'normal',
        'weight' : 'heavy',
        'size' : 16}

colors = ['k','b','r','g','m','y']

def wait(): COMM.recv(source=0,tag=1)
def release(): return [COMM.send('go',dest=i+1,tag=1) for i in xrange(NUMTHREADS - 1)]
def adjoint(matrix): return np.conjugate(np.transpose(matrix))
        
def leadsurfacegf(nrg, num_iter=200, W=10, edge="ac", V = None, 
                  U = None, Z = None,spin = 1, gfoverwrite = False,Bfield = None,
                  nosave = False, debug = False, A = None,hand = 'R',transres = cfg.Const.transres,
                  explicitH = False):
    """
    Returns the surface GF of a square-lattice lead W sites wide of type edge.
    Uses the recursive algorithm described in ref for num_iter iterations
    Cite: http://dx.doi.org/10.1088/0305-4608/14/5/016
    """

    gfdir = '%s/leadgf/%s/%i %i%s%s%s%s %.4e eV' % (cfg.output_dir, edge + ('6b' if cfg.Bools.sixband else '') + ('eH' if explicitH else '')
                                    + ('H' if cfg.Bools.Hpassivation else '') + ('%i' % spin if spin != 1 else ''), W, (0 if num_iter == None else num_iter),
                                    (' A%s' % A[1] if A != None else ''),('R' if hand == 'R' else 'L'),
                                    (' V%.2f' % V if V != None else ''),(' U%.2f%s' % (U[0],U[2]) if U != None else '') + (' Z%.2e' % Z[0] if Z != None else ''), (transres/cfg.Const.e))

    if RANK == 0: 
        cfg.mkdir(gfdir)
        release()
    else: wait()

    # TODO(anuj): add wait_for_procs and RANK bool to mkdirs (looks sloppy)

    gfpath = '%s/%i' % (gfdir, int(round(nrg/transres)))

    #if os.path.isfile(gfpath) and not gfoverwrite: 
        #if cfg.Bools.verbose: print "%s loaded" % gfpath
    BW = (3 if cfg.Bools.sixband else 1)
    if explicitH: W += 2

    try:
        if cfg.Bools.verbose: print "Trying to load %s" % gfpath
        if gfoverwrite or nosave: raise Exception
        data = genfromtxt(gfpath, delimiter=', ').view(complex)
        if len(data) != W*BW*spin: raise Exception
        return data

    except:
        try:
            if cfg.Bools.verbose: print "Trying to load AGAIN %s" % gfpath
            if gfoverwrite or nosave: raise Exception
            return np.array(genfromtxt(gfpath, delimiter=', ').T.tolist()).view(complex)
        except:
            if cfg.Bools.verbose: print "Did not load %s" % gfpath
            if hand == 'L':
                if edge == 'ac':
                    H = np.zeros((4*BW*W*spin,4*BW*W*spin),dtype = complex)

                    if explicitH: 
                        lattice = cfg.get_lattice(W-2,3,edge)
                        lattice = np.append(lattice,np.reshape(np.array(['H' for i in xrange(3)]),(3,1)),axis=1)
                        lattice = np.append(np.reshape(np.array(['H' for i in xrange(3)]),(3,1)),lattice,axis=1)
                    else: lattice = cfg.get_lattice(W,3,edge)

                    _H = cfg.Hamiltonian(np.array(lattice),edge,nrg=nrg,
                                         U = U, Z = Z,spin = spin, A = (A[0] if A != None else None),V = V).todense()

                    H[:BW*W*spin,:BW*W*spin] = _H[BW*W*spin:2*BW*W*spin,BW*W*spin:2*BW*W*spin].copy()
                    H[BW*W*spin:2*BW*W*spin,BW*W*spin:2*BW*W*spin] = _H[:BW*W*spin,:BW*W*spin].copy()
                    H[BW*W*spin:2*BW*W*spin,:BW*W*spin] = _H[:BW*W*spin,BW*W*spin:2*BW*W*spin].copy()
                    H[:BW*W*spin,BW*W*spin:2*BW*W*spin] = _H[BW*W*spin:2*BW*W*spin,:BW*W*spin].copy()

                    H[2*BW*W*spin:3*BW*W*spin,2*BW*W*spin:3*BW*W*spin] = _H[BW*W*spin:2*BW*W*spin,BW*W*spin:2*BW*W*spin].copy()
                    H[3*BW*W*spin:,3*BW*W*spin:] = _H[:BW*W*spin,:BW*W*spin].copy()
                    H[3*BW*W*spin:,2*BW*W*spin:3*BW*W*spin] = _H[:BW*W*spin,BW*W*spin:2*BW*W*spin].copy()
                    H[2*BW*W*spin:3*BW*W*spin,3*BW*W*spin:] = _H[BW*W*spin:2*BW*W*spin,:BW*W*spin].copy()

                    H[2*BW*W*spin:3*BW*W*spin,BW*W*spin:2*BW*W*spin] = _H[BW*W*spin:2*BW*W*spin,2*BW*W*spin:3*BW*W*spin].copy()

                    H[BW*W*spin:2*BW*W*spin,2*BW*W*spin:3*BW*W*spin] = _H[2*BW*W*spin:3*BW*W*spin,BW*W*spin:2*BW*W*spin].copy()

                    E_H = ((nrg + cfg.Const.i*cfg.Const.eps)*sparse.identity(BW*4*W*spin,dtype = complex) - H)

                elif edge == 'zz':
                    H = np.zeros((4*BW*W*spin,4*BW*W*spin),dtype = complex)

                    _H = cfg.Hamiltonian(cfg.get_lattice(W,3,edge),edge,nrg=nrg,B=Bfield, A = (A[0] if A != None else None),
                                         U = U, spin = spin,Z = Z,V = V).todense()

                    H[:BW*W*spin,:BW*W*spin] = _H[BW*W*spin:2*BW*W*spin,BW*W*spin:2*BW*W*spin].copy()
                    H[BW*W*spin:2*BW*W*spin,:BW*W*spin] = _H[:BW*W*spin,BW*W*spin:2*BW*W*spin].copy()
                    H[:BW*W*spin,BW*W*spin:2*BW*W*spin] = _H[BW*W*spin:2*BW*W*spin,:BW*W*spin].copy()
                    H[BW*W*spin:2*BW*W*spin,BW*W*spin:2*BW*W*spin] = _H[:BW*W*spin,:BW*W*spin].copy()
                    
                    H[2*BW*W*spin:,2*BW*W*spin:] = H[:2*BW*W*spin,:2*BW*W*spin].copy()

                    H[2*BW*W*spin:3*BW*W*spin,BW*W*spin:2*BW*W*spin] = _H[BW*W*spin:2*BW*W*spin,2*BW*W*spin:].copy()
                    
                    H[BW*W*spin:2*BW*W*spin,2*BW*W*spin:3*BW*W*spin] = _H[2*BW*W*spin:,BW*W*spin:2*BW*W*spin].copy()

                    E_H = ((nrg + cfg.Const.i*cfg.Const.eps)*np.identity(BW*4*W*spin,dtype = complex) - H  )

            elif hand == 'R':
                if edge == 'ac':
                    if explicitH:
                        lattice = cfg.get_lattice(W-2, 4, edge)
                        lattice = np.append(lattice,np.reshape(np.array(['H' for i in xrange(4)]),(4,1)),axis=1)
                        lattice = np.append(np.reshape(np.array(['H' for i in xrange(4)]),(4,1)),lattice,axis=1)
                    else:
                        lattice = cfg.get_lattice(W, 4, edge)
                    E_H = ((nrg + cfg.Const.i*cfg.Const.eps)*sparse.identity(BW*4*W*spin,dtype = complex) - 
                           cfg.Hamiltonian(np.array(lattice),edge,nrg=nrg, 
                           U = U, Z = Z,spin = spin, B = Bfield, A = (A[0] if A != None else None),V = V)).todense()

                elif edge == 'zz':
                    E_H = ((nrg + cfg.Const.i*cfg.Const.eps)*sparse.identity(BW*5*W*spin,dtype = complex) - 
                           cfg.Hamiltonian(cfg.get_lattice(W, 5, edge),edge,nrg=nrg,
                           U = U, Z = Z,spin = spin, B = Bfield, A = (A[0] if A != None else None),V = V))[BW*W*spin:,BW*W*spin:].todense()

                    #H = cfg.Hamiltonian(cfg.get_lattice(W, 5, edge),edge,nrg=nrg,
                    #       U = U, Z = Z,spin = spin, B = Bfield, A = (A[0] if A != None else None),V = V)[BW*W*spin:,BW*W*spin:].todense()

            d = E_H[:2*BW*W*spin,:2*BW*W*spin].copy()
            D = E_H[2*BW*W*spin:,2*BW*W*spin:].copy()
            A = -E_H[:2*BW*W*spin,2*BW*W*spin:].copy()
            B = -E_H[2*BW*W*spin:,:2*BW*W*spin].copy()
            I = np.identity(2*BW*W*spin,dtype = complex)


            # Iterate

            error = 1.
            delta = np.array([complex(1.,1.)])
            i = -1
            j = 0

            def listmax(_data):
                return np.array([abs(_data.real),abs(_data.imag)]).max()

            #while listmax(delta) > 0.:
            for i in xrange(num_iter):
                if listmax(delta) == 0.:
                    j += 1
                if j > 10: break #hack, weird that delta = 0 but needs 1 iter more
                i += 1
                try: D_inv = linalg.solve(D,I)
                except: 
                    gfdata = np.array([])
                    if not nosave: savetxt(gfpath, gfdata.view(float), delimiter=', ', newline='\n')
                    return gfdata
                delta = np.dot(np.dot(A,D_inv),B)
                d = d - delta
                D = D - np.dot(np.dot(A,D_inv),B) - np.dot(np.dot(B,D_inv),A)
                A = np.dot(np.dot(A,D_inv),A)
                B = np.dot(np.dot(B,D_inv),B)

            #print delta.real.max(),delta.imag.max()
            #print delta.real.min(),delta.imag.min()
            #print error

            try: 
                d_inv = linalg.solve(d,I)
                tmpbool = (edge == "ac" or edge == "zz")
                gfdata = np.array(d_inv[:BW*W*spin,:BW*W*spin]) if tmpbool else d_inv
            except: gfdata = np.array([])

            if not nosave: 
                savetxt(gfpath, gfdata.view(float), delimiter=', ', newline='\n')
                if cfg.Bools.verbose: print "gf saved to %s" % gfpath

            return gfdata.copy()



def trans_dos(nrgs, res, edge, lattice, pot = None,
              compute_dos = None, transoverwrite = False,condoverwrite = False,
              gfoverwrite = False,transskip = False,
              use_umfpack = True, B = None, A=None,V = None,
              U = None, Z = None,spin = 1,need_trans = False,nosave = False,
              noleads = False, debug = False,dirname = False,cond_nrgs = None,
              T = 300.,explicitH = False):
    """
    Returns a list of the transmission coefficient, the space-independent DOS,
    and the site-dependent DOS for a square lattice with hopping parameter t,
    of type edge, with lattice dev_config.
    EDIT: six-band approximation
    """
    
    #TODO(anuj): clean up W/W
    # tie in cond, integrate as flags
    W = len(lattice[0])
    L = len(lattice)
    BW = (3 if cfg.Bools.sixband else 1)

    latticeid = hash(' '.join([''.join(elem) for elem in lattice]))

    nrg0 = int(round(nrgs[0]/res))
    nrg1 = int(round(nrgs[1]/res))

    num_pts = nrg1 - nrg0 #domain is [nrg0,nrg1]

    transdir = '%s/transdos/%s/%i/%s%s%s/%.2e %.2e %.4e' % (cfg.output_dir, edge + ('6b' if cfg.Bools.sixband else '') + ('eH' if explicitH else '')
                                         + ('H' if cfg.Bools.Hpassivation else '') + ('P' if cfg.Bools.periodic else '') + 
                                         ('%i' % spin if spin != 1 else ''), latticeid, 
                                         'nopot' if pot == None else os.path.dirname(pot[1]),
                                         '' if pot == None else '/' + os.path.basename(pot[1]),
                                         ('A%s' % A[1] if A != None else '') + ('V%.2f' % V if V != None else '')
                                         + ('U%.2f%s' % (U[0],U[2]) if U != None else '')
                                         + ('Z%.2e' % (Z[0]) if Z != None else ''),
                                         nrgs[0],nrgs[1],(res/const.e))

    print transdir

    if dirname: return transdir

    if RANK == 0:
        cfg.mkdir(transdir)
        if compute_dos != None: cfg.mkdir(transdir + '/DOS')
        release()
    else: wait()

    transpath = []

    transpath.append('%s/energy.dat' % (transdir))
    if need_trans: transpath.append('%s/transmission.dat' % (transdir))

    if compute_dos != None:
        for i in compute_dos:
            for j in xrange(spin):
                transpath.append('%s/DOS/%i%s.dat' % (transdir,i,'-2' if j == 1 else ''))


    if all([os.path.isfile(trans) for trans in transpath]) and not transoverwrite: 
        packet = [np.loadtxt(filename,delimiter=', ') for filename in transpath]
    elif transskip: return (None,None)
    else:
        need_nrg = True

        if NUMTHREADS > 1:
            threadpts = num_pts/NUMTHREADS
            ptsleft = num_pts % NUMTHREADS
            if RANK < ptsleft: threadpts += 1
            start_nrg = nrg0 + RANK*threadpts + (ptsleft if RANK >= ptsleft else 0)
        else: 
            threadpts = num_pts
            ptsleft = 0
            start_nrg = nrg0

        nrgs_thread = np.arange(start_nrg,start_nrg + threadpts)*res

        if need_nrg: energydata = []
        else: energydata = np.loadtxt(transpath[0],delimiter=', ')
        if need_trans: transdata = []


        if compute_dos != None: 
            dosdata = []
            for i in compute_dos: 
                for j in xrange(spin):
                    dosdata.append([])

        for n,nrg in enumerate(nrgs_thread):

            if RANK == 0 and n % 10 == 2: print n*100./len(nrgs_thread),"%"
            debug = (True if n == 3 else False)

            if noleads: leadgfR = leadgfL = np.zeros((BW*W*spin,BW*W*spin),dtype = complex)
            else:
                leadgfR = leadsurfacegf(nrg = nrg, W = W - (2 if explicitH else 0), 
                                       edge = edge, gfoverwrite = gfoverwrite, 
                                       U = U, spin = spin, Bfield = B,A = A, V = V, 
                                       Z = Z,nosave = nosave,hand = 'R', debug= debug,
                                       num_iter = 200,transres = res,explicitH = explicitH)
                if A == None:
                    leadgfL = leadgfR.copy()
                else:
                    leadgfL = leadsurfacegf(nrg = nrg, W = W - (2 if explicitH else 0), 
                                           edge = edge, gfoverwrite = gfoverwrite, 
                                           U = U, spin = spin, Bfield = B,A = A, V = V, 
                                           Z = Z,nosave = nosave,hand = 'L', debug= debug,
                                           num_iter = 200,transres = res,explicitH = explicitH)

            if len(leadgfR) == 0 or len(leadgfL) == 0: continue
            elif need_trans == False and compute_dos == None: continue
            # TODO(anuj): work on another way to deal with NaNs in surfaceGF


            EHS = ((nrg + cfg.Const.i*const.eps)*sparse.identity(BW*L*W*spin,dtype = complex) -
                   cfg.Hamiltonian(lattice,edge,nrg = nrg,pot = pot, Z = Z,B = B, V = V,A=(A[0] if A != None else None),U = U,spin = spin)).asformat('lil')

            # Add self energy

            if noleads: VLD = np.zeros((BW*W*spin,BW*W*spin))
            else:
                if explicitH:
                    _lattice = cfg.get_lattice(W-2,3,edge)
                    _lattice = np.append(_lattice,np.reshape(np.array(['H' for i in xrange(3)]),(3,1)),axis=1)
                    _lattice = np.append(np.reshape(np.array(['H' for i in xrange(3)]),(3,1)),_lattice,axis=1)
                else: _lattice = cfg.get_lattice(W,3,edge)
                VLD = cfg.Hamiltonian(_lattice,edge,nrg = nrg, 
                                  U = U, spin = spin, B = B, V = V,Z = Z,A=(A[0] if A != None else None))[BW*W*spin:2*BW*W*spin,2*BW*W*spin:].todense()
            
            SE = np.dot(np.dot(adjoint(VLD),leadgfL),VLD)

            VLD = None

            EHS[:BW*W*spin,:BW*W*spin] = EHS[:BW*W*spin,:BW*W*spin]- SE

            GammaL = cfg.Const.i*(SE - cfg.adjoint(SE))
            
            SE = None

            if noleads: VLD = np.zeros((BW*W*spin,BW*W*spin))
            else:

                if edge == 'ac': 
                    if explicitH:
                        _lattice = cfg.get_lattice(W-2,3,edge)
                        _lattice = np.append(_lattice,np.reshape(np.array(['H' for i in xrange(3)]),(3,1)),axis=1)
                        _lattice = np.append(np.reshape(np.array(['H' for i in xrange(3)]),(3,1)),_lattice,axis=1)
                    else: _lattice = cfg.get_lattice(W,3,edge)
                    VLD = cfg.Hamiltonian(_lattice,edge,nrg = nrg, B = B, V = V,
                                                       U = U, spin = spin, Z = Z,
                                                       A=(A[0] if A != None else None))[2*BW*W*spin:,BW*W*spin:2*BW*W*spin].todense()
                elif edge == 'zz': VLD = cfg.Hamiltonian(cfg.get_lattice(W,3,edge),edge,
                                                         nrg = nrg, B = B, V = V,U = U, Z = Z,spin = spin,
                                                         A=(A[0] if A != None else None))[BW*W*spin:2*BW*W*spin,:BW*W*spin].todense()

            SE = np.dot(np.dot(adjoint(VLD),leadgfR),VLD)

            VLD = leadgfL = leadgfR = None

            EHS[-BW*W*spin:,-BW*W*spin:] = EHS[-BW*W*spin:,-BW*W*spin:] - SE

            GammaR = cfg.Const.i*(SE - cfg.adjoint(SE))

            SE = None

            EHS = sparse.csr_matrix(EHS)

            I = np.zeros(BW*L*W*spin, dtype = complex)

            if need_nrg: energydata.append(nrg)
            if need_trans:
                for i in xrange(BW*W*spin):
                    if i == 0:
                        I[i] = 1.
                        G = splinalg.spsolve(EHS,I,use_umfpack = use_umfpack)
                    else:
                        I[i-1],I[i] = 0.,1.
                        G = np.vstack((G,splinalg.spsolve(EHS,I,use_umfpack = use_umfpack)))

                Gn1 = G[:BW*W*spin,-BW*W*spin:]

                transdata.append(np.dot(np.dot(np.dot(GammaL,cfg.adjoint(Gn1)),GammaR),Gn1).trace().real[0,0])

                GammaL = GammaR = Gn1 = None

            if compute_dos != None:
            #CONTINUE HERE

                for i,dos in enumerate(compute_dos):
                    I = np.zeros(BW*L*W*spin, dtype = complex)
                    for j in xrange(spin): 
                        I[BW*dos*spin + j*BW - 1],I[BW*dos*spin + j*BW] = 0.,1.
                        dosdata[i*spin + j].append(-1./pi*splinalg.spsolve(EHS,I,use_umfpack = use_umfpack)[BW*dos*spin + j*BW].imag)
                        if BW == 3:
                            I[3*dos*spin + j*3],I[3*dos*spin + j*3 + 1] = 0.,1.
                            dosdata[i*spin + j][-1] += -1./pi*splinalg.spsolve(EHS,I,use_umfpack = use_umfpack)[3*dos*spin + j*3 + 1].imag
                            I[3*dos*spin + j*3 + 1],I[3*dos*spin + j*3 + 2] = 0.,1.
                            dosdata[i*spin + j][-1] += -1./pi*splinalg.spsolve(EHS,I,use_umfpack = use_umfpack)[3*dos*spin + j*3 + 2].imag

            G = None
            I = EHS = None

        if RANK == 0:
            for thread in xrange(NUMTHREADS - 1):
                if need_nrg:
                    energydata += COMM.recv(source = thread + 1, tag = 2)
                if need_trans:
                    transdata += COMM.recv(source = thread + 1, tag = 3)
                if compute_dos != None:
                    for i,dos in enumerate(compute_dos): 
                        for j in xrange(spin): dosdata[i*spin + j] += COMM.recv(source = thread + 1, tag = i*spin + j + 5)

            if need_nrg: np.savetxt(transpath[0],energydata,delimiter=', ')
            if need_trans:
                np.savetxt(transpath[1],transdata,delimiter=', ')

            packet = [np.array(energydata)]
            if need_trans: packet.append(np.array(transdata))
            if compute_dos != None:
                for i,dos in enumerate(compute_dos): 
                    for j in xrange(spin):
                        packet.append(np.array(dosdata[i*spin + j]))
                        np.savetxt(transpath[i*spin + j + 2 - (1 if not need_trans else 0)],dosdata[i*spin + j],delimiter=', ')
            
            for thread in xrange(NUMTHREADS - 1):
                COMM.send(packet, dest = thread + 1, 
                          tag = 4)

        else:
            if need_nrg:
                COMM.send(energydata, dest = 0, tag = 2)
            if need_trans:
                COMM.send(transdata, dest = 0, tag = 3)
            if compute_dos != None:
                for i,dos in enumerate(compute_dos):
                    for j in xrange(spin):
                        COMM.send(dosdata[i*spin + j], dest = 0, tag = i*spin + j + 5)

            packet = COMM.recv(source = 0, tag = 4)


    if cond_nrgs != None:

        if not need_trans: print 'need trans!'
        else:


            energydata,transdata = np.array(packet[0]),np.array(packet[1])
        
            energydata,transdata = cfg.remove_outliers((energydata,transdata),[100.])

            condpath = "%s/conductance.dat" % transdir

            if os.path.isfile(condpath) and not condoverwrite: 
                _fermidata,_conductance = np.loadtxt(condpath,delimiter=', ')
                _fermidata = [int(nrg) for nrg in _fermidata]
            else:
                _fermidata = _conductance = []

            num_pts = len(cond_nrgs)

            if NUMTHREADS > 1:
                threadpts = num_pts/NUMTHREADS
                ptsleft = num_pts % NUMTHREADS
                if RANK < ptsleft: threadpts += 1
                cond_index = RANK*threadpts + (0 if RANK < ptsleft else ptsleft)
                cond_nrgs = cond_nrgs[cond_index:cond_index + threadpts]
            else: 
                threadpts = num_pts
                ptsleft = 0

            fermidata,conddata = [],[]

            for nrg in cond_nrgs:
                #if abs(nrg*cfg.Const.e - energydata.min())
                nrg = int(round(nrg*cfg.Const.e/res))
                try: 
                    _fermidata.index(nrg)
                except:
                    fermidata.append(nrg)

                    nrg *= res

                    integrand = array([_trans*(cfg.fermi(_nrg,nrg + const.eV,T) - cfg.fermi(_nrg,nrg,T))
                                 for _nrg,_trans in zip(energydata,transdata)])

                    conddata.append((2 if spin == 1 else 1)*const.e**2/const.h/const.eV*simps(integrand, energydata))
                    
            if RANK == 0:
                for thread in xrange(NUMTHREADS - 1):
                    fermidata += COMM.recv(source = thread + 1, tag = 5)
                    conddata += COMM.recv(source = thread + 1, tag = 6)

                if len(_fermidata) > 0:
                    for nrg,_cond in zip(fermidata,conddata):
                        idx = cfg.closest_to_val(_fermidata,nrg) + 1
                        _fermidata = np.insert(_fermidata,idx,nrg)
                        _conductance = np.insert(_conductance,idx,_cond)
                else:
                    _fermidata = fermidata
                    _conductance = conddata

                for thread in xrange(NUMTHREADS - 1):
                    COMM.send((_fermidata,_conductance), dest = thread + 1, 
                              tag = 4)

            else:
                COMM.send(fermidata, dest = 0, tag = 5)
                COMM.send(conddata, dest = 0, tag = 6)

                (_fermidata,_conductance) = COMM.recv(source = 0, tag = 4)

            packet.append(np.array(_fermidata)*res)
            packet.append(np.array(_conductance))
    return (packet,transdir)

def bandStructure(Hs,disp,numK):
    """
    Calculates bandstructure of periodic system with all outgoing interactions 
    between a unit cell and other unit cells. disp is list of displacements of 
    these unit cells in same order. first of Hs/disp should be center unit cell.
    numK ks uniformly distributed on line from Gamma pt in BZ through K to M.
    """

    Kx,Ky = (2.*np.pi/3./const.a,2.*np.pi/3.**1.5/const.a)

    ks = []
    for i in xrange(numK):
        ks.append([Kx*(i + .5)*1.5/numK,
                   Ky*(i+.5)*1.5/numK])

    nrgs = []
    for k in ks:
        H0 = Hs[0].copy()
        for H,d in zip(Hs[1:],disp[1:]):
            H0 += (H*np.exp(1j*(k[0]*d[0] + k[1]*d[1])))

        #print H0,np.exp(1j*(k[0]*d[0] + k[1]*d[1]))

        nrgs.append(eigvalsh(H0))

    return (ks,nrgs)
            
           
def printbool():
    print cfg.Bools.Hpassivation
    print cfg.Bools.sixband
