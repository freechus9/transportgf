import sys
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from mpi4py import MPI
from subprocess import Popen
import os
import transportgf.core.support as supp
import transportgf.core.cfg as cfg

width = 15
length = 2
edge = 'ac'
lattice = cfg.get_lattice(width,length,edge)

cfg.Bools.sixband = False
cfg.Bools.Hpassivation = False
cfg.load()

data1 = supp.trans_dos([-2*cfg.Const.e,2*cfg.Const.e],
                       cfg.Const.e*.01,edge=edge,need_trans = True,
                       compute_dos = range(length*width),lattice=np.array(lattice))[0]

data1[2] = np.sum(data1[2:],0)
data1 = cfg.remove_outliers(data1,[100.,1.e20])

cfg.Bools.sixband = True
cfg.Bools.Hpassivation = True
cfg.load()

data2 = supp.trans_dos([-2*cfg.Const.e,2*cfg.Const.e],
                       cfg.Const.e*.01,edge=edge,need_trans = True,
                       compute_dos=range(length*width),lattice=np.array(lattice))[0]

data2[2] = np.sum(data2[2:],0)
data2 = cfg.remove_outliers(data2,[100.,1.e20])

if cfg.RANK == 0:
    fig = plt.figure(1,figsize=(24,9))
    ax = fig.add_subplot(121)
    ax.set_title('Single pz Basis')
    ax.set_xlabel('Carrier Energy (eV)')
    ax.plot(data1[0]/cfg.Const.e,data1[1],'r',lw=3,alpha=.5)
    ax.set_ylabel('T(E)')
    ax = ax.twinx()
    ax.plot(data1[0]/cfg.Const.e,data1[2]*cfg.Const.e,'b',lw=3,alpha=.5)
    ax.set_ylabel('DOS (1/eV)')
    ax = fig.add_subplot(122)
    ax.set_title('pdd Basis with H Passivation')
    ax.set_xlabel('Carrier Energy (eV)')
    ax.plot(data2[0]/cfg.Const.e,data2[1],'g',lw=3,alpha=.5)
    ax.set_ylabel('T(E)')
    ax = ax.twinx()
    ax.plot(data2[0]/cfg.Const.e,data2[2]*cfg.Const.e,'k',lw=3,alpha=.5)
    ax.set_ylabel('DOS (1/eV)')

    plt.savefig('compareBasis.png',format='png')
