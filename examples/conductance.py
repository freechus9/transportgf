import sys
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from mpi4py import MPI
from subprocess import Popen
import os
import transportgf.core.support as supp
import transportgf.core.cfg as cfg

width = 15
length = 2
edge = 'ac'
lattice = cfg.get_lattice(width,length,edge)

data = supp.trans_dos([-5.,5.],
                       cfg.Const.e*.01,edge=edge,need_trans = True,#compute_dos = xrange(length*width),
                       cond_nrgs = np.arange(-4,4,.05),condoverwrite = True,transoverwrite = False,lattice=lattice,T = 300.)[0]

nrg,trans = cfg.remove_outliers((data[0],data[1]),[20.])
ferminrg,cond = cfg.remove_outliers((data[2],data[3]),[20.])

if cfg.RANK == 0:
    fig = plt.figure(1)
    ax = fig.add_subplot(111)
    ax.plot(nrg,trans,'r',lw=3,alpha=.5,ls='--',label = "T")
    ax.legend(loc = 'lower left')
    ax.set_ylabel("Transmission Function")
    ax = ax.twinx()
    ax.plot(ferminrg,cond/(2.*cfg.Const.e**2/cfg.Const.h),'b',lw=3,alpha=.5,label = "G")
    ax.set_xlabel("Energy (eV)")
    ax.set_ylabel("Conductance (2e^2/h)")
    ax.legend(loc = 'lower right')
    plt.savefig('testfig.png',format='png')
