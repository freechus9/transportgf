import sys
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from mpi4py import MPI
from subprocess import Popen
import os
import transportgf.core.support as supp
import transportgf.core.cfg as cfg

width = 31
length = 20
edge = 'ac'
lattice = cfg.get_lattice(width,length,edge,pores = [[.6e-9,.5]])
center = (cfg.get_pos(length-1,1,edge)[0]/2.,
          cfg.get_pos(0,width-1,edge)[1]/2.)

potpath = 'gated_vplane.dat'

pot = (cfg.gen_pot(potpath),potpath,(5.e-9,5.e-9),(center[0],center[1]))

data1 = supp.trans_dos([-1.*cfg.Const.e,1.*cfg.Const.e],
                       cfg.Const.e*.002,edge=edge,need_trans = True,#compute_dos = xrange(length*width),
                       cond_nrgs = np.arange(-.75,.75,.01),condoverwrite = True,
                       transoverwrite = True,lattice=lattice,T = 300.)[0]

data2 = supp.trans_dos([-1.*cfg.Const.e,1.*cfg.Const.e],
                       cfg.Const.e*.002,edge=edge,need_trans = True,pot = pot,
                       cond_nrgs = np.arange(-.75,.75,.01),condoverwrite = True,
                       transoverwrite = True,lattice=lattice,T = 300.)[0]

if cfg.RANK == 0:
    fig = plt.figure(1,figsize=(12,9))
    ax = fig.add_subplot(211)

    ax.plot(data1[2]/cfg.Const.e,data1[3]/1.e-6,color='r',lw=2,alpha=.5)
    ax.plot(data2[2]/cfg.Const.e,data2[3]/1.e-6,color='b',lw=2,alpha=.5)
    ax.set_ylabel('G (uS)')

    ax = fig.add_subplot(212)

    ax.plot(data1[2]/cfg.Const.e,(data2[3] - data1[3])/1.e-6,color='b',lw=2,alpha=.5)
    ax.set_xlabel('Fermi Energy (eV)')
    ax.set_ylabel('deltaG (uS)')

    fig.savefig('deltaG-DNA.png')
    fig.clf()

    ax = fig.add_subplot(111)

    x = np.arange(0.,2.*center[0],1.e-11)
    y = np.arange(0.,2.*center[1],1.e-11)

    x,y = np.meshgrid(x,y)

    z = np.zeros((len(x),len(x[0])))

    for i in xrange(len(x)):
        for j in xrange(len(x[0])):
            z[i,j] = cfg.get_pot(pot,x[i,j],y[i,j])

    mesh = ax.pcolormesh(x/1.e-9,y/1.e-9,z*1000.)
    ax.set_xlim(0,x.max()/1.e-9)
    ax.set_ylim(0,y.max()/1.e-9)
    ax.set_xlabel('x (nm)')
    ax.set_ylabel('y (nm)')
    plt.colorbar(mesh,label='Potential (mV)')

    fig.savefig('potential.png',format='png')
