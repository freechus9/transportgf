import sys
import numpy as np
import matplotlib.pyplot as plt
from mpi4py import MPI
from subprocess import Popen
import os
import support as supp
import cfg as cfg

snapshot = int(sys.argv[1])
run = int(sys.argv[2])

width = 63
length = 48
edge = 'ac'
lattice = cfg.get_lattice(width,length,edge,shape='QPC',pores=[[1.2e-9,int(.75*width)/float(width)]],pinch=.15,extent=.2)
#lattice = cfg.get_lattice(width,length,edge,shape='QPC',pores=[[1.2e-9,.5]],pinch=.15,extent=.2)

offset = 0.
#potpath = 'pnaspotmaps/rerunDNA8x10/vplane8x10D%s.dat' % snapshot
potpath = 'stretch/run1/%i/vplane%s.dat' % (run,snapshot)
pot = (cfg.gen_pot(potpath,offset),potpath,(5.e-9,2.5e-9),(cfg.get_pos(length-1,1,'ac')[0]/2,int(.75*width)/float(width)*cfg.get_pos(0,width-1,'ac')[1]))
#pot = (cfg.gen_pot(potpath,offset),potpath,(5.e-9,2.5e-9),(cfg.get_pos(length-1,1,'ac')[0]/2,cfg.get_pos(0,width-1,'ac')[1]/2))

#data = supp.trans_dos([-.6*cfg.Const.e,.6*cfg.Const.e],
#                       cfg.Const.transres,edge=edge,need_trans = True,
#                       lattice=lattice,pot = pot)[0]

packet = supp.gather_data([-.6,.6], edge, lattice, pot = pot,
              need_trans = True,
              cond_nrgs = np.arange(-.2,.2,.02),T = 300.,EF = 0.,
              cond_only = False,transoverwrite = True)[0]

if cfg.RANK == 0:
    fig = plt.figure(1)
    ax = fig.add_subplot(311)
    ax.plot(packet[2][0],packet[2][1])
    '''
    X,Y = np.meshgrid(np.arange(0.,10.e-9,.1e-9),np.arange(0.,8.e-9,.1e-9))
    Z = np.zeros((len(X),len(X[0])))

    for i in xrange(len(X)):
        for j in xrange(len(X[0])):
            Z[i,j] = cfg.get_pot(pot,X[i,j],Y[i,j])

    ax = fig.add_subplot(312)
    mesh = ax.pcolormesh(X,Y,Z)
    plt.colorbar(mesh)

    ax = fig.add_subplot(313)
    cfg.print_dev_conf(lattice,edge,subplot=ax,fig=fig)
    '''
    fig.savefig('transtest.png',format='png')
