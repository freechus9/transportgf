import sys
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from mpi4py import MPI
from subprocess import Popen
import os
import transportgf.core.support as supp
import transportgf.core.cfg as cfg

width = 11
length = 4
edge = 'ac'
lattice = cfg.get_lattice(width,length,edge)

data = supp.trans_dos([-3*cfg.Const.t,3*cfg.Const.t],
                       cfg.Const.e*.01,edge=edge,need_trans = True,compute_dos = xrange(length*width),
                       lattice=lattice)[0]

data[2] = np.sum(data[2:],0)
data = cfg.remove_outliers(data,[100.,1.e20])

if cfg.RANK == 0:
    fig = plt.figure(1)
    ax = fig.add_subplot(111)
    ax.plot(data[0]/cfg.Const.e,data[1],'r',lw=3,alpha=.5)
    ax = ax.twinx()
    ax.plot(data[0]/cfg.Const.e,data[2],'b',lw=3,alpha=.5)

    '''
    X,Y = np.meshgrid(np.arange(0.,10.e-9,.1e-9),np.arange(0.,8.e-9,.1e-9))
    Z = np.zeros((len(X),len(X[0])))

    for i in xrange(len(X)):
        for j in xrange(len(X[0])):
            Z[i,j] = cfg.get_pot(pot,X[i,j],Y[i,j])

    ax = fig.add_subplot(312)
    mesh = ax.pcolormesh(X,Y,Z)
    plt.colorbar(mesh)

    ax = fig.add_subplot(313)
    cfg.print_dev_conf(lattice,edge,subplot=ax,fig=fig)
    '''
    #plt.show()
    plt.savefig('testfig.png',format='png')
