import support as supp
import cfg
import numpy as np
from matplotlib import pyplot as plt
from scipy.interpolate import interp1d
from scipy.interpolate import griddata
from scipy.integrate import simps
from scipy.integrate import quadrature
from mpi4py import MPI
import random
import sys

prestr = "A" if len(sys.argv) > 1 else ""
sys.stdout = open('%sFMlog.txt' % prestr,'w')

COMM = MPI.COMM_WORLD
RANK = COMM.Get_rank()
SIZE = COMM.Get_size()

def wait(): COMM.recv(source=0,tag=1)
def release(): return [COMM.send('go',dest=i+1,tag=1) for i in xrange(SIZE - 1)]

np.set_printoptions(precision = 2)

width = 2
length = 3
edge = 'zz'

lattice = cfg.get_lattice(width,length,edge,shape='GNR')

B = 0.

U = 5.4
Z = 0.#-5.e-3*cfg.Const.t

def A(r):
    return np.array([0.,B*r[0],0.])

for phase in ["AFM"]:

    if phase == "AFM":
        occupation = np.array([((0. if i % 2 == 0 else 1) if (i/2) % 2 == 0 else (0. if i % 2 == 1 else 1))
                                 if (i/(2*width)) % 2 == 0 else 
                                 ((1. if i % 2 == 0 else 0.) if (i/2) % 2 == (width % 2) else (1. if i % 2 == 1 else 0.))
                                 for i in xrange(width*(length-1)*2)]*5) - .5
        #occupation = np.loadtxt("AFMocc.dat")
    elif phase == "FM":
        occupation = np.array([((0. if i % 2 == 0 else 1) if (i/2) % 2 == 0 else (0. if i % 2 == 1 else 1))
                                 if (i/(2*width)) % 2 == ((i%(2*width))/(width)) else 
                                 ((1. if i % 2 == 0 else 0.) if (i/2) % 2 == (width % 2) else (1. if i % 2 == 1 else 0.))
                                 for i in xrange(width*(length-1)*2)]*5)
        #occupation = np.loadtxt("FMocc.dat")
        #occupation += .5

    #occupation = np.array([1. if i % 2 == 1. else 1. for i in xrange(length*width*10)])

    magnetization = [(occupation[2*i] - occupation[2*i + 1]) for i in xrange((length-1)*width)]

    #print occupation
    error = 1.

    x = -1
    thresh = 1.e-3
    while error > thresh:
    #for q in xrange(1):
        if RANK == 0:
            #print np.reshape(occupation[:(length-1)*width*2],((length-1),width,2))
            print np.reshape(magnetization,(length-1,width))
        EF = np.sum(occupation[:2*(length-1)*width])*U/(width*(length-1.))/2.*cfg.Const.e
        if RANK == 0: print "EF: ",EF
        x += 1
        overwrite = True if x == 0 else True
        tmp = supp.trans_dos([-3.*cfg.Const.t + U*cfg.Const.e/2.,3*cfg.Const.t + U*cfg.Const.e/2.],cfg.Const.transres,edge=edge,need_trans = False ,
                              compute_dos = xrange(width*(length-1)),transoverwrite = overwrite,gfoverwrite = overwrite,
                              nosave = True,lattice=lattice,U = (U,np.array(occupation),phase),spin = 2)
        data = tmp[0]

        index = []
        for datum in data[1:]:
            for j,val in enumerate(datum):
                if val > 1.e21 or val < 0.:
                    index.append(j)
                elif j > 0 and j < len(datum) - 1:
                    slope1 = (datum[j] - datum[j-1])/(data[0][j] - data[0][j-1])
                    slope2 = (datum[j+1] - datum[j])/(data[0][j+1] - data[0][j])
                    slope3 = (datum[j+1] - datum[j-1])/(data[0][j+1] - data[0][j-1])

                    if abs((abs(slope1/slope2) - 1)) < .05 and abs(slope3/slope1) < .1:
                        index.append(j)
        if len(index) > 0: data = np.delete(data,list(set(index)),1)

        nmax = 2

        for n in xrange(1,nmax + 1):

            #identify singularities
            maxindex = []
            maxindex2 = []
            maxthresh = .05
            for datum in data[1:]:
                tmpmaxindex = []
                tmpmaxindex2 = []
                for j,val in enumerate(datum):
                    if j > 0 and j < len(datum) - 1:

                        if datum[j] > 1.e17 and datum[j] > datum[j-1] and datum[j] > datum[j+1]:
                            slope1 = (datum[j] - datum[j-1])/(data[0][j] - data[0][j-1])
                            slope2 = (datum[j+1] - datum[j])/(data[0][j+1] - data[0][j])
                            slope3 = (datum[j+1] - datum[j-1])/(data[0][j+1] - data[0][j-1])
                            
                            if (abs(datum[j]/datum[j-1] - 1.) > 1.e-3 or
                                abs(datum[j+1]/datum[j] - 1.) > 1.e-3 or
                                abs(datum[j+1]/datum[j-1] - 1.) > 1.e-3):

                                maxindex.append(j)
                                maxindex2.append(data[0][j]/cfg.Const.e)
                                tmpmaxindex.append(j)
                                tmpmaxindex2.append(data[0][j]/cfg.Const.e)

                                """
                                if RANK == 0: 
                                    print '\n\n\n',maxindex2[-1]
                                    print data[:,j-1],data[:,j],data[:,j+1]
                                    print slope1,slope2,slope3
                                    print slope1/slope2,slope1/slope3,slope2/slope3
                                    print datum[j]/datum[j-1],datum[j+1]/datum[j],datum[j+1]/datum[j-1]
                                """
            maxindex = list(set(maxindex))
            print "NUMMAX:",len(maxindex)

            if len(maxindex) == 0: break



            delwhere = []
            for maximum in maxindex:
                #if RANK == 0: print data[0][maximum]/cfg.Const.e
                tmp = supp.trans_dos([data[0][maximum] - 30*cfg.Const.transres/(10.**n),data[0][maximum] + 30*cfg.Const.transres/(10**n)],
                                     cfg.Const.transres/(10**n),edge=edge,need_trans = False,
                                      compute_dos = xrange(width*(length-1)),transoverwrite = True,
                                      gfoverwrite = True,
                                      nosave = True,lattice=lattice,
                                      U = [U,occupation,phase],Z = [Z],spin = 2)[0]
                index = []
                for datum in tmp[1:]:
                    for j,val in enumerate(datum):
                        if val > thresh/(2*cfg.Const.transres/10**nmax) or val < 0. or not val == val:
                            index.append(j)

                        # Identify artifacts
                        elif j > 0 and j < len(datum) - 1:
                            slope1 = (datum[j] - datum[j-1])/(tmp[0][j] - tmp[0][j-1])
                            slope2 = (datum[j+1] - datum[j])/(tmp[0][j+1] - tmp[0][j])
                            slope3 = (datum[j+1] - datum[j-1])/(tmp[0][j+1] - tmp[0][j-1])

                            if abs((abs(slope1/slope2) - 1)) < .05 and abs(slope3/slope1) < .1:
                                index.append(j)

                if len(index) > 0: tmp = np.delete(tmp,list(set(index)),1)


                #if RANK == 0: print tmp[0]/cfg.Const.e
                for nrg in tmp[0]:
                    where = np.where((data[0]/cfg.Const.e).round(12) == round(nrg/cfg.Const.e,12))[0].tolist()
                    if len(where) > 0:
                        delwhere += where

                data = np.append(data,tmp,1)

            if len(delwhere) > 0: data = np.delete(data,list(set(delwhere)),1)
            data = data[:,np.argsort(data[0])]

        if RANK == 0:
            fig = plt.figure(1)
            ax = fig.add_subplot(111)
            ax.plot(data[0]/cfg.Const.e,np.sum(data[1:],0))
            ax.vlines([np.sum(occupation[:2*(length-1)*width])*U/4./2.],0.,np.sum(data[1:],0).max())
            fig.savefig('NEGF.png',format='png')
            fig.clear()
            release()
        else: wait()

        alpha = 1.
        old = occupation.copy()
        
        for n,dat in enumerate(data[0]):
            if dat > EF: data[1:][:,n] *= 0.

        occupation = np.array([simps(data[i+1],data[0]) for i in xrange(2*(length-1)*width)]*5)
        np.savetxt("%socc.dat" % phase,occupation)
        #occupation *= (10./np.sum(occupation[:2*(length-1)*width],0))

        magnetization = [(occupation[2*i] - occupation[2*i + 1])/2. for i in xrange(length*width*2/3)]
        error = np.linalg.norm(occupation - old,np.inf)

        if RANK == 0:
            print np.reshape(magnetization,(length-1,width))
            print error
            nrg =  (sum([simps(data[i+1]*data[0],data[0])  
                         for i in xrange(2*(length-1)*width)]) 

                    - U*cfg.Const.e*sum([occupation[2*i]*
                                           occupation[2*i+1]
                                           for i in xrange((length-1)*width)]))
            print "nrg:",nrg/cfg.Const.e
            print '\n\n\n'

    """
    if RANK == 0:
        fig = plt.figure(1)
        ax = fig.add_subplot(121 + (1 if phase == "FM" else 0))

        
    
        

        ax.plot(data[0]/cfg.Const.e,np.sum(data[1:],0)) 
        #ax = ax.twinx()
        #spl = UniSpl(data[0]/cfg.Const.e,np.sum(data[1:],0))
        #ax.plot(data[0]/cfg.Const.e,derivative(data[0],data[1],k)) 

plt.show()
    """
