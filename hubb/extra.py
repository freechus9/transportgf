
def gather_data(nrgs, edge, lattice, pot = None,
              compute_dos = None, need_trans = False,
              trans_split = None,cond_nrgs = None,T = 300.,
              EF = 0.,cond_only = False,transoverwrite = False):

    if cond_only and cond_nrgs != None:
        nrg = nrgs[0]
        dnrg = np.ptp(nrgs)/trans_split
        transdir = trans_dos([nrg*cfg.Const.e,(nrg + dnrg)*cfg.Const.e], 
                          cfg.Const.transres, edge, lattice, pot = pot, dirname = True)
        condpath = "%s/%.2f/conductance.dat" % (transdir,T)
        if os.path.isfile(condpath) and not bools.condoverwrite: 
            _fermidata,_conductance = np.loadtxt(condpath,delimiter=', ')
            _fermidata = [int(nrg) for nrg in _fermidata]
            try:
                for nrg in cond_nrgs: 
                    _fermidata.index(int(round(nrg*cfg.Const.e/cfg.Const.transres)))
                return [np.array(_fermidata),np.array(_conductance)]
            except:
                print 'need to calc boo'
                

    if trans_split != None:
        nrg = nrgs[0]
        dnrg = np.ptp(nrgs)/trans_split
        nrg2 = nrg + dnrg
        nrg = round(nrg,5)
        nrg2 = round(nrg2,5)
        if nrg == 0.: nrg = 0.
        if nrg2 == 0.: nrg2 = 0.
        tmp = trans_dos([nrg*cfg.Const.e,nrg2*cfg.Const.e],cfg.Const.transres,edge,lattice,pot = pot,
                           compute_dos = compute_dos, need_trans = need_trans)
        packet = [data.tolist() for data in tmp[0]]
        transdir = tmp[1]
        print transdir
        print ' gotdatrans'
        for i in xrange(trans_split - 1):
            nrg = nrgs[0] + (i + 1)*dnrg
            nrg2 = nrg + dnrg
            nrg = round(nrg,5)
            nrg2 = round(nrg2,5)
            if nrg == 0.: nrg = 0.
            if nrg2 == 0.: nrg2 = 0.
            print nrg,nrg2
            packet = [a + b for a,b in zip(packet,[data.tolist() for data in trans_dos([nrg*cfg.Const.e,nrg2*cfg.Const.e],cfg.Const.transres,edge,lattice,pot = pot,
                           compute_dos = compute_dos, need_trans = need_trans)[0]])]
    else:
        packet,transdir = trans_dos([nrgs[0]*cfg.Const.e,nrgs[1]*cfg.Const.e],cfg.Const.transres,edge,lattice,pot = pot,
                           compute_dos = compute_dos, need_trans = need_trans,transoverwrite = transoverwrite)
        packet = list(packet)

    if cond_nrgs != None and need_trans:
        energydata,transdata = packet[0],packet[1]

        condpath = "%s/conductance.dat" % transdir
        if os.path.isfile(condpath) and not bools.condoverwrite: 
            _fermidata,_conductance = np.loadtxt(condpath,delimiter=', ')
            _fermidata = [int(nrg) for nrg in _fermidata]
        else:
            _fermidata = _conductance = []

        num_pts = len(cond_nrgs)

        if NUMTHREADS > 1:
            threadpts = num_pts/NUMTHREADS
            ptsleft = num_pts % NUMTHREADS
            if RANK < ptsleft: threadpts += 1
            cond_index = RANK*(threadpts + (0 if RANK < NUMTHREADS else 1))
            cond_nrgs = cond_nrgs[cond_index:cond_index + threadpts]
        else: 
            threadpts = num_pts
            ptsleft = 0

        fermidata,conddata = [],[]

        print cond_nrgs,RANK
        for nrg in cond_nrgs:
            nrg = int(round(nrg*cfg.Const.e/cfg.Const.transres))
            try: 
                _fermidata.index(nrg)
            except:
                fermidata.append(nrg)

                nrg *= cfg.Const.transres

                integrand = array([_trans*(cfg.fermi(_nrg,nrg,T) - cfg.fermi(_nrg + const.eV,nrg,T))
                             for _nrg,_trans in zip(energydata,transdata)])

                conddata.append(2*const.e**2/const.h/const.eV*simps(integrand, energydata))
                


        if RANK == 0:
            for thread in xrange(NUMTHREADS - 1):
                fermidata += COMM.recv(source = thread + 1, tag = 5)
                conddata += COMM.recv(source = thread + 1, tag = 6)

            if len(_fermidata) > 0:
                for nrg,_cond in zip(fermidata,conddata):
                    idx = cfg.closest_to_val(_fermidata,nrg) + 1
                    _fermidata = np.insert(_fermidata,idx,nrg)
                    _conductance = np.insert(_conductance,idx,_cond)
            else:
                _fermidata = fermidata
                _conductance = conddata
            
            for thread in xrange(NUMTHREADS - 1):
                COMM.send((_fermidata,_conductance), dest = thread + 1, 
                          tag = 4)

        else:
            COMM.send(fermidata, dest = 0, tag = 5)
            COMM.send(conddata, dest = 0, tag = 6)

            (_fermidata,_conductance) = COMM.recv(source = 0, tag = 4)

        packet.append((_fermidata,_conductance))
    return (packet,transdir)
"""
def conductance(nrgs, res, edge, lattice, trans_nrgs = None,split_range = None,pot = None,
                  condoverwrite = False, transoverwrite = False, gfoverwrite = False,
                  remove_outliers = None,bypass_cond = False, condskip = False, T = const.T):

    W = len(lattice[0])
    nrg0 = int(round(nrgs[0]/res))
    nrg1 = int(round(nrgs[1]/res))

    num_pts = nrg1 - nrg0 #domain is [nrg0,nrg1]

    latticeid = hash(' '.join([''.join(elem) for elem in lattice]))

    if bools.verbose: print "Calculating for lattice %s" % latticeid

    conddir = '%s/cond/%s/%i/%s' % (cfg.output_dir, edge + ('6b' if bools.sixband else '')
                                    + ('H' if bools.Hpassivation else ''), latticeid, 
                                    'nopot' if pot == None else os.path.dirname(pot[1]))


    condpath = '%s/%s %.8e %.8e %i %.2fK' % (conddir,
                                             'nopot' if pot == None else os.path.basename(pot[1]),
                                             nrgs[0],nrgs[1],num_pts,T)

    if os.path.isfile(condpath) and not condoverwrite: 
        if bools.verbose and RANK==0: print "Found %s!" % condpath
        if trans_nrgs != None:
            if split_range != None:
                dnrg = np.ptp(trans_nrgs)/split_range
                for i,nrg1 in enumerate(np.array(xrange(split_range))*dnrg - trans_nrgs[0]):
                    nrg2 = nrg1 + dnrg
                    energies = [nrg1*cfg.Const.e,nrg2*cfg.Const.e]
                    if i == 0:
                        data = trans_dos(energies,cfg.Const.transres,edge,lattice,need_trans=True,transoverwrite = transoverwrite,gfoverwrite = gfoverwrite)
                        data = [data[0].tolist(),data[1].tolist()]
                    else:
                        tmp = trans_dos(energies,cfg.Const.transres,edge,lattice,need_trans=True,transoverwrite = transoverwrite,gfoverwrite = gfoverwrite)
                        data[0] += tmp[0].tolist()
                        data[1] += tmp[1].tolist()
                energydata,transdata = np.array(data)
            else:
                energydata,transdata = trans_dos([trans_nrgs[0],trans_nrgs[1]],
                                                 const.transres,
                                                 edge = edge, lattice = lattice,
                                                 need_trans = True,pot = pot,transoverwrite = transoverwrite,gfoverwrite = gfoverwrite)
            
        else:
            energydata,transdata = trans_dos([nrgs[0] - 10*const.kB*T,nrgs[1] + 10*const.kB*T],
                                             const.transres,
                                             edge = edge, lattice = lattice,
                                             need_trans = True,pot = pot,transoverwrite = transoverwrite,gfoverwrite = gfoverwrite)
        fermidata,conddata = genfromtxt(condpath,delimiter=', ')
        return (fermidata,conddata,energydata,transdata)
    elif condskip: return (None,None)
    else:
        if bools.verbose and RANK==0: print "Did not find %s!" % condpath
        if RANK == 0: 
            cfg.mkdir(conddir)
            release()
        else: wait()

        energydata,transdata = trans_dos([nrgs[0] - 10*const.kB*T,nrgs[1] + 10*const.kB*T],
                                         const.transres,
                                         edge = edge, lattice = lattice,
                                         need_trans = True,pot = pot,transoverwrite = transoverwrite,gfoverwrite = gfoverwrite)

        if remove_outliers != None:
            index = []
            for i,trans in enumerate(transdata):
                if trans < 0. or trans > remove_outliers:
                    index.append(i)
            energydata = np.delete(energydata,index)
            transdata = np.delete(transdata,index)

        if bypass_cond: return (energydata,transdata)
        #TODO(anuj): load split trans nrg ranges a la tmp
        '''
        '''
        if NUMTHREADS > 1:
            threadpts = num_pts/NUMTHREADS
            ptsleft = num_pts % NUMTHREADS
            if RANK < ptsleft: threadpts += 1

        else: 
            threadpts = num_pts
            ptsleft = 0

        fermidata,conddata = [],[]

        for point in xrange(threadpts):
            nrg = res*(RANK*threadpts
                       + (ptsleft if RANK >= ptsleft else 0)
                       + point) + nrgs[0]

            fermidata.append(nrg)

            integrand = array([_trans*(cfg.fermi(_nrg,nrg,T) - cfg.fermi(_nrg + const.eV,nrg,T))
                         for _nrg,_trans in zip(energydata,transdata)])

            conddata.append(2*const.e**2/const.h/const.eV*simps(integrand, energydata))

        if RANK == 0:
            for thread in xrange(NUMTHREADS - 1):
                fermidata += COMM.recv(source = thread + 1, tag = 5)
                conddata += COMM.recv(source = thread + 1, tag = 6)
            
            for thread in xrange(NUMTHREADS - 1):
                COMM.send((fermidata,conddata), dest = thread + 1, 
                          tag = 4)

            savetxt(condpath,(fermidata,conddata),delimiter=', ')

        else:
            COMM.send(fermidata, dest = 0, tag = 5)
            COMM.send(conddata, dest = 0, tag = 6)

            (fermidata,conddata) = COMM.recv(source = 0, tag = 4)

        return (fermidata,conddata,energydata,transdata)

"""
