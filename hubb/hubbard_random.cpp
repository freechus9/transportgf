/*
   Copyright (c) 2009-2014, Jack Poulson
   All rights reserved.

   This file is part of Elemental and is under the BSD 2-Clause License, 
   which can be found in the LICENSE file in the root directory, or at 
   http://opensource.org/licenses/BSD-2-Clause
*/
// NOTE: It is possible to simply include "elemental.hpp" instead
//#include <iomanip>
#include "elemental.hpp"
#include ELEM_DIAGONALSCALE_INC
#include ELEM_HEMM_INC
#include ELEM_HERK_INC
#include ELEM_FROBENIUSNORM_INC
#include ELEM_IDENTITY_INC
#include <cblas.h>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <complex>
#include <sstream>
#include <iostream>
#include <string>
#include <math.h>
using namespace std;
using namespace elem;

typedef double Real;
typedef Complex<Real> C;

struct by_nrg { 
        bool operator()(vector<double> const &a, vector<double> const &b) const { 
                    return a.at(0) < b.at(0);
                        }
};

int main( int argc, char* argv[] )
{
    Initialize( argc, argv );

    try 
    {
        const Int n = Input("--size","size of matrix",100);
        const int BW = Input("--BW","blockwidth",1);
        const Int N1 = Input("--n1","size of matrix",100);
        const Int N2 = Input("--n2","size of matrix",100);
        const Int N3 = Input("--n3","blockwidth",1);
        const Int N4 = Input("--n4","size of matrix",100);
        const Int N5 = Input("--n5","size of matrix",100);
        const Int N6 = Input("--n6","blockwidth",1);
        const int width = Input("--width","blockwidth",1);
        const Int threads = Input("--threads","blockwidth",1);
        const Int NUM_KS = Input("--num_ks","blockwidth",1);
        const Int khash = Input("--khash","khash",0);
        const bool print = Input("--print","print matrices?",false);
        //const Int filehash = Input("--index_hash","indices file name hash",0);
        const Int filehash = Input("--filehash","indices file name",0);

        //cerr << filename.c_str() << endl;

        ProcessInput();
        //PrintInputReport();

        Grid g( mpi::COMM_WORLD );


        const int SIZE = mpi::CommSize(mpi::COMM_WORLD);
        const int RANK = mpi::CommRank(mpi::COMM_WORLD);
        if (RANK==0) cerr << g.Height() << " " << g.Width() << endl;

        int Ns[6];

        Ns[0] = N1;
        Ns[1] = N2;
        Ns[2] = N3;
        Ns[3] = N4;
        Ns[4] = N5;
        Ns[5] = N6;

        vector<Matrix<C>> Hs;
        for (int p = 0; p < 6; p++) {

            Matrix<C> H(Ns[p],2);
            Zeros(H,Ns[p],2);
            double a,b;
            int i,j,num_elem,ind=0;
            string line;
            stringstream filename;

            filename << "/home/freechus9/Research/transportgf/output/ind/indices-" 
                     << filehash << "-" << p << "-" << threads << ".dat";
            ifstream indices(filename.str());

            while(getline(indices,line)) {

                stringstream stream(line);
                
                stream >> i;
                stream >> j; 
                stream >> a;
                stream >> b;

                
                H.Set(ind,0,C(i,j));
                H.Set(ind,1,C(a,b));
                ind++;
            }

            indices.close();
            Hs.push_back(H);
        }

        double epsilon = .001;

        int sidelen = NUM_KS;

        double a = 1.42e-10;

        //vector<C> ks;
        /*
        double dx = 4.*M_PI/3./a/double(sidelen);
        double dy = 4.*M_PI/sqrt(3.)/a/double(sidelen);

        //for (int i = 0; i < sidelen; i++) {
        for (int i = -sidelen/2; i < sidelen/2; i++) {
            for (int j = -sidelen/2; j < sidelen/2; j++) {
            //for (int j = 0; j < sidelen/2; j++) {
                double kx = dx*(static_cast<double>(i)+.5);
                double ky = dy*(static_cast<double>(j)+.5);

                //if (kx < (2.*M_PI/3./a)) {
                if (kx < 0.) {
                    if (ky > 0.) {
                        if (ky > (-kx*sqrt(3.)*(1. + epsilon))) continue;
                        else ks.push_back(C(kx,ky));
                    } else {
                        if (ky < (kx*sqrt(3.)*(1. + epsilon))) continue;
                        else ks.push_back(C(kx,ky));
                    }
                } else {
                    if (ky > 0.) {
                        //if (ky > (((4*M_PI/3./a) - kx)*sqrt(3.)*(1. + epsilon))) continue;
                        if (ky > (kx*sqrt(3.)*(1. + epsilon))) continue;
                        else ks.push_back(C(kx,ky));
                    } else {
                        if (ky < (-kx*sqrt(3.)*(1. + epsilon))) continue;
                        //if (ky < -(((4*M_PI/3./a) - kx)*sqrt(3.)*(1. + epsilon))) continue;
                        else ks.push_back(C(kx,ky));
                    }
                }
                        
            }
        }
        for (int i = 0; i < sidelen; i++) {
            for (int j = -sidelen/2; j < sidelen/2; j++) {
                double kx = dx*(static_cast<double>(i)+.5);
                double ky = dy*(static_cast<double>(j)+.5);

                if (kx < (2.*M_PI/3./a)) {
                    if (ky > 0.) {
                        if (ky > (kx*sqrt(3.)*(1. + epsilon))) continue;
                        else ks.push_back(C(kx,ky));
                    } else {
                        if (ky < (-kx*sqrt(3.)*(1. + epsilon))) continue;
                        else ks.push_back(C(kx,ky));
                    }
                } else {
                    if (ky > 0.) {
                        if (ky > (((4*M_PI/3./a) - kx)*sqrt(3.)*(1. + epsilon))) continue;
                        else ks.push_back(C(kx,ky));
                    } else {
                        if (ky < -(((4*M_PI/3./a) - kx)*sqrt(3.)*(1. + epsilon))) continue;
                        else ks.push_back(C(kx,ky));
                    }
                }
                        
            }
        }
        */
        /*
        double dx = 4.*M_PI/3./a/double(sidelen);
        double hexH = 17030979945.861198;
        double dy = 2.*hexH/double(sidelen);
        double m = (17030979945.861198 - 2.*M_PI/3./sqrt(3.)/a)/(2.*M_PI/3./a);

        //for (int i = -sidelen/2; i < sidelen/2; i++) {
        for (int i = 0; i < sidelen/2; i++) {
            //for (int j = -sidelen/2; j < sidelen/2; j++) {
            for (int j = 0; j < sidelen/2; j++) {
                double kx = dx*(static_cast<double>(i)+.5);
                double ky = dy*(static_cast<double>(j)+.5);

                if (kx > 0.) {
                    if (ky > 0.) {
                        if (ky > ((-m*kx + hexH)*(1. + epsilon))) continue;
                        else ks.push_back(C(kx,ky));
                    } else {
                        if (ky < ((m*kx - hexH)*(1. + epsilon))) continue;
                        else ks.push_back(C(kx,ky));
                    }
                } else {
                    if (ky > 0.) {
                        if (ky > ((m*kx + hexH)*(1. + epsilon))) continue;
                        else ks.push_back(C(kx,ky));
                    } else {
                        if (ky < ((-m*kx - hexH)*(1. + epsilon))) continue;
                        else ks.push_back(C(kx,ky));
                    }
                }
                        
            }
        }
        */
        DistMatrix<C,CIRC,CIRC> ks(NUM_KS,1,g);

        if (RANK == 0) {
            string kline;
            stringstream kfilename;

            kfilename << "/home/freechus9/Research/transportgf/output/ks/" << NUM_KS << "/" << khash;

            ifstream ksfile(kfilename.str());
            for (int i = 0; i < NUM_KS; i++) {
                double aa,bb;

                getline(ksfile,kline);
                stringstream kstream(kline);
                kstream >> aa;
                kstream >> bb;
                ks.SetLocal(i,0,C(aa,bb));
            }
            ksfile.close();
        }

        DistMatrix<C,VC,STAR> myks(ks);


        //int nk = ks.size();
        int nk = NUM_KS;

        if (RANK == 0) cerr << "numks: " << nk << endl;
        vector<int> nks;
        for (int i = 0; i < SIZE; i++) {
            nks.push_back(nk/SIZE + (i < (nk % SIZE) ? 1 : 0));
        }

        vector<vector<double>> nrgs_sort; 
        vector<Matrix<Real>> kmats;

        C *pkt;
        double *pkt2;
        if (RANK > 0) {
            pkt = new C[2*2*2*width*BW*nks.at(RANK)];
            pkt2 = new double[2*width*BW*2*2*width*BW*nks.at(RANK)];
        } else {
            pkt = new C[1];
            pkt2 = new double[1];
            delete [] pkt;
            delete [] pkt2;
        }
    
        for (int k = 0; k < nks.at(RANK); k++) {
            int _k = RANK*nks.at(RANK) + (RANK > ((nk % SIZE)-1) ? (nk % SIZE) : 0) + k;
            //double kx = real(ks.at(_k)),ky = imag(ks.at(_k));
            double kx = real(myks.GetLocal(k,0));
            double ky = imag(myks.GetLocal(k,0));
            //if (RANK == 0) cout << k << endl;
            for (int s = 0; s < 2; s++) {

                Matrix<C> H;
                Matrix<C> psi;
                Matrix<Real> nrg;

                Zeros(H,2*width*BW,2*width*BW);
                Zeros(psi,2*width*BW,2*width*BW);
                Zeros(nrg,2*width*BW,2*width*BW);

                for (int p = 0; p < 3; p++) {

                    double T;
                    if (p == 0) T = -sqrt(3.)*1.42e-10;
                    else if (p == 1) T = 0.;
                    else if (p == 2) T = sqrt(3.)*1.42e-10;

                    for (int q = 0; q < Ns[p + s*3]; q++) {
                        int i = int(real(Hs.at(p + s*3).Get(q,0)));
                        int j = int(imag(Hs.at(p + s*3).Get(q,0)));
                        C tmp = (Hs.at(p + s*3).Get(q,1))*exp(C(0.,1.)*(ky*T));
                        H.Update(i,j,tmp);
                    }
                }

                HermitianEig( LOWER , H , nrg , psi, ASCENDING);
                if (RANK==0) {
                    Matrix<Real>psi2(2*width*BW,2*width*BW);
                    for (int x = 0; x < 2*width*BW; x++) {
                        vector<double> a;
                        a.push_back(nrg.Get(x,0));
                        a.push_back(static_cast<double>(s));
                        a.push_back(static_cast<double>(_k));
                        a.push_back(static_cast<double>(x));
                        nrgs_sort.push_back(a);
                        for (int y = 0; y < 2*width*BW; y++) psi2.Set(y,x,real(psi.Get(y,x)*conj(psi.Get(y,x))));
                    }
                    kmats.push_back(psi2);
                } else {
                    for (int x = 0; x < 2*width*BW; x++) {
                        pkt[k*2*2*width*2 + s*2*2*width*BW + x*2] = C(nrg.Get(x,0),static_cast<double>(s));
                        pkt[k*2*2*width*2 + s*2*2*width*BW + x*2 + 1] = C(static_cast<double>(_k),static_cast<double>(x));
                        for (int y = 0; y < 2*width*BW; y++) pkt2[k*2*2*width*BW*(2*width*BW) + s*2*width*BW*2*width*BW + x*2*width*BW + y] = real(psi.Get(y,x)*conj(psi.Get(y,x)));
                    }
                }
            }
        }
        if (RANK == 0) {
            for (int proc = 1; proc < SIZE; proc++) {

                C *_pkt = new C[2*2*2*width*BW*nks.at(proc)];
                double *_pkt2 = new double[2*width*BW*2*2*width*BW*nks.at(proc)];

                mpi::Recv(_pkt,2*2*2*width*BW*nks.at(proc),proc,mpi::COMM_WORLD);
                mpi::Recv(_pkt2,2*width*BW*2*2*width*BW*nks.at(proc),proc,mpi::COMM_WORLD);

                for (int k =0; k < nks.at(proc); k++) {
                    for (int l = 0; l < 2*2*width*BW; l++) {
                        vector<double> a;

                        a.push_back(real(_pkt[k*2*2*width*BW*2 + l*2]));
                        a.push_back(imag(_pkt[k*2*2*width*BW*2 + l*2]));
                        a.push_back(real(_pkt[k*2*2*width*BW*2 + l*2 + 1]));
                        a.push_back(imag(_pkt[k*2*2*width*BW*2 + l*2 + 1]));

                        nrgs_sort.push_back(a);
                    }

                    for (int s = 0; s < 2; s++) {

                        Matrix<Real> psi(2*width*BW,2*width*BW);
                        
                        for (int p = 0; p < 2*width*BW; p++)
                            for (int q = 0; q < 2*width*BW; q++)
                                psi.Set(q,p,_pkt2[k*2*2*width*BW*2*width*BW + s*2*width*BW*2*width*BW + p*2*width*BW + q]);

                        kmats.push_back(psi);
                    }
                }

                delete [] _pkt;
                delete [] _pkt2;
            }
        } else {
            mpi::Send(pkt,2*2*2*width*BW*nks.at(RANK),0,mpi::COMM_WORLD);
            mpi::Send(pkt2,2*width*BW*2*2*width*BW*nks.at(RANK),0,mpi::COMM_WORLD);
            delete [] pkt;
            delete [] pkt2;
        }
        if (RANK == 0) {

            sort(nrgs_sort.begin(),nrgs_sort.end(),by_nrg()); 

            cerr << "sorted" << endl;

            int s,k,x;
            double nrg;
            Matrix<Real> occupation(2*2*width*BW,1),energy(2*width*BW,1);

            Zeros(occupation,2*2*width*BW,1);
            Zeros(energy,2*width*BW,1);

            for (int i = 0; i < nk*2*width; i++) {
                nrg = nrgs_sort.at(i).at(0);
                s = static_cast<int>(nrgs_sort.at(i).at(1) + .1);
                k = static_cast<int>(nrgs_sort.at(i).at(2) + .1);
                x = static_cast<int>(nrgs_sort.at(i).at(3) + .1);

                for (int j = 0; j < 2*width*BW; j++) {
                    occupation.Update(j*2 + s,0,kmats.at(2*k + s).Get(j,x)/nk);
                    energy.Update(j,0,kmats.at(2*k + s).Get(j,x)/nk*nrg);
                }
            }

            //Print(occupation);
            stringstream occerr,nrgout;
            occerr << "/home/freechus9/Research/transportgf/output/tmp/occ-" << filehash << "-" << threads;
            nrgout << "/home/freechus9/Research/transportgf/output/tmp/nrg-" << filehash << "-" << threads;
            Write(occupation,occerr.str(),ASCII);
            Write(energy,nrgout.str(),ASCII);
        }

    }
    catch( exception& e ) { ReportException(e); }

    Finalize();
    return 0;
}
