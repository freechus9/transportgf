import cfg
import numpy as np
import scipy.sparse as sparse
from numpy.linalg import eig
from subprocess import Popen,PIPE
import time
from itertools import repeat
import sys
import matplotlib.pyplot as plt
import os
from scipy.interpolate import RectBivariateSpline as RBS
import support as supp
from mpi4py import MPI
import random
COMM = MPI.COMM_WORLD           # MPI Init
RANK = COMM.Get_rank()          # Each thread gets own rank
NUMTHREADS = COMM.Get_size()      # Each thread knows numprocs
print RANK
def wait(): COMM.recv(source=0,tag=1)
def release(): return [COMM.send('go',dest=i+1,tag=1) for i in xrange(NUMTHREADS - 1)]

if len(sys.argv) > 1:
    THREADS = int(sys.argv[1])
    width = int(sys.argv[2])
    length = int(sys.argv[3])
    phase = str(sys.argv[4])
    U = float(sys.argv[5])
    num_ks = int(sys.argv[6])
    B = float(sys.argv[7])
    tol = float(sys.argv[8])
    current = float(sys.argv[9])

BW = (3 if cfg.Bools.sixband else 1)

def A(r):
    return np.array([0.,B*r[0],0.])

def count_nonzero(iterable):
    counter = 0
    indices = []
    for z,p in enumerate(iterable): 
        if p != 0.:  
            counter += 1
            indices.append(z)

    return (counter,indices)

def get_occupation(lattice,occupation,U=0.,Z=None,khash=0):
    times = []
    start = time.time()
    
    N = 0

    for col in lattice:
        for row in col:
            if row != 'O': N += 1

    error = 1.
    start_time = time.time()

    H = cfg.Hamiltonian(lattice,edge,spin=2,U=(U,occupation*10),Z = mag*10).todense()/cfg.Const.e

    # hack for bw = 1, make for bw=3
    Hs = [H[::2,::2][2*width:4*width,:2*width],
          H[::2,::2][2*width*BW:4*width*BW,2*width*BW:4*width*BW],
          H[::2,::2][2*width*BW:4*width*BW,4*width*BW:6*width*BW],
          H[1::2,1::2][2*width:4*width,:2*width],
          H[1::2,1::2][2*width*BW:4*width*BW,2*width*BW:4*width*BW],
          H[1::2,1::2][2*width*BW:4*width*BW,4*width*BW:6*width*BW]]

    N1,N2,N3,N4,N5,N6 = 0,0,0,0,0,0

    for i,H in enumerate(Hs):

        H = sparse.lil_matrix(H)

        x_ind,y_ind = H.nonzero()

        filename = "%i-%i-%.2f-%.2f-%s-%i-%.2e" % (width,length,B,U,phase,khash,current)

        filehash = abs(hash(filename)) % 10000000

        f = open("%s/ind/indices-%i-%i-%i.dat" % (cfg.output_dir,filehash,i,THREADS),'w')

        for x,y in zip(x_ind,y_ind):
            f.write("%i %i %.10e %.10e\n" % (x,y,H[x,y].real,H[x,y].imag))

        if i == 0: N1 = len(x_ind)
        if i == 1: N2 = len(x_ind)
        if i == 2: N3 = len(x_ind)
        if i == 3: N4 = len(x_ind)
        if i == 4: N5 = len(x_ind)
        if i == 5: N6 = len(x_ind)

        f.close()

    
    times.append(time.time() - start)
    start = time.time()

    args = "THREADS=%i SIZE=%i BW=%i FILEHASH=%i WIDTH=%i N1=%i N2=%i N3=%i N4=%i N5=%i N6=%i NUM_KS=%i KHASH=%i ./runocc.sh" \
            % (THREADS,N*2*BW,BW,filehash,width,N1,N2,N3,N4,N5,N6,num_ks,khash)
    print args
    proc = Popen(args,shell=True)

    output = proc.communicate("")[0]#.split('\n')[:-1]   

    occfile = open("%s/tmp/occ-%i-%i.txt" % (cfg.output_dir,filehash,THREADS),'r')
    nrgfile = open("%s/tmp/nrg-%i-%i.txt" % (cfg.output_dir,filehash,THREADS),'r')

    occupation = map(float,[val for val in occfile][:-1])#
    localenergy = np.array(map(float,[val for val in nrgfile][:-1]))

    occfile.close()
    nrgfile.close()
    times.append(time.time() - start)

    print np.array(times)/sum(times)
    print times

    for i in xrange(width*2):
        localenergy[i] -= (U*sum([occupation[2*BW*i + k] for k in xrange(BW)])*
                             sum([occupation[2*BW*i + BW + k] for k in xrange(BW)]))

    return (occupation,np.array(localenergy))

edge = 'zz'

lattice = cfg.get_lattice(width,length,edge,shape='GNR')

'''
load_ks = khash
filename1 = cfg.output_dir + "/occ" + cfg.get_dirs(width,6,B,U,phase,load_ks,current)
filename2 = cfg.output_dir + "/nrg" + cfg.get_dirs(width,6,B,U,phase,load_ks,current)
bool1 = os.path.isfile(filename1)
bool2 = os.path.isfile(filename2)

runanyway = True

print bool1,bool2
if bool1 and bool2: 
    occupation = np.loadtxt(filename1).tolist()
    _nrg = np.loadtxt(filename2).tolist()
    if len(_nrg) != 3 or len(occupation) != (2*width*2*BW):
        run = True
        load = False
    elif _nrg[2] > tol: run = load = True
    elif runanyway: run = load = True
    else:  run = load = False
else: 
    run = True
    load = False
#load = False
#run = True
'''
for krun in xrange(10):
    _k = 0
    a = 1.42e-10
    hexH = 17030979945.861198
    ks = []
    while _k < num_ks:
        kx = random.uniform(0.,1.)*2.*np.pi/3./1.42e-10
        ky = random.uniform(0.,1.)*hexH
        m = (hexH - 2.*np.pi/3./np.sqrt(3.)/a)/(2.*np.pi/3./a)
        if ky > ((-m*kx + hexH)*(1.001)): continue
        else:
             ks.append([kx,ky])
             _k += 1

    cfg.mkdir(cfg.output_dir + '/ks/%i' % num_ks)
    khash = hash(np.sum(np.array(ks))) % 1000000
    tmpfile = open(cfg.output_dir + '/ks/%i/%i' % (num_ks,khash),'w')
    for k in xrange(num_ks):
        tmpfile.write("%.10e %.10e\n" % (ks[k][0],ks[k][1]))
    tmpfile.close()
    for current in np.arange(0.,10.e-3,20.e-3):
        for phase in ["FM","AFM"]:
            run = True
            load = False

            if run and RANK == 0:
                if not load:
                    if phase == "AFM":
                        occupation = np.array([((0. if i % 2 == 0 else 1) if (i/2) % 2 == 0 
                                                 else (0. if i % 2 == 1 else 1))
                                                 if (i/(2*width)) % 2 == 0 else 
                                                 ((1. if i % 2 == 0 else 0.) if (i/2) % 2 == (width % 2) 
                                                 else (1. if i % 2 == 1 else 0.))
                                                 for i in xrange(width*2*2)])/2.
                    elif phase == "FM":
                        occupation = np.array([(0. if i % 2 == 0 else 1) for i in xrange(width*2*2)])/2.

                    for i in xrange(2):
                        for j in xrange(width):
                            if lattice[i,j] == 'O': 
                                occupation[2*i*width + 2*j] = 0
                                occupation[2*i*width + 2*j + 1] = 0.

                    occupation = occupation.tolist()

                wires = [[current,np.array([0.,1.,0.]),np.array([-10.e-9,0.,0.])],
                         [current,np.array([0.,-1.,0.]),np.array([10.e-9 + cfg.get_pos(1,width-1,edge)[0],0.,0.])]]

                _B = cfg.Bwire(wires)

                x = np.arange(-1.e-9,cfg.get_pos(1,width-1,edge)[0]+ 1.e-9,.1e-9)
                y = np.arange(0.,2.e-9,.1e-9)
                z = np.arange(0.,1.,1.)

                Nx,Ny,Nz = len(x),len(y),len(z)

                R = np.array([[[np.array([i,j,k]) for k in z] for j in y] for i in x])

                Bfield = np.zeros((Nx,Ny,Nz,3),dtype = float)

                for i in xrange(Nx):
                    for j in xrange(Ny):
                        for k in xrange(Nz):
                            Bfield[i,j,k] = _B(R[i,j,k])

                interp = RBS(x,y,Bfield[:,:,Nz/2,2])

                mag = []
                for col in xrange(2):
                    for row in xrange(width):
                        for spin in [1,-1.]:
                            mag.append(spin*-928.476377e-26*interp(cfg.get_pos(col,row,edge)[0],
                                                                   cfg.get_pos(col,row,edge)[1])[0][0])

                occupation = [x for item in occupation for x in repeat(item,BW)]
                mag = [x for item in mag for x in repeat(item,BW)]

                unitcellnrg = 0.
                error = 10000.


                print cfg.output_dir + '/ks/%i/%i' % (num_ks,khash) 

                while error > tol:

                    _occupation = map(sum,cfg.grouper(BW,occupation,0))

                    magnetization = np.array([_occupation[2*i] - _occupation[2*i+1] for i in xrange(width*2)])/2.

                    old = np.array(occupation)

                    occupation,localenergy = get_occupation(lattice,occupation,U = U,Z = mag,khash = khash)

                    print np.sum(occupation)
                    unitcellnrg = np.sum(localenergy)
                    nrg = np.sum(localenergy)

                    error = np.linalg.norm(occupation - old,np.inf)
                    print np.reshape(magnetization,(2,width)).T[:10,:]
                    print np.reshape(magnetization,(2,width)).T[-10:,:]

                    print "error:",error
                    print 'unitcell energy (eV):',unitcellnrg
                    print 'energy (t):',nrg/2.7,"\n\n"

                print "B:",B

                outdir = cfg.output_dir + cfg.get_dirs(width,length,B,U,phase,num_ks,current)

                cfg.mkdir(outdir + '/nrg')
                cfg.mkdir(outdir + '/occ')
                cfg.mkdir(outdir + '/mag')

                np.savetxt(outdir + "/nrg/%i" % khash,[unitcellnrg,nrg,error])
                np.savetxt(outdir + "/occ/%i" % khash,_occupation)
                np.savetxt(outdir + "/mag/%i" % khash,mag)

            else: 
                print "didnt run"
            if RANK == 0: release()
            else: wait()
