/*
   Copyright (c) 2009-2014, Jack Poulson
   All rights reserved.

   This file is part of Elemental and is under the BSD 2-Clause License, 
   which can be found in the LICENSE file in the root directory, or at 
   http://opensource.org/licenses/BSD-2-Clause
*/
// NOTE: It is possible to simply include "elemental.hpp" instead
//#include <iomanip>
#include "elemental.hpp"
#include ELEM_DIAGONALSCALE_INC
#include ELEM_HEMM_INC
#include ELEM_HERK_INC
#include ELEM_FROBENIUSNORM_INC
#include ELEM_IDENTITY_INC
#include <cblas.h>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <complex>
#include <sstream>
#include <iostream>
#include <string>
#include <math.h>
using namespace std;
using namespace elem;

typedef double Real;
typedef Complex<Real> C;

struct by_nrg { 
        bool operator()(vector<double> const &a, vector<double> const &b) const { 
                    return a.at(0) < b.at(0);
                        }
};
void solveH(Matrix<C> &H,Matrix<C> &psi,
            Matrix<Real> &nrg,int n,
            vector<Matrix<C>> &Hs,
            int*Ns,double kx,double ky,
            int s) {
    Zeros(H,n,n);
    Zeros(psi,n,n);
    Zeros(nrg,n,n);

    for (int p = 0; p < 3; p++) {

        double T;
        if (p == 0) T = -sqrt(3.)*1.42e-10;
        else if (p == 1) T = 0.;
        else if (p == 2) T = sqrt(3.)*1.42e-10;

        for (int q = 0; q < Ns[p + s*3]; q++) {
            int i = static_cast<int>(real(Hs.at(p + s*3).Get(q,0)) + .1);
            int j = static_cast<int>(imag(Hs.at(p + s*3).Get(q,0)) + .1);
            C tmp = (Hs.at(p + s*3).Get(q,1))*exp(C(0.,1.)*(ky*T));
            H.Update(i,j,tmp);
        }
    }
}

int main( int argc, char* argv[] )
{
    Initialize( argc, argv );

    try 
    {
        const Int n = Input("--size","size of matrix",100);
        const Int BW = Input("--BW","blockwidth",1);
        const Int N1 = Input("--n1","size of matrix",100);
        const Int N2 = Input("--n2","size of matrix",100);
        const Int N3 = Input("--n3","blockwidth",1);
        const Int N4 = Input("--n4","size of matrix",100);
        const Int N5 = Input("--n5","size of matrix",100);
        const Int N6 = Input("--n6","blockwidth",1);
        const int width = Input("--width","blockwidth",1);
        const Int threads = Input("--threads","blockwidth",1);
        const Int NUM_KS = Input("--num_ks","blockwidth",1);
        const bool print = Input("--print","print matrices?",false);
        //const Int filehash = Input("--index_hash","indices file name hash",0);
        const Int filehash = Input("--filehash","indices file name",0);

        //cerr << filename.c_str() << endl;

        ProcessInput();
        //PrintInputReport();

        Grid g( mpi::COMM_WORLD );


        const int SIZE = mpi::CommSize(mpi::COMM_WORLD);
        const int RANK = mpi::CommRank(mpi::COMM_WORLD);
        if (RANK==0) cerr << g.Height() << " " << g.Width() << endl;

        int Ns[6];

        Ns[0] = N1;
        Ns[1] = N2;
        Ns[2] = N3;
        Ns[3] = N4;
        Ns[4] = N5;
        Ns[5] = N6;

        vector<Matrix<C>> Hs;
        for (int p = 0; p < 6; p++) {

            Matrix<C> H(Ns[p],2);
            Zeros(H,Ns[p],2);
            double a,b;
            int i,j,num_elem,ind=0;
            string line;
            stringstream filename;

            filename << "output/ind/indices-" 
                     << filehash << "-" << p << "-" << threads << ".dat";
            ifstream indices(filename.str());

            while(getline(indices,line)) {

                stringstream stream(line);
                
                stream >> i;
                stream >> j; 
                stream >> a;
                stream >> b;

                
                H.Set(ind,0,C(i,j));
                H.Set(ind,1,C(a,b));
                ind++;
            }

            indices.close();
            Hs.push_back(H);
            //Print(H);
        }


        double epsilon = .001;

        int sidelen = NUM_KS;

        double a = 1.42e-10;

        vector<C> ks;

        double dx = 4.*M_PI/3./a/double(sidelen);
        double hexH = 17030979945.861198;
        double dy = 2.*hexH/double(sidelen);
        double m = (17030979945.861198 - 2.*M_PI/3./sqrt(3.)/a)/(2.*M_PI/3./a);

        //for (int i = -sidelen/2; i < sidelen/2; i++) {
        for (int i = 0; i < sidelen/2; i++) {
            //for (int j = -sidelen/2; j < sidelen/2; j++) {
            for (int j = 0; j < sidelen/2; j++) {
                double kx = dx*(static_cast<double>(i)+.5);
                double ky = dy*(static_cast<double>(j)+.5);

                if (kx > 0.) {
                    if (ky > 0.) {
                        if (ky > ((-m*kx + hexH)*(1. + epsilon))) continue;
                        else ks.push_back(C(kx,ky));
                    } else {
                        if (ky < ((m*kx - hexH)*(1. + epsilon))) continue;
                        else ks.push_back(C(kx,ky));
                    }
                } else {
                    if (ky > 0.) {
                        if (ky > ((m*kx + hexH)*(1. + epsilon))) continue;
                        else ks.push_back(C(kx,ky));
                    } else {
                        if (ky < ((-m*kx - hexH)*(1. + epsilon))) continue;
                        else ks.push_back(C(kx,ky));
                    }
                }
                        
            }
        }


        int nk = ks.size();

        if (RANK == 0) cerr << "numks: " << nk << endl;
        vector<int> nks;
        for (int i = 0; i < SIZE; i++) {
            nks.push_back(nk/SIZE + (i < (nk % SIZE) ? 1 : 0));
        }

        //vector<Sorter> nrgs_sort; 
        vector<vector<double>> nrgs_sort; 
        vector<Matrix<Real>> kmats;

        C *pkt;
        double *pkt2;
        if (RANK > 0) {
            pkt = new C[2*2*n*nks.at(RANK)];
            pkt2 = new double[n*2*n*nks.at(RANK)];
        } else {
            pkt = new C[1];
            pkt2 = new double[1];
            delete [] pkt;
            delete [] pkt2;
        }
    
        for (int k = 0; k < nks.at(RANK); k++) {
            int _k = RANK*nks.at(RANK) + (RANK > ((nk % SIZE)-1) ? (nk % SIZE) : 0) + k;
            double kx = real(ks.at(_k)),ky = imag(ks.at(_k));
    
            for (int s = 0; s < 2; s++) {

                Matrix<C> H;
                Matrix<C> psi;
                Matrix<Real> nrg;

                solveH(H,psi,nrg,n,Hs,Ns,kx,ky,s);

                HermitianEig( LOWER , H , nrg , psi, ASCENDING);

                if (RANK==0) {
                    Matrix<Real>psi2(n,n);
                    for (int x = 0; x < n; x++) {
                        vector<double> a;
                        a.push_back(nrg.Get(x,0));
                        a.push_back(static_cast<double>(s));
                        a.push_back(static_cast<double>(_k));
                        a.push_back(static_cast<double>(x));
                        nrgs_sort.push_back(a);
                        for (int y = 0; y < n; y++) psi2.Set(y,x,real(psi.Get(y,x)*conj(psi.Get(y,x))));
                    }
                    kmats.push_back(psi2);
                } else {
                    for (int x = 0; x < n; x++) {
                        pkt[k*2*n*2 + s*2*n + x*2] = C(nrg.Get(x,0),static_cast<double>(s));
                        pkt[k*2*2*n + s*2*n + x*2 + 1] = C(static_cast<double>(_k),static_cast<double>(x));
                        for (int y = 0; y < n; y++) pkt2[k*2*n*(n) + s*n*n + x*n + y] = real(psi.Get(y,x)*conj(psi.Get(y,x)));
                    }
                }
            }
        }
    if (RANK == 0) {
            for (int proc = 1; proc < SIZE; proc++) {

                C *_pkt = new C[2*2*n*nks.at(proc)];
                double *_pkt2 = new double[n*2*n*nks.at(proc)];

                mpi::Recv(_pkt,2*2*n*nks.at(proc),proc,mpi::COMM_WORLD);
                mpi::Recv(_pkt2,n*2*n*nks.at(proc),proc,mpi::COMM_WORLD);

                for (int k =0; k < nks.at(proc); k++) {
                    for (int l = 0; l < 2*n; l++) {
                        vector<double> a;

                        a.push_back(real(_pkt[k*2*n*2 + l*2]));
                        a.push_back(imag(_pkt[k*2*n*2 + l*2]));
                        a.push_back(real(_pkt[k*2*n*2 + l*2 + 1]));
                        a.push_back(imag(_pkt[k*2*n*2 + l*2 + 1]));

                        nrgs_sort.push_back(a);
                    }

                    for (int s = 0; s < 2; s++) {

                        Matrix<Real> psi(n,n);
                        
                        for (int p = 0; p < n; p++)
                            for (int q = 0; q < n; q++)
                                psi.Set(q,p,_pkt2[k*2*n*n + s*n*n + p*n + q]);

                        kmats.push_back(psi);
                    }
                }

                delete [] _pkt;
                delete [] _pkt2;
            }
        } else {
            mpi::Send(pkt,2*2*n*nks.at(RANK),0,mpi::COMM_WORLD);
            mpi::Send(pkt2,n*2*n*nks.at(RANK),0,mpi::COMM_WORLD);
            delete [] pkt;
            delete [] pkt2;
        }

        if (RANK == 0) {

            sort(nrgs_sort.begin(),nrgs_sort.end(),by_nrg()); 

            cerr << "sorted" << endl;

            int s,k,x;
            double nrg;
            Matrix<Real> occupation(2*(n/BW),1),energy((n/BW),1);

            Zeros(occupation,2*(n/BW),1);
            Zeros(energy,(n/BW),1);

            for (int i = 0; i < nk*(n/BW); i++) {
                nrg = nrgs_sort.at(i).at(0);
                //cout << nrg << endl;
                s = static_cast<int>(nrgs_sort.at(i).at(1) + .1);
                k = static_cast<int>(nrgs_sort.at(i).at(2) + .1);
                x = static_cast<int>(nrgs_sort.at(i).at(3) + .1);

                for (int j = 0; j < (n/BW); j++) {
                    for (int f = 0; f < BW; f++) {
                        occupation.Update(j*2 + s,0,kmats.at(2*k + s).Get(j*BW+f,x)/nk);
                        energy.Update(j,0,kmats.at(2*k + s).Get(j*BW+f,x)/nk*nrg);
                        //if (abs(nrg) < 1.e-10) cout << kmats.at(2*k + s).Get(j*BW+f,x) << endl;
                    }
                }
            }

            //Print(occupation);
            stringstream occerr,nrgout;
            occerr << "output/tmp/occ-" << filehash << "-" << threads;
            nrgout << "output/tmp/nrg-" << filehash << "-" << threads;
            Write(occupation,occerr.str(),ASCII);
            Write(energy,nrgout.str(),ASCII);


            Matrix<Real> ek(2*NUM_KS*n,3);
            Zeros(ek,2*NUM_KS*n,3);
            for (k=0; k < NUM_KS; k++) {
                for (s = 0; s < 2; s++) {
                    double kx = 4.*M_PI/3./1.42e-10/NUM_KS*k; 
                    double ky = 4.*M_PI/3./sqrt(3.)/1.42e-10/NUM_KS*k; 

                    Matrix<C> H;
                    Matrix<C> psi;
                    Matrix<Real> nrg;

                    solveH(H,psi,nrg,n,Hs,Ns,kx,ky,s);

                    HermitianEig( LOWER , H , nrg , psi, ASCENDING);
                    for (x=0; x < n; x++) {
                        ek.Set(k*2*n + s*n + x,0,kx);
                        ek.Set(k*2*n + s*n + x,1,ky);
                        ek.Set(k*2*n + s*n + x,2,nrg.Get(x,0));
                    }
                }
            }

            stringstream ekout;
            ekout << "output/tmp/ek-" << filehash << "-" << threads;
            Write(ek,ekout.str(),ASCII);
        }

    }
    catch( exception& e ) { ReportException(e); }

    Finalize();
    return 0;
}
