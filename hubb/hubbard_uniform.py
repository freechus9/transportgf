import cfg
import numpy as np
import scipy.sparse as sparse
from numpy.linalg import eig
from subprocess import Popen,PIPE
import time
from itertools import repeat
import sys
import matplotlib.pyplot as plt
import os
from scipy.interpolate import RectBivariateSpline as RBS
import support as supp
from mpi4py import MPI
COMM = MPI.COMM_WORLD           # MPI Init
RANK = COMM.Get_rank()          # Each thread gets own rank
NUMTHREADS = COMM.Get_size()      # Each thread knows numprocs
print RANK
def wait(): COMM.recv(source=0,tag=1)
def release(): return [COMM.send('go',dest=i+1,tag=1) for i in xrange(NUMTHREADS - 1)]

def run_hubb(THREADS,width,length,phase,U,num_ks,tol,current,runanyway=False,ignore_empty = False):

    BW = (3 if cfg.Bools.sixband else 1)

    def get_occupation(lattice,occupation,U=0.,Z=None,A = None):
        edge = 'zz'
        times = []
        start = time.time()

        vacs = []
        
        for i,col in enumerate(lattice[:2]):
            for j,row in enumerate(col):
                if row == 'O': 
                    for p in xrange(BW): vacs.append((i*width + j)*BW + p)

        error = 1.
        start_time = time.time()

        H = cfg.Hamiltonian(lattice,edge,spin=2,U=(U,occupation*10),Z = mag*10).todense()/cfg.Const.e

        # hack for bw = 1, make for bw=3
        indices1 = []
        indices2 = []
        indices3 = []
        indices4 = []

        for i in xrange(2*width*2*BW):
            if (i / BW) % 2 == 0: 
                indices1.append([i])        
                indices2.append(i)        
            else:
                indices3.append([i])
                indices4.append(i)

        Hs = [H[2*width*2*BW:4*width*2*BW,:2*width*2*BW][indices1,indices2],
              H[2*width*2*BW:4*width*2*BW,2*width*2*BW:4*width*2*BW][indices1,indices2],
              H[2*width*2*BW:4*width*2*BW,4*width*2*BW:6*width*2*BW][indices1,indices2],
              H[2*width*2*BW:4*width*2*BW,:2*width*2*BW][indices3,indices4],
              H[2*width*2*BW:4*width*2*BW,2*width*2*BW:4*width*2*BW][indices3,indices4],
              H[2*width*2*BW:4*width*2*BW,4*width*2*BW:6*width*2*BW][indices3,indices4]]

        '''
        print Hs[0][::BW,::BW].real
        print Hs[1][::BW,::BW].real
        print Hs[2][::BW,::BW].real
        print Hs[3][::BW,::BW].real
        print Hs[4][::BW,::BW].real
        print Hs[5][::BW,::BW].real
        '''

        N1,N2,N3,N4,N5,N6 = 0,0,0,0,0,0

        filename = "%i-%i-%.2f-%s-%i-%.2e" % (width,length,U,phase,num_ks,current)

        filehash = abs(hash(filename)) % 10000000

        for i,H in enumerate(Hs):

            H = np.delete(H,vacs,0)
            H = np.delete(H,vacs,1)

            H = sparse.lil_matrix(H)

            x_ind,y_ind = H.nonzero()


            f = open("%s/ind/indices-%i-%i-%i.dat" % (cfg.output_dir,filehash,i,THREADS),'w')

            for x,y in zip(x_ind,y_ind):
                f.write("%i %i %.16e %.16e\n" % (x,y,H[x,y].real,H[x,y].imag))

            if i == 0: N1 = len(x_ind)
            if i == 1: N2 = len(x_ind)
            if i == 2: N3 = len(x_ind)
            if i == 3: N4 = len(x_ind)
            if i == 4: N5 = len(x_ind)
            if i == 5: N6 = len(x_ind)

            f.close()
        
        times.append(time.time() - start)
        start = time.time()

        N = 2*width - (len(vacs)/BW)

        args = "THREADS=%i SIZE=%i BW=%i FILEHASH=%i WIDTH=%i N1=%i N2=%i N3=%i N4=%i N5=%i N6=%i NUM_KS=%i ./runocc.sh" \
                % (THREADS,N*BW,BW,filehash,width,N1,N2,N3,N4,N5,N6,num_ks)
        print args
        proc = Popen(args,shell=True)

        output = proc.communicate("")[0]#.split('\n')[:-1]   

        occfile = open("%s/tmp/occ-%i-%i.txt" % (cfg.output_dir,filehash,THREADS),'r')
        nrgfile = open("%s/tmp/nrg-%i-%i.txt" % (cfg.output_dir,filehash,THREADS),'r')

        occupation = map(float,[val for val in occfile][:-1])#
        localenergy = map(float,[val for val in nrgfile][:-1])

        for vac in [vac/BW for vac in vacs][::BW]:
            occupation.insert(2*vac,0.)
            occupation.insert(2*vac+1,0.)
            localenergy.insert(2*vac,0.)
            localenergy.insert(2*vac+1,0.)

        occfile.close()
        nrgfile.close()
        times.append(time.time() - start)

        print np.array(times)/sum(times)
        print times

        for i in xrange(width*2):
            localenergy[i] -= (U*occupation[2*i]*occupation[2*i + 1])

        
        return (occupation,np.array(localenergy),filehash)

    edge = 'zz'

    lattice = cfg.get_lattice(width,length,edge,shape='GNR')

    outdir = cfg.output_dir + '/hubb' + cfg.get_dirs(width,length,U,phase,num_ks,current)

    if not ignore_empty: cfg.mkdir(outdir)
    filename1 = outdir + '/occ' 
    filename2 = outdir + '/nrg'
    filename3 = outdir + '/mag'
    bool1 = os.path.isfile(filename1)
    bool2 = os.path.isfile(filename2)
    bool3 = os.path.isfile(filename3)

    if bool1 and bool2 and bool3: 
        occupation = np.loadtxt(filename1).tolist()
        _nrg = np.loadtxt(filename2).tolist()
        _mag = np.loadtxt(filename3).tolist()
        if len(_nrg) != 3 or len(occupation) != (2*width*2) or len(_mag) != (2*width*2):
            run = True
            load = False
        elif _nrg[2] > tol: run = load = True
        else: run = load = False
        if runanyway: 
            run = True
            load = False
    elif ignore_empty:
        return ([0.,0.,0.],None,None,outdir)
    else: 
        run = True
        load = False
    #load = False
    #run = True


    if run: 
        if not load:
            if phase == "AFM":
                occupation = np.array([((0. if i % 2 == 0 else 1) if (i/2) % 2 == 0 
                                         else (0. if i % 2 == 1 else 1))
                                         if (i/(2*width)) % 2 == 0 else 
                                         ((1. if i % 2 == 0 else 0.) if (i/2) % 2 == (width % 2) 
                                         else (1. if i % 2 == 1 else 0.))
                                         for i in xrange(width*2*2)])/2.
            elif phase == "FM":
                occupation = np.array([(0. if i % 2 == 0 else 1) for i in xrange(width*2*2)])/2.

            for i in xrange(2):
                for j in xrange(width):
                    if lattice[i,j] == 'O': 
                        occupation[2*i*width + 2*j] = 0
                        occupation[2*i*width + 2*j + 1] = 0.

            occupation = occupation.tolist()

        wires = [[current,np.array([0.,1.,0.]),np.array([-1.e-9,0.,0.])],
                 [current,np.array([0.,-1.,0.]),np.array([1.e-9 + cfg.get_pos(1,width-1,edge)[0],0.,0.])]]

        _B = cfg.Bwire(wires)
        _A = cfg.Awire(wires)   
        _A = None

        x = np.arange(-.5e-9,cfg.get_pos(1,width-1,edge)[0]+ .5e-9,.1e-9)
        y = np.arange(0.,2.e-9,.1e-9)
        z = np.arange(0.,1.,1.)

        Nx,Ny,Nz = len(x),len(y),len(z)

        R = np.array([[[np.array([i,j,k]) for k in z] for j in y] for i in x])

        Bfield = np.zeros((Nx,Ny,Nz,3),dtype = float)

        for i in xrange(Nx):
            for j in xrange(Ny):
                for k in xrange(Nz):
                    Bfield[i,j,k] = _B(R[i,j,k])

        interp = RBS(x,y,Bfield[:,:,Nz/2,2])

        mag = []
        for col in xrange(2):
            for row in xrange(width):
                for spin in [1,-1.]:
                    mag.append(spin*-928.476377e-26*interp(cfg.get_pos(col,row,edge)[0],
                                                           cfg.get_pos(col,row,edge)[1])[0][0])

        print mag

        unitcellnrg = 0.
        error = 10000.

        while error > tol:
        #for pp in xrange(4):

            _occupation = np.array(occupation)

            magnetization = np.array([_occupation[2*i] - _occupation[2*i+1] for i in xrange(width*2)])/2.

            old = np.array(occupation)

            occupation,localenergy,filehash = get_occupation(lattice,occupation,U = U,Z = mag)

            print np.sum(occupation)
            unitcellnrg = np.sum(localenergy)
            nrg = np.sum(localenergy)

            error = np.linalg.norm(occupation - old,np.inf)
            print np.reshape(magnetization,(2,width)).T[:10,:]
            print np.reshape(magnetization,(2,width)).T[-10:,:]

            print "error:",error
            print 'unitcell energy (eV):',unitcellnrg
            print 'energy (t):',nrg/2.7,"\n\n"

        np.savetxt(outdir + "/nrg",[unitcellnrg,nrg,error])
        np.savetxt(outdir + "/occ",_occupation)
        np.savetxt(outdir + "/mag",mag)


    else:
        unitcellnrg,nrg,error = _nrg
        mag = _mag
        _occupation = occupation

    """
    filename = "%i-%i-%.2f-%s-%i-%.2e" % (width,length,U,phase,num_ks,current)

    filehash = abs(hash(filename)) % 10000000
    print "%s/tmp/ek-%i-%i.txt" % (cfg.output_dir,filehash,THREADS)
    ekfile = open("%s/tmp/ek-%i-%i.txt" % (cfg.output_dir,filehash,THREADS),'r')

    ks = np.zeros((num_ks,2*width*2 + 1),dtype = float)

    for i,line in enumerate(ekfile): 
        try: 
            kx,ky,energy = [float(val) for val in line.split()]
            ks[i / (2*width*2),0] = np.sqrt(kx**2 + ky**2)
            ks[i / (2*width*2),(i % (2*width*2))+1] = energy
            #plt.scatter(np.sqrt(kx**2 + ky**2),energy)
        except:
            print line
    #print ks
    plt.xlim(0.,25546469918.791798*cfg.Const.a)
    plt.ylim(-2,3.)
    for plot in xrange(2*width):
        plt.plot(ks[:,0]*cfg.Const.a,ks[:,plot+1] - U/2,color='k',lw=6)
    for plot in xrange(2*width):
        plt.plot(ks[:,0]*cfg.Const.a,ks[:,plot+1 + 2*width] - U/2,color='r',lw=6)
    plt.savefig('ektest.png',format='png') 
    """
    return ([unitcellnrg,nrg,error],_occupation,mag,outdir)
