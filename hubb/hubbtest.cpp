/*
   Copyright (c) 2009-2014, Jack Poulson
   All rights reserved.

   This file is part of Elemental and is under the BSD 2-Clause License, 
   which can be found in the LICENSE file in the root directory, or at 
   http://opensource.org/licenses/BSD-2-Clause
*/
// NOTE: It is possible to simply include "elemental.hpp" instead
//#include <iomanip>
#include "elemental.hpp"
#include ELEM_DIAGONALSCALE_INC
#include ELEM_HEMM_INC
#include ELEM_HERK_INC
#include ELEM_FROBENIUSNORM_INC
#include ELEM_IDENTITY_INC
#include <cblas.h>
#include <fstream>
#include <stdio.h>
#include <complex>
#include <sstream>
#include <iostream>
#include <string>
#include <math.h>
using namespace std;
using namespace elem;

typedef double Real;
typedef Complex<Real> C;

struct Sorter
{
    double energy; 
    double kx,ky;
    int spin;
    Matrix<C> psi;
    Sorter() : energy(0.),spin(0),kx(0.),ky(0.) {}
    Sorter(double nrg, int s,double _kx,double _ky,Matrix<C> psi_k) : energy(nrg),spin(s),kx(_kx),ky(_ky),psi(psi_k) {}

    bool operator < (const Sorter& srtr) const {
        return (energy < srtr.energy);
    }
};

int main( int argc, char* argv[] )
{
    Initialize( argc, argv );

    try 
    {
        const Int n = Input("--size","size of matrix",100);
        const int BW = Input("--BW","blockwidth",1);
        const Int N1 = Input("--n1","size of matrix",100);
        const Int N2 = Input("--n2","size of matrix",100);
        const Int N3 = Input("--n3","blockwidth",1);
        const Int N4 = Input("--n4","size of matrix",100);
        const Int N5 = Input("--n5","size of matrix",100);
        const Int N6 = Input("--n6","blockwidth",1);
        const int width = Input("--width","blockwidth",1);
        const Int threads = Input("--threads","blockwidth",1);
        const Int NUM_KS = Input("--num_ks","blockwidth",1);
        const bool print = Input("--print","print matrices?",false);
        //const Int filehash = Input("--index_hash","indices file name hash",0);
        const Int filehash = Input("--filehash","indices file name",0);

        //cout << filename.c_str() << endl;

        ProcessInput();
        //PrintInputReport();

        Grid g( mpi::COMM_WORLD );

        const int SIZE = mpi::CommSize(mpi::COMM_WORLD);
        const int RANK = mpi::CommRank(mpi::COMM_WORLD);

        int Ns[6];

        Ns[0] = N1;
        Ns[1] = N2;
        Ns[2] = N3;
        Ns[3] = N4;
        Ns[4] = N5;
        Ns[5] = N6;

        vector<Matrix<C>> Hs;
        for (int p = 0; p < 6; p++) {

            Matrix<C> H(Ns[p],2);
            Zeros(H,Ns[p],2);
            double a,b;
            int i,j,num_elem,ind=0;
            string line;
            stringstream filename;

            filename << "../output/ind/indices-" << filehash << "-" << p<<".dat";
            ifstream indices(filename.str());

            while(getline(indices,line)) {

                stringstream stream(line);
                
                stream >> i;
                stream >> j; 
                stream >> a;
                stream >> b;

                
                H.Set(ind,0,C(i,j));
                H.Set(ind,1,C(a,b));
                ind++;
            }

            indices.close();
            Hs.push_back(H);
        }

        double epsilon = .001;

        int sidelen = NUM_KS;

        double a = 1.42e-10;

        double dx = 4.*M_PI/3./a/(double(sidelen) - 1.);
        double dy = 8.*M_PI/sqrt(3.)/a/(double(2*sidelen) - 1.);

        vector<C> ks;

        for (int i = 0; i < sidelen; i++) {
            for (int j = -sidelen + 1; j < sidelen; j++) {
                double kx = dx*double(i);
                double ky = dy*double(j);

                if (kx < (2.*M_PI/3./a)) {
                    if (ky > 0.) {
                        if (ky > (kx*sqrt(3.)*(1. + epsilon))) continue;
                        else ks.push_back(C(kx,ky));
                    } else {
                        if (ky < -(kx*sqrt(3.)*(1. + epsilon))) continue;
                        else ks.push_back(C(kx,ky));
                    }
                } else {
                    if (ky > 0.) {
                        if (ky > (((4*M_PI/3./a) - kx)*sqrt(3.)*(1. + epsilon))) continue;
                        else ks.push_back(C(kx,ky));
                    } else {
                        if (ky < -(((4*M_PI/3./a) - kx)*sqrt(3.)*(1. + epsilon))) continue;
                        else ks.push_back(C(kx,ky));
                    }
                }
                        
            }
        }


        int nk = ks.size();
        vector<int> nks;
        for (int i = 0; i < SIZE; i++) nks.push_back(nk/SIZE + (i < (nk % SIZE) ? 1 : 0));

        vector<Sorter> nrgs_sort;
        //if (RANK == 0) for (int i = 0; i < nk*2*2*width*BW; i++) nrgs_sort.push_back(Sorter());
    
        for (int k = 0; k < nks.at(RANK); k++) {
            int _k = RANK*nks.at(RANK) + (RANK > ((nk % SIZE)-1) ? (nk % SIZE) : 0) + k;
            double kx = real(ks.at(_k)),ky = imag(ks.at(_k));
            for (int s = 0; s < 2; s++) {

                Matrix<C> H;
                Matrix<C> psi;
                Matrix<Real> nrg;
                //DistMatrix<C> H(2*width*BW,2*width*BW,g);
                //DistMatrix<C> psi(g);
                //DistMatrix<Real,VR,STAR> nrg(g);

                Zeros(H,2*width*BW,2*width*BW);
                Zeros(psi,2*width*BW,2*width*BW);
                Zeros(nrg,2*width*BW,2*width*BW);

                for (int p = 0; p < 3; p++) {

                    double T;
                    if (p == 0) T = -sqrt(3.)*1.42e-10;
                    else if (p == 1) T = 0.;
                    else if (p == 2) T = sqrt(3.)*1.42e-10;

                    for (int q = 0; q < Ns[p + s*3]; q++) {
                        int i = int(real(Hs.at(p + s*3).Get(q,0)));
                        int j = int(imag(Hs.at(p + s*3).Get(q,0)));
                        C tmp = (Hs.at(p + s*3).Get(q,1))*exp(C(0.,1.)*(ky*T));
                        H.Update(i,j,tmp);
                    }
                }

                HermitianEig( LOWER , H , nrg , psi, ASCENDING);

                for (int x = 0; x < 2*width*BW; x++) {
                    Matrix<C> mat(2*width*BW,1);
                    for (int y = 0; y < 2*width*BW; y++) mat.Set(y,0,psi.Get(y,x));
                    nrgs_sort.push_back(Sorter(nrg.Get(x,0),s,kx,ky,mat));
                }
                        
            }
        }
        
        if (RANK==0) cout << "rank " << RANK << ": " << nrgs_sort.size() << endl;
        if (RANK == 0) {
            for (int proc = 1; proc < SIZE; proc++) {
                double _nrg,_kx,_ky;
                C *pkt = new C[(2 + 2*width*BW)*2*2*width*BW*nks.at(proc)];
                mpi::Recv(pkt,(2+2*width*BW)*2*2*width*BW*nks.at(proc),proc,mpi::COMM_WORLD);
                int _s;
                Matrix<C> mat(2*width*BW,1);
                C tmp;
                for (int k =0; k < nks.at(proc); k++) {
                    for (int l = 0; l < 2*2*width*BW; l++) {
                        for (int j = 0; j < 2*width*BW; j++) {
                            tmp = pkt[k*2*2*width*BW*(2*width*BW+2) + l*(2*width*BW+2) + j];
                            mat.Set(j,0,tmp);
                        }

                        //mpi::Recv(&_kx,1,proc,mpi::COMM_WORLD);
                        //mpi::Recv(&_ky,1,proc,mpi::COMM_WORLD);
                        //mpi::Recv(&_s,1,proc,mpi::COMM_WORLD);
                        tmp = pkt[k*2*2*width*BW*(2*width*BW+2) + l*(2*width*BW+2) + 2*width*BW];
                        _nrg = real(tmp);
                        _s = int(imag(tmp));
                        tmp = pkt[k*2*2*width*BW*(2*width*BW+2) + l*(2*width*BW+2) + 2*width*BW + 1];
                        _kx = real(tmp);
                        _ky = imag(tmp);

                        nrgs_sort.push_back(Sorter(_nrg,_s,_kx,_ky,mat));
                    }
                }
                delete [] pkt;
            }
        } else {
            Sorter tmp;
            C tmpC;
            double _nrg,_kx,_ky;
            C *pkt = new C[(2 + 2*width*BW)*2*2*width*BW*nks.at(RANK)];
            int _s;
            
            for (int k = 0; k < nks.at(RANK); k++) {
                for (int l = 0; l < 2*2*width*BW; l++) {
                    tmp = nrgs_sort.at(k*2*2*width + l);
                    for (int j = 0; j < 2*width*BW; j++) {
                        pkt[k*2*2*width*(2*width+2) + l*(2*width+2) + j] = tmp.psi.Get(j,0);
                        //mpi::Send(&tmpC,1,0,mpi::COMM_WORLD);
                    }
                    //_nrg = tmp.energy;
                    //_kx = tmp.kx;
                    //_ky = tmp.ky;
                    pkt[k*2*2*width*(2*width+2) + l*(2*width+2) + 2*width] = C(tmp.energy,tmp.spin);
                    pkt[k*2*2*width*(2*width+2) + l*(2*width+2) + 2*width + 1] = C(tmp.kx,tmp.ky);
                    
                    //mpi::Send(&_kx,1,0,mpi::COMM_WORLD);
                    //mpi::Send(&_ky,1,0,mpi::COMM_WORLD);
                    //mpi::Send(&_s,1,0,mpi::COMM_WORLD);
                }
            }
            mpi::Send(pkt,(2 + 2*width*BW)*2*2*width*BW*nks.at(RANK),0,mpi::COMM_WORLD);
            delete [] pkt;
        }
            
                
        if (RANK == 0) {
            sort(nrgs_sort.begin(),nrgs_sort.end()); 

            cout << nk*2*2*width << " " << nrgs_sort.size() << endl;

            Matrix<Real> occupation(2*2*width*BW,1),energy(2*width*BW,1);

            Zeros(occupation,2*2*width*BW,1);
            Zeros(energy,2*width*BW,1);

            for (int i = 0; i < nk*2*width; i++) {
                for (int j = 0; j < 2*width*BW; j++) {
                    occupation.Update(j*2 + nrgs_sort.at(i).spin,0,real(nrgs_sort.at(i).psi.Get(j,0)*conj(nrgs_sort.at(i).psi.Get(j,0)))/nk);
                    energy.Update(j,0,real(nrgs_sort.at(i).psi.Get(j,0)*conj(nrgs_sort.at(i).psi.Get(j,0)))/nk*nrgs_sort.at(i).energy);
                }
            }

            stringstream occout,nrgout;
            occout << "../output/tmp/occ-" << filehash << "-" << threads;
            nrgout << "../output/tmp/nrg-" << filehash << "-" << threads;
            Write(occupation,occout.str(),ASCII);
            Write(energy,nrgout.str(),ASCII);
            //Print(occupation);

        }
    }
    catch( exception& e ) { ReportException(e); }

    Finalize();
    return 0;
}
