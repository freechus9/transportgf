import sys
import numpy as np
import matplotlib.pyplot as plt
from mpi4py import MPI
from subprocess import Popen
import os

import support as supp
import cfg as cfg
'''
'''

def get_trans(width=100,length=6,U=2.7,phase="FM",num_ks=100,B=0.,tol=1.e-6,current=0.,overwrite = False):

    edge = 'zz'
    lattice = cfg.get_lattice(width,3,'zz')
    outdir = cfg.output_dir + '/hubb' + cfg.get_dirs(width,length,B,U,phase,num_ks,current)
    data = []
    files = []
    mags = []
    nrgs = []
    occs = []
    if os.path.isdir(outdir):
        avg = []
        for file in os.listdir(outdir + '/nrg'):
            nrgfile = outdir + '/nrg/%i' % int(file)
            occfile = outdir + '/occ/%i' % int(file)
            magfile = outdir + '/mag/%i' % int(file)
            if all([os.path.isfile(_file) for _file in [nrgfile,occfile,magfile]]):
                nrg = np.loadtxt(nrgfile)
                occ = np.loadtxt(occfile).tolist()
                mag = np.loadtxt(magfile).tolist()

                print len(occ)

                if len(nrg) == 3:
                    files.append(file)
                    nrgs.append(nrg)
                    occs.append(occ)
                    mags.append(mag)
                    filename = "%i-%i-%.2f-%.2f-%i-%.2e" % (width,6,B,U,num_ks,current)

                    filehash = abs(hash(filename)) % 1000000

                    data.append(supp.trans_dos([-.15*cfg.Const.e + U*cfg.Const.e/2,.15*cfg.Const.e + U*cfg.Const.e/2],
                                    cfg.Const.transres,edge=edge,need_trans = True ,
                                    transoverwrite = overwrite,gfoverwrite = overwrite,
                                    lattice=lattice,spin=2,U = (U,occ*10,phase+str(filehash) + str(file)), Z = mag*10)[0])
        return (nrgs,occs,mags,data,files)
    else: print "no",outdir
