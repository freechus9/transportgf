/*
   Copyright (c) 2009-2014, Jack Poulson
   All rights reserved.

   This file is part of Elemental and is under the BSD 2-Clause License, 
   which can be found in the LICENSE file in the root directory, or at 
   http://opensource.org/licenses/BSD-2-Clause
*/
// NOTE: It is possible to simply include "elemental.hpp" instead
//#include <iomanip>
#include "elemental.hpp"
#include ELEM_DIAGONALSCALE_INC
#include ELEM_HEMM_INC
#include ELEM_HERK_INC
#include ELEM_FROBENIUSNORM_INC
#include ELEM_IDENTITY_INC
#include <cblas.h>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <complex>
#include <sstream>
#include <iostream>
#include <string>
#include <math.h>
using namespace std;
using namespace elem;

typedef double Real;
typedef Complex<Real> C;

struct by_nrg { 
        bool operator()(vector<double> const &a, vector<double> const &b) const { 
                    return a.at(0) < b.at(0);
                        }
};

int main( int argc, char* argv[] )
{
    Initialize( argc, argv );

    try 
    {
        const Int n = Input("--size","size of matrix",100);
        const int BW = Input("--BW","blockwidth",1);
        const Int N1 = Input("--n1","size of matrix",100);
        const Int N2 = Input("--n2","size of matrix",100);
        const Int N3 = Input("--n3","blockwidth",1);
        const Int N4 = Input("--n4","size of matrix",100);
        const Int N5 = Input("--n5","size of matrix",100);
        const Int N6 = Input("--n6","blockwidth",1);
        const int width = Input("--width","blockwidth",1);
        const Int threads = Input("--threads","blockwidth",1);
        const Int NUM_KS = Input("--num_ks","blockwidth",1);
        const bool print = Input("--print","print matrices?",false);
        //const Int filehash = Input("--index_hash","indices file name hash",0);
        const Int filehash = Input("--filehash","indices file name",0);

        //cerr << filename.c_str() << endl;

        ProcessInput();
        //PrintInputReport();

        Grid g( mpi::COMM_WORLD );


        const int SIZE = mpi::CommSize(mpi::COMM_WORLD);
        const int RANK = mpi::CommRank(mpi::COMM_WORLD);
        if (RANK==0) cerr << g.Height() << " " << g.Width() << endl;

        int Ns[6];

        Ns[0] = N1;
        Ns[1] = N2;
        Ns[2] = N3;
        Ns[3] = N4;
        Ns[4] = N5;
        Ns[5] = N6;

        vector<Matrix<C>> Hs;
        for (int p = 0; p < 6; p++) {

            Matrix<C> H(Ns[p],2);
            Zeros(H,Ns[p],2);
            double a,b;
            int i,j,num_elem,ind=0;
            string line;
            stringstream filename;

            filename << "/scratch/users/girdhar2/mag/output/ind/indices-" 
                     << filehash << "-" << p << "-" << threads << ".dat";
            ifstream indices(filename.str());

            while(getline(indices,line)) {

                stringstream stream(line);
                
                stream >> i;
                stream >> j; 
                stream >> a;
                stream >> b;

                
                H.Set(ind,0,C(i,j));
                H.Set(ind,1,C(a,b));
                ind++;
            }

            indices.close();
            Hs.push_back(H);
        }

        double epsilon = .001;

        int sidelen = NUM_KS;

        double a = 1.42e-10;

        double dx = 4.*M_PI/3./a/(double(sidelen) - 1.);
        double dy = 8.*M_PI/sqrt(3.)/a/(double(2*sidelen) - 1.);

        vector<C> ks;

        for (int i = 0; i < sidelen; i++) {
            for (int j = -sidelen + 1; j < sidelen; j++) {
                double kx = dx*double(i);
                double ky = dy*double(j);

                if (kx < (2.*M_PI/3./a)) {
                    if (ky > 0.) {
                        if (ky > (kx*sqrt(3.)*(1. + epsilon))) continue;
                        else ks.push_back(C(kx,ky));
                    } else {
                        if (ky < -(kx*sqrt(3.)*(1. + epsilon))) continue;
                        else ks.push_back(C(kx,ky));
                    }
                } else {
                    if (ky > 0.) {
                        if (ky > (((4*M_PI/3./a) - kx)*sqrt(3.)*(1. + epsilon))) continue;
                        else ks.push_back(C(kx,ky));
                    } else {
                        if (ky < -(((4*M_PI/3./a) - kx)*sqrt(3.)*(1. + epsilon))) continue;
                        else ks.push_back(C(kx,ky));
                    }
                }
                        
            }
        }


        int nk = ks.size();

        if (RANK == 0) cerr << "numks: " << nk << endl;
        vector<int> nks;
        for (int i = 0; i < SIZE; i++) nks.push_back(nk/SIZE + (i < (nk % SIZE) ? 1 : 0));

        //vector<Sorter> nrgs_sort; 
        vector<vector<double>> nrgs_sort; 

        double *pkt;
        if (RANK > 0) pkt = new double[4*2*2*width*BW*nks.at(RANK)];
        else {
            pkt = new double[1];
            delete [] pkt;
        }
    
        for (int k = 0; k < nks.at(RANK); k++) {
            int _k = RANK*nks.at(RANK) + (RANK > ((nk % SIZE)-1) ? (nk % SIZE) : 0) + k;
            double kx = real(ks.at(_k)),ky = imag(ks.at(_k));
            for (int s = 0; s < 2; s++) {

                Matrix<C> H;
                Matrix<C> psi;
                Matrix<Real> nrg;

                Zeros(H,2*width*BW,2*width*BW);
                Zeros(psi,2*width*BW,2*width*BW);
                Zeros(nrg,2*width*BW,2*width*BW);

                for (int p = 0; p < 3; p++) {

                    double T;
                    if (p == 0) T = -sqrt(3.)*1.42e-10;
                    else if (p == 1) T = 0.;
                    else if (p == 2) T = sqrt(3.)*1.42e-10;

                    for (int q = 0; q < Ns[p + s*3]; q++) {
                        int i = int(real(Hs.at(p + s*3).Get(q,0)));
                        int j = int(imag(Hs.at(p + s*3).Get(q,0)));
                        C tmp = (Hs.at(p + s*3).Get(q,1))*exp(C(0.,1.)*(ky*T));
                        H.Update(i,j,tmp);
                    }
                }

                HermitianEig( LOWER , H , nrg , psi, ASCENDING);

                stringstream kfile;
                kfile << "/scratch/users/girdhar2/mag/output/kmat/" << filehash << "-" << _k << "-" << s;
                Write(psi,kfile.str(),BINARY_FLAT);

                if (RANK==0) for (int x = 0; x < 2*width*BW; x++) {
                    vector<double> a;
                    a.push_back(nrg.Get(x,0));
                    a.push_back(static_cast<double>(s));
                    a.push_back(static_cast<double>(_k));
                    a.push_back(static_cast<double>(x));
                    nrgs_sort.push_back(a);
                }
                else {
                    for (int x = 0; x < 2*width*BW; x++) {
                        pkt[k*2*2*width*4 + (2*x+s)*4] = nrg.Get(x,0);
                        pkt[k*2*2*width*4 + (2*x+s)*4 + 1] = static_cast<double>(s);
                        pkt[k*2*2*width*4 + (2*x+s)*4 + 2] = static_cast<double>(_k);
                        pkt[k*2*2*width*4 + (2*x+s)*4 + 3] = static_cast<double>(x);
                    }
                }
            }
        }
        if (RANK == 0) {
            for (int proc = 1; proc < SIZE; proc++) {

                double *_pkt = new double[4*2*2*width*BW*nks.at(proc)];

                mpi::Recv(_pkt,4*2*2*width*BW*nks.at(proc),proc,mpi::COMM_WORLD);

                for (int k =0; k < nks.at(proc); k++) {
                    for (int l = 0; l < 2*2*width*BW; l++) {
                        vector<double> a;

                        a.push_back(_pkt[k*2*2*width*BW*4 + l*4]);
                        a.push_back(_pkt[k*2*2*width*BW*4 + l*4 + 1]);
                        a.push_back(_pkt[k*2*2*width*BW*4 + l*4 + 2]);
                        a.push_back(_pkt[k*2*2*width*BW*4 + l*4 + 3]);

                        nrgs_sort.push_back(a);
                        
                    }
                }

                delete [] _pkt;
            }
        } else {
            mpi::Send(pkt,4*2*2*width*BW*nks.at(RANK),0,mpi::COMM_WORLD);
            delete [] pkt;
        }

        if (RANK == 0) {

            sort(nrgs_sort.begin(),nrgs_sort.end(),by_nrg()); 

            /*
            cerr << "sorted" << endl;

            Matrix<Real> kmetadata(nrgs_sort.size(),4);

            for (int i = 0; i < nrgs_sort.size(); i++) {
                kmetadata.Set(i,0,nrgs_sort.at(i).at(0));
                kmetadata.Set(i,1,nrgs_sort.at(i).at(1));
                kmetadata.Set(i,2,nrgs_sort.at(i).at(2));
                kmetadata.Set(i,3,nrgs_sort.at(i).at(3));
            }

            stringstream kmetaout;
            kmetaout << "/scratch/users/girdhar2/mag/output/kmetadata/" << filehash;
            Write(kmetadata,kmetaout.str(),ASCII);
            */

            int s,k,x;
            double nrg;
            Matrix<Real> occupation(2*2*width*BW,1),energy(2*width*BW,1);

            Zeros(occupation,2*2*width*BW,1);
            Zeros(energy,2*width*BW,1);

            cout << "rdy2write" << endl;

            for (int i = 0; i < nk*2*width; i++) {
                nrg = nrgs_sort.at(i).at(0);
                s = static_cast<int>(nrgs_sort.at(i).at(1) + .1);
                k = static_cast<int>(nrgs_sort.at(i).at(2) + .1);
                x = static_cast<int>(nrgs_sort.at(i).at(3) + .1);

                stringstream kfile;

                kfile << "/scratch/users/girdhar2/mag/output/kmat/" << filehash << "-" << k << "-" << s << ".dat";

                Matrix<C> psi;
                read::BinaryFlat(psi,2*width*BW,2*width*BW,kfile.str());

                for (int j = 0; j < 2*width*BW; j++) {
                    occupation.Update(j*2 + s,0,real(psi.Get(j,x)*conj(psi.Get(j,x)))/nk);
                    energy.Update(j,0,real(psi.Get(j,x)*conj(psi.Get(j,x)))/nk*nrg);
                }
            }

            Print(occupation);
            //stringstream occerr,nrgout;
            //occerr << "/scratch/users/girdhar2/mag/output/tmp/occ-" << filehash << "-" << threads;
            //nrgout << "/scratch/users/girdhar2/mag/output/tmp/nrg-" << filehash << "-" << threads;
            //Write(occupation,occerr.str(),ASCII);
            //Write(energy,nrgout.str(),ASCII);
        }

    }
    catch( exception& e ) { ReportException(e); }

    Finalize();
    return 0;
}
