import support as supp
import cfg
import numpy as np
from matplotlib import pyplot as plt
from scipy.interpolate import interp1d
from scipy.interpolate import griddata
from scipy.interpolate import RectBivariateSpline as RBS
import sys

#width = 201
width = int(sys.argv[2])

length = 2
edge = 'ac'

lattice = cfg.get_lattice(width,length,edge,shape='GNR')

B = float(sys.argv[1])
def A(r):
    return np.array([B*r[1],0.,0.])

data = supp.trans_dos([-.5*cfg.Const.e,.5*cfg.Const.e],
                       cfg.Const.transres,edge=edge,need_trans = True,
                       lattice=lattice,A=(A,'B%.2f' % B))[0]
                       #lattice=lattice)[0]
'''
B = 10.
def A(r):
    return np.array([B*r[1],0.,0.])

data2 = supp.trans_dos([-.5*cfg.Const.e,.5*cfg.Const.e],
                       cfg.Const.transres,edge=edge,need_trans = True,
                       lattice=lattice,A=(A,'B%.2f' % B))[0]

index = []
for i,val in enumerate(data[1]):
    if val < 0. or abs(val) > 50:
        index.append(i)

data = np.delete(data,index,1)

index = []
for i,val in enumerate(data2[1]):
    if val < 0. or abs(val) > 50:
        index.append(i)
data2 = np.delete(data2,index,1)


if cfg.RANK == 0:
    fig = plt.figure(1)

    ax = fig.add_subplot(111)
    ax.plot(data[0],data[1])
    ax.plot(data2[0],data2[1])
    fig.savefig('magtest.png',format='png')
'''
