import sys
import numpy as np
import matplotlib.pyplot as plt
from mpi4py import MPI
from subprocess import Popen
import os
import support as supp
import cfg as cfg
import hubbtrans
COMM = MPI.COMM_WORLD           # MPI Init
RANK = COMM.Get_rank()          # Each thread gets own rank
NUMTHREADS = COMM.Get_size()      # Each thread knows numprocs
def wait(): COMM.recv(source=0,tag=1)
def release(): return [COMM.send('go',dest=i+1,tag=1) for i in xrange(NUMTHREADS - 1)]
if len(sys.argv) > 1:
    width = int(sys.argv[1])
    length = int(sys.argv[2])
    phase = str(sys.argv[3])
    U = float(sys.argv[4])
    num_ks = int(sys.argv[5])
    B = float(sys.argv[6])
    tol = float(sys.argv[7])
    current = float(sys.argv[8])

'''
width = 100
length = 6
U = 2.7
num_ks = 300
tol = 1.e-6
B = 0.
'''
tmp = hubbtrans.get_trans(width,length,U,"FM",num_ks,B,tol,current)
tmp2 = hubbtrans.get_trans(width,length,U,"AFM",num_ks,B,tol,current)


if RANK == 0:
    curves = []
    for (data,file) in zip(tmp[3],tmp[4]):
        try: 
            _ind = tmp2[4].index(file)
            curves.append(data)
            print data[0]/cfg.Const.e
            curves.append(tmp2[3][_ind])
        except: continue
    cfg.plot(curves=curves,subplots=len(curves)/2,filename='transtest')
'''
if RAVNK == 0:
    data = np.array(data)
    fig = plt.figure(1)
    ax = fig.add_subplot(1,1,1)
    #ax.scatter(current,np.min(data[1][1]) - np.min(data[0][1]),color='k',lw=2)
    #ax.plot(data[0][3][0]/1.602e-19,data[0][3][1],color='k',lw=2)
    #ax.plot(data[1][3][0]/1.602e-19,data[1][3][1],color='g',ls='dashed',lw=2)
    ax = ax.twinx()
    #ax.scatter(current,(nrgs[0] - nrgs[1]),marker="+")
    #ax.set_xlim(-10.e-3,50.e-3)
    #ax.set_ylim(0.,.0001)
    ax.scatter(current,(data[1][0][0] - data[0][0][0]))

    #print data[0][1]
    #print data[1][1]
    ax.set_xlabel(' current')
    ax.set_ylabel('T')
fig.savefig('hubbtranstest.png',format='png')
'''
