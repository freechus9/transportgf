import hubbard_uniform as hubb
import hubbtrans
import matplotlib.pyplot as plt
import sys
from mpi4py import MPI
import numpy as np

COMM = MPI.COMM_WORLD           # MPI Init
RANK = COMM.Get_rank()          # Each thread gets own rank
NUMTHREADS = COMM.Get_size()      # Each thread knows numprocs

def wait(): COMM.recv(source=0,tag=1)
def release(): return [COMM.send('go',dest=i+1,tag=1) for i in xrange(NUMTHREADS - 1)]

'''
width = int(input('width:'))
phase = str(input('phase:'))
U = float(input('U:'))
num_ks = int(input('num_ks:') )
tol = float(input('tolerance:') )
current = float(input('current:') )
threads = int(input('threads:') )
width = 8
phase = "AFM"
U = 2.7
num_ks = 30
tol = 1.e-6
current = 0.
threads = 4
'''

#width = 8
#phase = "AFM"
#U = 2.7
#num_ks = 30
#tol = 1.e-6
#current = 0.
#threads = 4
width = int(sys.argv[1])
phase = str(sys.argv[2])
U = float(sys.argv[3])
num_ks = int(sys.argv[4])
tol = float(sys.argv[5])
current = float(sys.argv[6])
threads = int(sys.argv[7])

data = []
if RANK == 0: 
    data.append(hubb.run_hubb(THREADS=threads,width=width,length=6,phase=phase,U=U,num_ks=num_ks,tol=tol,current=current,runanyway=True)[0][0])
    release()
else: wait()
