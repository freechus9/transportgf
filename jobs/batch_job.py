import subprocess

num_jobs = 3
output = subprocess.check_output(["qsub", "submit_job"])

for i in xrange(num_jobs - 1):
    output = subprocess.check_output(["qsub","-W","depend=afterok:%s" % output[:-1],"submit_job"])


