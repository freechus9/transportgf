qsub << EOF
#!/bin/bash
#PBS -q secondary
#PBS -l nodes=$NUM_NODES:ppn=12:taub
#PBS -l walltime=$HOURS:$MINUTES:00
#PBS -j oe
#PBS -o /scratch/users/girdhar2/stdout/$WIDTH-$PHASE-$NUM_NODES-$U-$NUM_KS-$TOL-$I.out
#PBS -N $WIDTH-$PHASE-$U-$NUM_KS-$TOL-$I
cd /home/girdhar2/project-cse/research/transportgf/src
#module load gcc
module load mvapich2/2.0b-gcc-4.7.1
#module load mvapich2
python runhubb.py $WIDTH $PHASE $U $NUM_KS $TOL $I $[12*$NUM_NODES]
#mpiexec -n $[12*$NUM_NODES] python trans.py 1
EOF
