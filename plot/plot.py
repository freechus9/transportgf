import cfg
import numpy as np
import matplotlib.pyplot as plt
import os
import sys
from subprocess import Popen
from scipy.interpolate import interp1d
from scipy.interpolate import griddata
from scipy.interpolate import RectBivariateSpline as RBS
from matplotlib import cm
from matplotlib.patches import Circle
from matplotlib.collections import PatchCollection
from matplotlib.gridspec import GridSpec as GS
from matplotlib.gridspec import GridSpecFromSubplotSpec as GSFSS
import hubbard_uniform as hubb
import matplotlib as mpl

mpl.rcParams['font.weight'] = 'extra bold'
mpl.rcParams['font.size'] = 28
mpl.rcParams['xtick.major.size'] = 8
mpl.rcParams['xtick.major.pad'] = 8
mpl.rcParams['ytick.major.size'] = 8
mpl.rcParams['ytick.major.pad'] = 8
mpl.rcParams['axes.linewidth'] = 6

run = False
plot = False

fig = plt.figure(1,figsize=(9,15))

gs = GS(2,1,height_ratios=[3,1])

gs0 = GSFSS(3,1,subplot_spec=gs[0],hspace=0.)
gs1 = gs[1]

Us = np.arange(2.0,3.0,.1)
Is = np.arange(0.e-3,3.e-3,3.e-4)
for w,width in enumerate([192,216]):

    ax = plt.subplot(gs0[w,0])
    ax.set_title('%i' % width)

    x = []
    y = []
    z = []

    for I in Is:
        for U in Us:
            nrg = []
            for phase in ["AFM","FM"]:
                nrg.append(hubb.run_hubb(THREADS=1,width=width,length=6,phase=phase,U=U,num_ks=128,tol=5.e-7,current=I,runanyway=False,ignore_empty = True)[0][0])
            x.append(I/1.e-3)
            y.append(U)
            z.append((nrg[1]-nrg[0])/1.e-6)

    print 'set'

    print z

    #gridx,gridy = np.mgrid[10:80:100j,2.5:3.:70j]
    #f = griddata((np.array(x),np.array(y)),np.array(z),
            #(gridx,gridy),method='linear')
    #plt.imshow(f)

    f = RBS(Is/1.e-3,Us,np.reshape(z,(len(Is),len(Us))),s=0)

    #X = np.arange(0.,50.,1.)
    #Y = np.arange(2.0,3.5,.05)
    #X,Y = np.meshgrid(X,Y)
    #Z = np.zeros((len(X),len(X[0])))

    #for i in xrange(len(X)):
    #    for j in xrange(len(X[0])):
    #        Z[i,j] = f(X[i,j],Y[i,j])[0][0]

    print 'makemesh'
    mesh = ax.pcolormesh(np.reshape(x,(len(Is),len(Us))),np.reshape(y,(len(Is),len(Us))),np.reshape(z,(len(Is),len(Us))),cmap = cm.RdBu,vmin=-1e2,vmax=1e2)
    #ax.set_xlim(0.,50.)
    #ax.set_ylim(2.0,3.4)
    
    '''
    if w == 2:
        ax.set_yticks(np.arange(1.6,3.2,.6).tolist())
        ax.set_yticklabels(['%.1f' % val for val in np.arange(1.6,3.2,.6)])
        ax.set_xticks(np.arange(0.,140.,40.).tolist())
        ax.set_xticklabels([str(int(val + .1)) for val in np.arange(0.,140.,40.)])
        ax.set_xlabel('I (mA)',fontweight='extra bold')
    elif w == 0:
        ax.set_xticks(np.arange(0.,140.,40.).tolist())
        ax.set_xticklabels([])
        ax.set_yticks(np.arange(1.6,3.5,.6).tolist())
        ax.set_yticklabels(['%.1f' % val for val in np.arange(1.6,3.5,.6)])

        rect = [0.,.5,.4,.5]

        box = ax.get_position()
        width = box.width
        height = box.height
        inax_position  = ax.transAxes.transform(rect[0:2])
        transFigure = fig.transFigure.inverted()
        infig_position = transFigure.transform(inax_position)    
        x = infig_position[0]
        y = infig_position[1]
        width *= rect[2]
        height *= rect[3]  # <= Typo was here
        subax = fig.add_axes([x,y,width,height],axisbg='w')

        Ic = 48.2
        x = np.arange(0.,100.,1.)
        y = [1 if v > Ic else 0 for v in x]

        subax.plot(x,y,lw=6,color='r')
        subax.set_xlim(0,150)
        subax.set_ylim(-.8,1.8)
        subax.set_xticks([Ic])
        subax.set_xticklabels(['I C'])
        subax.axvline(Ic,linestyle='dashed',linewidth=3,zorder=3)
        subax.set_yticks([0,1])
        subax.set_yticklabels(['0','G 0'])
        subax.xaxis.set_tick_params(width=5)
        subax.yaxis.set_tick_params(width=5)
        subax.xaxis.set_label_position('top')
        subax.yaxis.set_label_position('right')
        subax.xaxis.tick_top()
        subax.yaxis.tick_right()
        for tick in subax.yaxis.get_major_ticks():
            tick.set_pad(-15)
            tick.label2.set_fontsize(18)
            tick.label2.set_horizontalalignment('right')
        for tick in subax.xaxis.get_major_ticks():
            tick.label2.set_fontsize(18)

    else:
        ax.set_xticks(np.arange(0.,140.,40.).tolist())
        ax.set_xticklabels([])
        ax.set_yticks(np.arange(1.6,3.2,.6).tolist())
        ax.set_yticklabels(['%.1f' % val for val in np.arange(1.6,3.2,.6)])
        ax.set_ylabel('U (eV)',fontweight='extra bold')

    ax.xaxis.set_tick_params(width=5)
    ax.yaxis.set_tick_params(width=5)

    #plt.title('%.2f nm' % (width*1.5*1.42e-10/1.e-9))

    Ic = []

    for i in xrange(len(X)):
        Ic.append(X[0,abs(Z[i,:]).tolist().index(min(abs(Z[i,:])))])
    ax.plot(Ic,Y[:,0],lw=6,dashes=(4,2),color='k')

    print 'plotty'
    '''

cax = fig.add_axes([.92,.3545,.04,.546])
cbar = plt.colorbar(mesh,cax=cax)

cbar.set_label(r'$\mathbf{\Delta}$E (ueV)',fontweight='extra bold')

"""

width = 120
length = 6
_length = 6
U = 2.0
#ks = np.arange(100,300,2)
num_ks = 200
ax = plt.subplot(gs1)
current = 20.e-3
phase = "FM"
outdir = cfg.output_dir + '/hubb' + cfg.get_dirs(width,length,U,phase,num_ks,current)
nrgfile = outdir + '/nrg'
occfile = outdir + '/occ'
magfile = outdir + '/mag'
if all([os.path.isfile(_file) for _file in [nrgfile,occfile,magfile]]):
    nrg = np.loadtxt(nrgfile)
    occ = np.loadtxt(occfile)
    mag = np.loadtxt(magfile)

    occ = np.array((occ[::2] - occ[1::2]).tolist()*(_length/2))

    _width = 14

    print np.array([(-.3 if val < 0. else .3) for val in np.reshape(occ,(_length,width))[:,:20].flatten()])

    patches = []
    for col in xrange(_length):
        for row in xrange(_width):
            x,y = cfg.get_pos(col,row,'zz')
            patches.append(Circle((x/cfg.Const.a,y/cfg.Const.a),abs(occ[col*width + row])**.5*1.5,ec='none'))
    p = PatchCollection(patches,cmap=plt.cm.cool,alpha=1.)
    p.set_array(np.array([(-.3 if val < 0. else .3) for val in np.reshape(occ,(_length,width))[:,:_width].flatten()]))
    p.set_clim([-.3,.3])
    cfg.print_dev_conf(cfg.get_lattice(_width,_length,'zz'),'zz',subplot = ax,fig=fig)
    ax.add_collection(p)
    ax.set_aspect('equal')
    ax.set_xlim(-1.,11.)
    ax.set_ylim(-1,3.5)
    ax.annotate('',xy=(-.05,.9),xycoords='axes fraction',xytext=(-.05,.3),
            arrowprops=dict(facecolor='k',width=10,headwidth=20),
            horizontalalignment='left',
            verticalalignment='bottom')
    ax.annotate('',xy=(.5,.05),xycoords='axes fraction',xytext=(.2,.05),
            arrowprops=dict(facecolor='k',width=10,headwidth=20),
            horizontalalignment='left',
            verticalalignment='bottom')
    ax.text(2.9,-1.4,'x')
    ax.text(-2.3,1.6,'y')
    run = False

ax = fig.add_axes([.7,.14,.3,.14])
#ax.plot(np.linspace(0,1,len(mag[::2])/2),mag[::2][:width],lw=6,color='k')
c = ['r','b']
for col in [0,1]:
    for row in xrange(10):
        #plt.scatter(cfg.get_pos(col,row,'zz')[0]/1.e-9,occ[col*width + row]*200,color=c[(row % 2 + col)%2],s=50)
        plt.scatter(cfg.get_pos(col,row,'zz')[0]/1.e-9,
                    occ[col*width + row]*200,color='k',s=50)
#ax.plot(x,y,lw=6,color='k')
ax.set_xlabel('x (nm)',fontweight='extra bold')
ax.set_ylabel('m (%)',fontweight='extra bold')
ax.set_xticks([0.,1.,2.])
ax.set_xticklabels(['0','1','2'])
ax.set_yticks([-60,-40,-20,0])
ax.xaxis.set_tick_params(width=5)
ax.yaxis.set_tick_params(width=5)
ax.xaxis.set_label_position('bottom')
ax.yaxis.set_label_position('right')
ax.xaxis.tick_bottom()
ax.yaxis.tick_right()

"""
plt.savefig('mag-%i.png' % width,format='png',bbox_inches='tight')

plt.clf()
"""

EF = 1.95

filename = "%i-%i-%.2f-%s-%i-%.2e" % (12,6,2.7,"FM",100,0.)

filehash = abs(hash(filename)) % 10000000

ekfile = open("%s/tmp/ek-%i-%i.txt" % (cfg.output_dir,filehash,4),'r')

ks = np.zeros((100,2*12*2 + 1),dtype = float)

for i,line in enumerate(ekfile): 
    try: 
        kx,ky,energy = [float(val) for val in line.split()]
        ks[i / (2*12*2),0] = np.sqrt(kx**2 + ky**2)
        ks[i / (2*12*2),(i % (2*12*2))+1] = energy
        #plt.scatter(np.sqrt(kx**2 + ky**2),energy)
    except:
        print line
print ks
fig = plt.figure(1,figsize=(12,9))
ax = fig.add_subplot(212)
ax.set_xlim(0.,25546469918.791798*cfg.Const.a)
ax.set_ylim(-2.5,2.5)
for plot in xrange(2*12):
    ax.plot(ks[:,0]*cfg.Const.a,ks[:,plot+1] - EF,color='r',lw=6,label = ' ')
for plot in xrange(2*12):
    ax.plot(ks[:,0]*cfg.Const.a,ks[:,plot+1 + 2*12] - EF,color='b',lw=6,label=' ')
ax.xaxis.set_tick_params(width=5)
ax.yaxis.set_tick_params(width=5)
ax.set_xticks([0.,25546469918.791798*cfg.Const.a/1.5,25546469918.791798*cfg.Const.a])
ax.set_xticklabels(['L','K','M'])
ax.set_yticks([-2,-1,0,1,2])
ax.set_ylabel('E - E F (eV)',fontweight='extra bold',y = 1.)
handles,labels = ax.get_legend_handles_labels()
ax.legend((handles[0],handles[-1]),(labels[0],labels[-1]),loc='lower left',frameon=False)

filename = "%i-%i-%.2f-%s-%i-%.2e" % (12,6,2.7,"AFM",100,0.)

filehash = abs(hash(filename)) % 10000000

ekfile = open("%s/tmp/ek-%i-%i.txt" % (cfg.output_dir,filehash,4),'r')

ks = np.zeros((100,2*12*2 + 1),dtype = float)

for i,line in enumerate(ekfile): 
    try: 
        kx,ky,energy = [float(val) for val in line.split()]
        ks[i / (2*12*2),0] = np.sqrt(kx**2 + ky**2)
        ks[i / (2*12*2),(i % (2*12*2))+1] = energy
        #plt.scatter(np.sqrt(kx**2 + ky**2),energy)
    except:
        print line
print ks

ax = fig.add_subplot(211)
ax.set_xlim(0.,25546469918.791798*cfg.Const.a)
ax.set_ylim(-2.5,2.5)
for plot in xrange(2*12*2):
    ax.plot(ks[:,0]*cfg.Const.a,ks[:,plot+1] - EF,color='k',lw=6)
ax.xaxis.set_tick_params(width=5)
ax.yaxis.set_tick_params(width=5)
ax.set_xticks([0.,25546469918.791798*cfg.Const.a/1.5,25546469918.791798*cfg.Const.a])
ax.set_xticklabels([])
ax.set_yticks([-2,-1,0,1,2])
plt.subplots_adjust(hspace=0.1)
plt.savefig('ektest.png',format='png') 
"""
'''

    Ics = []
    or i,U in enumerate(Us):
        data = Z[i,:]
        f = interp1d(Is/1.e-3,data,kind='linear')
        newI = np.arange(0.,95,.1)
        newdata = f(newI)
        pooval = 0.
        for j,val in enumerate(newdata[:-1]):
            if newdata[j] > 0. and newdata[j+1] < 0. and abs(newdata[j]) < 100. and abs(newdata[j+1]) < 100.:
                pooval = newI[j]
                break
        Ics.append(pooval)

    print Ics
            
    for i,U in enumerate(Us):
        if Ics[i] > .1:
            plt.scatter(U,Ics[i])

    plt.xlim(1.4,3.6)
    plt.ylim(0.,100.)

    plt.xlabel('U (eV)')
    plt.ylabel('Ic (mA)')
    plt.title('%.2f nm' % (width*1.5*1.42e-10/1.e-9))

    plt.savefig('Ic-%i.png' % width,format='png',bbox_inches='tight')

    plt.clf()
    x = np.arange(0.,100.,.1)
    y = np.zeros(1000)

    y[632:] = 1.

    plt.plot(x,y,lw=6)
    plt.xlabel('I_EXT (mA)')
    plt.ylabel('G_RIB (2e' + r'$^2$' + '/h)')
    plt.xlim(0.,100.)
    plt.ylim(-1.,2.)
    plt.vlines([63.1],-1,2,linestyles='dashed')
    plt.savefig('current.png',format='png',bbox_inches='tight')
'''
