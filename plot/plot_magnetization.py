import cfg
import numpy as np
import matplotlib.pyplot as plt
import os
from subprocess import Popen
from matplotlib import cm
from matplotlib.patches import Circle
from matplotlib.collections import PatchCollection

run = True

width = 120
length = 6
_length = 16
U = 2.0
#ks = np.arange(100,300,2)
num_ks = 200
fig = plt.figure(1)
ax = fig.add_subplot(1,1,1)
current = 20.e-3
phase = "FM"
outdir = cfg.output_dir + '/hubb' + cfg.get_dirs(width,length,U,phase,num_ks,current)
if os.path.isdir(outdir):
    if run:
        nrgfile = outdir + '/nrg'
        occfile = outdir + '/occ'
        magfile = outdir + '/mag'
        if all([os.path.isfile(_file) for _file in [nrgfile,occfile,magfile]]):
            nrg = np.loadtxt(nrgfile)
            occ = np.loadtxt(occfile)
            mag = np.loadtxt(magfile)

            occ = np.array((occ[::2] - occ[1::2]).tolist()*(_length/2))

            print np.array([(-.3 if val < 0. else .3) for val in np.reshape(occ,(_length,width))[:,:20].flatten()])

            patches = []
            for col in xrange(_length):
                for row in xrange(20):
                    x,y = cfg.get_pos(col,row,'zz')
                    patches.append(Circle((x/cfg.Const.a,y/cfg.Const.a),abs(occ[col*width + row])**.5*1.5,ec='none'))
            p = PatchCollection(patches,cmap=plt.cm.jet,alpha=.7)
            p.set_array(np.array([(-.3 if val < 0. else .3) for val in np.reshape(occ,(_length,width))[:,:20].flatten()]))
            p.set_clim([-.3,.3])
            cfg.print_dev_conf(cfg.get_lattice(20,_length,'zz'),'zz',subplot = ax,fig=fig)
            ax.add_collection(p)
            ax.set_aspect('equal')
            ax.set_xlim(-1,20.)
            ax.set_ylim(-1,12)
            run = False

x = np.zeros(width*2)
y = np.zeros(width*2)

ax = fig.add_axes([.7,.7,.3,.3])
#ax.plot(np.linspace(0,1,len(mag[::2])/2),mag[::2][:width],lw=6,color='k')
c = ['r','b']
for col in [0,1]:
    for row in xrange(20):
        plt.scatter(cfg.get_pos(col,row,'zz')[0]/1.e-9,occ[col*width + row],color=c[(row % 2 + col)%2])
#ax.plot(x,y,lw=6,color='k')
ax.set_xlabel('x (nm)')
ax.set_ylabel('occupation')
ax.xaxis.set_label_position('top')
ax.yaxis.set_label_position('right')
ax.xaxis.tick_top()
ax.yaxis.tick_right()
#ax.xaxis.set_ticklabels([])
#ax.yaxis.set_ticklabels([])
fig.savefig('bfield.png',format='png',bbox_inches='tight')
