import sys
import numpy as np
import matplotlib.pyplot as plt
import os
import support as supp
import cfg as cfg
import matplotlib as mpl

mpl.rcParams['font.weight'] = 'extra bold'
mpl.rcParams['font.size'] = 28
mpl.rcParams['xtick.major.size'] = 8
mpl.rcParams['xtick.major.pad'] = 8
mpl.rcParams['ytick.major.size'] = 8
mpl.rcParams['ytick.major.pad'] = 8
mpl.rcParams['axes.linewidth'] = 6

width = 63
length = 48
edge = 'ac'


lattices = [cfg.get_lattice(width,length,edge,shape='QPC',pores=[[1.2e-9,.5]],pinch=.15,extent=.2),
         cfg.get_lattice(width,length,edge,shape='QPC',pores=[[1.2e-9,int(.75*width)/float(width)]],pinch=.15,extent=.2)]


fig = plt.figure(1,figsize=(24,18))
ax = fig.add_subplot(211)
cfg.print_dev_conf(lattices[0],edge,filename='lattice.png',subplot = ax,fig = fig,style='k-')
ax = fig.add_subplot(212)
cfg.print_dev_conf(lattices[1],edge,filename='lattice.png',subplot = ax,fig = fig,style='k-')
fig.savefig('poop.png',format='png')
