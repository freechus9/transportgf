import sys
import numpy as np
import matplotlib.pyplot as plt
from mpi4py import MPI
from subprocess import Popen
import os
import support as supp
import cfg as cfg
from matplotlib.gridspec import GridSpec as GS
from matplotlib.gridspec import GridSpecFromSubplotSpec as GSFSS
import matplotlib as mpl

mpl.rcParams['font.weight'] = 'extra bold'
mpl.rcParams['font.size'] = 28
mpl.rcParams['xtick.major.size'] = 8
mpl.rcParams['xtick.major.pad'] = 8
mpl.rcParams['ytick.major.size'] = 8
mpl.rcParams['ytick.major.pad'] = 8
mpl.rcParams['axes.linewidth'] = 6

width = 63
length = 48
edge = 'ac'
lattices = [cfg.get_lattice(width,length,edge,shape='QPC',pores=[[1.2e-9,.5]],pinch=.15,extent=.2),
         cfg.get_lattice(width,length,edge,shape='QPC',pores=[[1.2e-9,int(.75*width)/float(width)]],pinch=.15,extent=.2)]

cond_nrgs = [-.25,0,.25]
#cond_nrgs = np.arange(-.35,.35,.04)

fig = plt.figure(1,figsize=(15,10))


for path,potpath in enumerate(['pnaspotmaps/rerunDNA8x10/vplane8x10D','stretch/run1']):

    gs = GS(2,2,wspace=.1,height_ratios=[2,1])

    for lt,lattice in enumerate(lattices):

        if path == 0: 
            gs0 = GSFSS(len(cond_nrgs),1,subplot_spec=gs[lt],hspace=0.22)
        #gs1 = GSFSS(4,1,subplot_spec=gs[1],hspace=0.)

            skip =1
            snaps,conds = np.zeros(400/skip),np.zeros((len(cond_nrgs) if path == 0 else 2,400/skip))
            for snap,snapshot in enumerate(np.arange(1,401,skip)):

                offset = 0.
                if lt == 1: pot = (cfg.gen_pot(potpath + '%s.dat' % snapshot,offset),potpath + '%s.dat' % snapshot,(5.e-9,4.e-9),(cfg.get_pos(length-1,1,'ac')[0]/2,int(.75*width)/float(width)*cfg.get_pos(0,width-1,'ac')[1]))
                else: pot = (cfg.gen_pot(potpath + '%s.dat' % snapshot,offset),potpath + '%s.dat' % snapshot,(5.e-9,4.e-9),(cfg.get_pos(length-1,1,'ac')[0]/2,cfg.get_pos(0,width-1,'ac')[1]/2))

                packet = supp.gather_data([-.6,.6], edge, lattice, pot = pot,
                              need_trans = True,
                              cond_nrgs = cond_nrgs,T = 300.,EF = 0.,cond_only = False)[0]

                snaps[snap] = snapshot
                for p in xrange(len(cond_nrgs)):
                    conds[p,snap] = packet[2][1][p]/1.e-6


            for p in xrange(len(cond_nrgs)):
                ax = plt.subplot(gs0[p,0])
                ax.plot(snaps,conds[p,:],color='k',lw=6)
                if lt == 0:
                    if p == 0:
                        ax.set_ylim(15.4,15.6)
                        ax.set_yticks([15.4,15.5,15.6])
                        ax.set_yticklabels(['15.4','','15.6'])
                    elif p == 1:
                        ax.set_ylim(25.2,27.6)
                        ax.set_yticks([25.2,26.4,27.6])
                        ax.set_yticklabels(['25.2','','27.6'])
                    elif p == 2:
                        ax.set_ylim(8.4,9.4)
                        ax.set_yticks([8.4,8.9,9.4])
                        ax.set_yticklabels(['8.4','','9.4'])
                elif lt == 1:
                    if p == 0:
                        ax.set_ylim(90.4,91.6)
                        ax.set_yticks([90.4,91.0,91.6])
                        ax.set_yticklabels(['90.4','','91.6'])
                    elif p == 1:
                        ax.set_ylim(26.2,27.2)
                        ax.set_yticks([26.2,26.7,27.2])
                        ax.set_yticklabels(['26.2','','27.2'])
                    elif p == 2:
                        ax.set_ylim(15.2,16.6)
                        ax.set_yticks([15.2,15.9,16.6])
                        ax.set_yticklabels(['15.2','','16.6'])
                rng = np.ptp(conds[p,:])
                #ax.set_ylim(min(conds[p,:]),max(conds[p,:]))
                #ax.set_yticks([min(conds[p,:]),max(conds[p,:])])
                #ax.set_yticklabels(['%.1f' % min(conds[p,:]),'%.1f' % max(conds[p,:])])
                ax.set_xticks([50,150,250,350])
                ax.set_xlim(0,400)
                if not (p == (len(cond_nrgs)-1)):
                    ax.set_xticklabels([])
                if p == 1:
                    ax.set_ylabel('G (uS)',fontweight='extra bold')
                if lt == 1: 
                    ax.yaxis.tick_right()
                    ax.yaxis.set_label_position('right')
                ax.xaxis.set_tick_params(width=5)
                ax.yaxis.set_tick_params(width=5)
        else: 
            gs0 = gs[lt + 2] 
            skip =1
            for stretch,snaplen in zip(['/1/vplane','/5/vplane'],[34,60]):
                
                snaps,conds = np.zeros(snaplen/skip),np.zeros(snaplen/skip)
                for snap,snapshot in enumerate(np.arange(1,snaplen+1,skip)):

                    offset = 0.
                    if lt == 1: pot = (cfg.gen_pot(potpath + stretch + '%s.dat' % snapshot,offset),potpath + stretch + '%s.dat' % snapshot,(5.e-9,2.5e-9),(cfg.get_pos(length-1,1,'ac')[0]/2,int(.75*width)/float(width)*cfg.get_pos(0,width-1,'ac')[1]))
                    else: pot = (cfg.gen_pot(potpath + stretch + '%s.dat' % snapshot,offset),potpath + stretch + '%s.dat' % snapshot,(5.e-9,2.5e-9),(cfg.get_pos(length-1,1,'ac')[0]/2,cfg.get_pos(0,width-1,'ac')[1]/2))

                    packet = supp.gather_data([-.6,.6], edge, lattice, pot = pot,
                                  need_trans = True,
                                  cond_nrgs = [.25],T = 300.,EF = 0.,cond_only = False)[0]

                    snaps[snap] = snapshot
                    conds[snap] = packet[2][1][0]/1.e-6


                ax = plt.subplot(gs0)
                colors = ['r','g']
                ax.plot(snaps,conds,color=colors[snaplen/34 - 1],lw=6)
                #ax.set_ylim(min(conds),max(conds))
                #ax.set_yticks([min(conds),max(conds)])
                #ax.set_yticklabels(['%.1f' % min(conds),'%.1f' % max(conds)])
                #ax.set_xticks([100,200,300])
                ax.set_xlabel('Snapshot',fontweight='extra bold')
                ax.set_ylabel('G (uS)',fontweight='extra bold')
                ax.xaxis.set_tick_params(width=5)
                ax.yaxis.set_tick_params(width=5)
                ax.set_xlim(0,60)
                if lt == 0:
                    ax.set_yticks([8.2,8.8,9.4])
                if lt == 1: 
                    ax.set_yticks([15.2,15.9,16.6])
                    ax.yaxis.tick_right()
                    ax.yaxis.set_label_position('right')


if cfg.RANK == 0:
    fig.savefig('transtest.png',format='png',bbox_inches='tight',dpi=600)

    '''

    X,Y = np.meshgrid(np.arange(0.,10.e-9,.1e-9),np.arange(0.,8.e-9,.1e-9))
    Z = np.zeros((len(X),len(X[0])))

    for i in xrange(len(X)):
        for j in xrange(len(X[0])):
            Z[i,j] = cfg.get_pot(pot,X[i,j],Y[i,j])

    ax = fig.add_subplot(312)
    mesh = ax.pcolormesh(X,Y,Z)
    plt.colorbar(mesh)

    ax = fig.add_subplot(313)
    cfg.print_dev_conf(lattice,edge,subplot=ax,fig=fig)
    '''
