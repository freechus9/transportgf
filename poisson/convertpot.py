"""
Anuj Girdhar

Converts poisson solver output into transport input
"""

import numpy as np
from matplotlib import pyplot as plt
import cfg
from scipy.interpolate import griddata

length = 100
width = 59
'''
data = np.loadtxt('../pots/test/pot0.0.dat',delimiter=',')
epsgrid = np.loadtxt('../pots/test/epsgrid.dat',delimiter=', ')
flagsolute = np.loadtxt('../pots/test/flagsolute.dat',delimiter=', ')
flagsolute = np.reshape(flagsolute,(129,129,129))[:,:,94]

x = (np.reshape(data[:,0],(129,129)) - (24.9e-9 - cfg.get_pos(length - 1,1,'ac')[0]/2))/cfg.Const.a
y = (np.reshape(data[:,1],(129,129)) - (24.9e-9 - cfg.get_pos(0,width-1,'ac')[1]/2))/cfg.Const.a
v = np.reshape(data[:,3],(129,129))

'''
potpath = 'test/pot0.0.dat'
pot = (cfg.gen_pot(potpath),potpath,(24.9e-9,24.9e-9),(cfg.get_pos(length-1,1,'ac')[0]/2,cfg.get_pos(0,width-1,'ac')[1]/2))

x = []
y = []
potdata = []

for k,i in enumerate(xrange(length)):
    x.append([])
    y.append([])
    potdata.append([])

    for j in xrange(width):
        x[k].append(cfg.get_pos(i,j,'ac')[0])
        y[k].append(cfg.get_pos(i,j,'ac')[1])
        potdata[k].append(
                pot[0](pot[2][0] - (pot[3][0] - x[k][j])*1.5,
                        pot[2][1] - (pot[3][1] - y[k][j])*1.5)[0][0])

avgpot = sum(potdata[0] + potdata[-1])/(2*width)

print avgpot

pot = (cfg.gen_pot(potpath,-avgpot),potpath,(25.e-9,25.e-9),(cfg.get_pos(length-1,1,'ac')[0]/2,cfg.get_pos(0,width-1,'ac')[1]/2))

x = []
y = []
potdata = []

for k,i in enumerate(xrange(length)):
    x.append([])
    y.append([])
    potdata.append([])

    for j in xrange(width):
        x[k].append(cfg.get_pos(i,j,'ac')[0])
        y[k].append(cfg.get_pos(i,j,'ac')[1])
        potdata[k].append(
                pot[0](pot[2][0] - (pot[3][0] - x[k][j])*1.5,
                        pot[2][1] - (pot[3][1] - y[k][j])*1.5)[0][0])

avgpot = sum(potdata[0] + potdata[-1])/(2*width)

print avgpot
x = np.array(x)/cfg.Const.a
y = np.array(y)/cfg.Const.a
potdata = np.array(potdata)
v = np.loadtxt('../../../scratch/pots/test/0.0.dat',delimiter=',')
fig = plt.figure(1)
ax = fig.add_subplot(111)

#vx = np.arange(x.min(),x.max(),1.e-11)[:,None]
#vy = np.arange(y.min(),y.max(),1.e-11)[None,]
#VX,VY = np.broadcast_arrays(vx,vy)

#conc = griddata((x.ravel(),y.ravel()),potdata.ravel(),(VX,VY),method = 'cubic')
#print conc

mesh = ax.pcolormesh(x,y,potdata)

lattice = cfg.get_lattice(width,length,'ac',pores=[[1.2e-9,.5]],shape='QPC')
cfg.print_dev_conf(lattice,'ac',subplot=ax)

print v[63:66,63:66]
cax = fig.add_axes([.67,.09,.03,.82])
cbar = fig.colorbar(mesh,cax=cax)
fig.savefig('test.png',format='png')
